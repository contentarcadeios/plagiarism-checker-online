//
//  AppDelegate.swift
//  PlagiarismChecker
//
//  Created by Talha Ejaz on 12/15/17.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import ObjectiveDropboxOfficial
import Fabric
import Crashlytics
import AppKit
import StoreKit
import SwiftyStoreKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {


    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
      //  callGenerateToken()
        
        SKPaymentQueue.default().add(self)
        SubscriptionService.shared.loadSubscriptionOptions()
        
        self.validateUserInAppSubscription()
   
        let userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/603.1.23 (KHTML, like Gecko) Version/10.0 Mobile/14E5239e Safari/602"
        var dict:[String:Any] = [String:Any]()
        dict["UserAgent"] = userAgent
        UserDefaults.standard.register(defaults: dict)
        
        DBClientsManager.setup(withAppKeyDesktop: kDropboxAppKey)
        
        Fabric.with([Crashlytics.self])
        
        userDefaults.register(defaults: ["NSApplicationCrashOnExceptions" : true])
       // registerCustomFonts()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationWillFinishLaunching(_ notification: Notification) {
        NSAppleEventManager.shared().setEventHandler(self, andSelector: #selector(handleAppleEvent(_:_:)), forEventClass: AEEventClass(kInternetEventClass), andEventID: AEEventID(kAEGetURL))
    }
    
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
    
    func validateUserInAppSubscription(){
        guard
            SubscriptionService.shared.hasReceiptData else {
                showRestoreAlert()
                return
        }
        
//        SubscriptionService.shared.uploadReceipt { (success) in
//            print("session id upload :\(String(describing: SubscriptionService.shared.currentSessionId))")
//            print("sub id upload :\(String(describing: SubscriptionService.shared.currentSubscription?.productId))")
//            print("sub id active \(String(describing: SubscriptionService.shared.currentSubscription?.isActive))")
//        }
    }
    
    func showRestoreAlert(){
        if ( dialogOKCancel(question:"Subscription Issue" , text: "We are not able to find your subscription. If you've reinstalled the app or got a new device, then you can subscribe to a new plan or stick to Basic.")){
            SubscriptionService.shared.restorePurchases()
        }
    }
    func callGenerateToken(){
        generateToken(nil, successHandler: { (success) in
            print("token generated")
        }) { (error) in
            print("error in token generation")
        }
    }
    
    @objc func handleAppleEvent(_ event : NSAppleEventDescriptor, _ replyEvent : NSAppleEventDescriptor){
        if let url = URL.init(string: (event.paramDescriptor(forKeyword: keyDirectObject)?.stringValue)!){
            if (DBClientsManager.handleRedirectURL(url)) != nil{
                let authResult = DBClientsManager.handleRedirectURL(url)
                if authResult != nil {
                    if (authResult?.isSuccess())!{
                        print("Success! user is logged into dropbox")
                        let notification:Notification = Notification.init(name: Notification.Name(rawValue: DropboxSessionWasAuthorized))
                        NotificationCenter.default.post(notification)
                    }else if (authResult?.isCancel())!{
                        print("Authorization flow was manually canceled by user!")
                    }else if (authResult?.isError())!{
                        print("Error : \(authResult!)")
                    }
                }
            }
        }
    }
    
    func printFamilyNames() {
        let fontManager = NSFontManager.shared
        for family in fontManager.availableFontFamilies {
            if let fonts = fontManager.availableMembers(ofFontFamily: family) {
                for font in fonts{
                    print(font)
                }
            }
        }
    }
    
    func registerCustomFonts() {
        let paths = Bundle.main.paths(forResourcesOfType: "otf", inDirectory: "")
        for path in paths {
            let fontUrl = NSURL(fileURLWithPath: path)
            var errorRef: Unmanaged<CFError>?
            
            let success = CTFontManagerRegisterFontsForURL(fontUrl, .process, &errorRef)
            
            if (errorRef != nil) {
                let error = errorRef!.takeRetainedValue()
                print("Error registering custom font: \(error)")
            }
            print(success)
        }
    }
    
    //MARK: - ACTIONS
    @IBAction func dropBoxMenuClicked(_ sender: Any) {
        if let window = NSApplication.shared.mainWindow {
            let vc = window.contentViewController as? HomeVC
            vc?.dropboxAccountMenuTapped(sender)
        }
    }
    
    @IBAction func gDriveMenuClicked(_ sender: Any) {
        if let window = NSApplication.shared.mainWindow {
            let vc = window.contentViewController as? HomeVC
            vc?.googleDriveActionLinked(sender)
        }
    }
    
    @IBAction func moreAppsClicked(_ sender: Any) {
        openApps { (completion) in
        }
    }
    
    @IBAction func rateUsMenuClicked(_ sender: Any) {
        rateApp(appId: "1326081964") { (response) in
        }
    }
    
    @IBAction func openPreferences(_ sender: Any) {
        
        if(User.accessToken.isEmpty)
        {
            dialogOK(question: "No User Logged In", text: "")
            return
        }
        let storyBoard = NSStoryboard.init(name: NSStoryboard.Name.init(rawValue: "Main"), bundle: Bundle.main)
        let prefVC = storyBoard.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "PreferencesViewController"))
        if let window = NSApplication.shared.mainWindow {
            window.contentViewController?.presentViewControllerAsModalWindow(prefVC as! NSViewController)
        }
    }
    
    

}

// MARK: - SKPaymentTransactionObserver
extension AppDelegate: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue,
                      updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                handlePurchasingState(for: transaction, in: queue)
            case .purchased:
                handlePurchasedState(for: transaction, in: queue)
            case .restored:
                handleRestoredState(for: transaction, in: queue)
            case .failed:
                handleFailedState(for: transaction, in: queue)
            case .deferred:
                handleDeferredState(for: transaction, in: queue)
            }
        }
    }
    
    func handlePurchasingState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
        print("User is attempting to purchase product id: \(transaction.payment.productIdentifier)")
    }
    
    func handlePurchasedState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
        print("User purchased product id: \(transaction.payment.productIdentifier)")
        
        queue.finishTransaction(transaction)
        SubscriptionService.shared.uploadReceipt { (success) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: SubscriptionService.purchaseSuccessfulNotification, object: nil)
            }
        }
    }
    
    func handleRestoredState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
        print("Purchase restored for product id: \(transaction.payment.productIdentifier)")
        queue.finishTransaction(transaction)
        SubscriptionService.shared.uploadReceipt { (success) in
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: SubscriptionService.restoreSuccessfulNotification, object: nil)
            }
        }
    }
    
    func handleFailedState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
        print("Purchase failed for product id: \(transaction.payment.productIdentifier)")
        dialogOK(question: "Subscription Error", text:"Purchase failed for product id: \(transaction.payment.productIdentifier)")
    }
    
    func handleDeferredState(for transaction: SKPaymentTransaction, in queue: SKPaymentQueue) {
        print("Purchase deferred for product id: \(transaction.payment.productIdentifier)")
        dialogOK(question: "Purchase deferred", text:"Purchase deferred for product id: \(transaction.payment.productIdentifier)")
    }
}

