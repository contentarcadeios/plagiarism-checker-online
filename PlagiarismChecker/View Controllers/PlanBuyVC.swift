//
//  PlanBuyVC.swift
//  PlagiarismChecker
//
//  Created by macbook on 10/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa
import SwiftyStoreKit

class PlanBuyVC: NSViewController {
    //MARK: - PROPERTIES -
    var mouseLocation: NSPoint {
        return NSEvent.mouseLocation
    }
    
    var options: [Subscription]?
    
    var basicPlanFee = "$0.00"
    var ProPlanFee = "$14.99"
    var businessPlanFee = "$29.99"
    
    let planSelected = "PLAN SELECTED"
    let planChoose = "CHOOSE PLAN"
    let cancelSub = "CANCEL"
    
    var selectedTag : Int = 9
    
    //MARK: - OUTLETS -
    @IBOutlet weak var firstBox          : HoverView!
    @IBOutlet weak var secondBox        : HoverView!
    @IBOutlet weak var thirdBox         : HoverView!
    
    @IBOutlet weak var firstBoxTopContainer     : NSBox!
    @IBOutlet weak var secondBoxTopContainer    : NSBox!
    @IBOutlet weak var thirdBoxTopContainer     : NSBox!
    
    @IBOutlet weak var btnChoosePlanBasic       : DudNSButton!
    @IBOutlet weak var btnChoosePlanPro         : DudNSButton!
    @IBOutlet weak var btnChoosePlanBusiness    : DudNSButton!
    @IBOutlet weak var btnRestore               : DudNSButton!
    @IBOutlet weak var btnProSave10             : DudNSButton!
    @IBOutlet weak var btnProSave20             : DudNSButton!
    @IBOutlet weak var btnBusinessSave10        : DudNSButton!
    @IBOutlet weak var btnBusinessSave20        : DudNSButton!
    
    @IBOutlet weak var lbBasic          : NSTextField!
    @IBOutlet weak var lbBasicPrice     : NSTextField!
    @IBOutlet weak var lbPro            : NSTextField!
    @IBOutlet weak var lbProPrice       : NSTextField!
    @IBOutlet weak var lbBusiness       : NSTextField!
    @IBOutlet weak var lbBusinessPrice  : NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleOptionsLoaded(notification:)),
                                               name: SubscriptionService.optionsLoadedNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handlePurchaseSuccessfull(notification:)),
                                               name: SubscriptionService.purchaseSuccessfulNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleSessionId(notification:)),
                                               name: SubscriptionService.sessionIdSetNotification,
                                               object: nil)
        
         NotificationCenter.default.addObserver(self,
                                                selector: #selector(self.handleRestoreSession(notification:)),
                                                name: SubscriptionService.restoreSuccessfulNotification,
                                                object: nil)
    
        defaultSettings()
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        firstBox.wantsLayer = true
        firstBox.layer?.cornerRadius = 5.0
        
        secondBox.wantsLayer = true
        secondBox.layer?.cornerRadius = 5.0
        
        thirdBox.wantsLayer = true
        thirdBox.layer?.cornerRadius = 5.0
    }
    
    func defaultSettings(){
        self.view.wantsLayer = true
        self.view.backgroundColor = NSColor.white
        
        firstBox.delegate = self
        firstBox.backgroundColor = NSColor.white
        firstBox.borderColor  = NSColor.lightGray.withAlphaComponent(0.5)
        firstBoxTopContainer.backgroundColor = NSColor.init(hexString: "EEEEE")
        
        secondBox.delegate = self
        secondBox.backgroundColor = NSColor.white
        secondBox.borderColor  = NSColor.lightGray.withAlphaComponent(0.5)
        secondBoxTopContainer.backgroundColor = NSColor.init(hexString: "EEEEE")
        
        thirdBox.delegate = self
        thirdBox.backgroundColor = NSColor.white
        thirdBox.borderColor  = NSColor.lightGray.withAlphaComponent(0.5)
        thirdBoxTopContainer.backgroundColor = NSColor.init(hexString: "EEEEE")
        
        changeBackGround(firstBoxTopContainer, InMouse: false, inColor: "E32733", outColor: "EEEEEE")
        changeBackGround(secondBoxTopContainer, InMouse: false, inColor: "E32733", outColor: "EEEEEE")
        changeBackGround(thirdBoxTopContainer, InMouse: false, inColor: "E32733", outColor: "EEEEEE")
        
        changeButtonColors(btnChoosePlanBasic, InMouse: false, planSelected)
        changeButtonColors(btnChoosePlanPro, InMouse: false, cancelSub)
        changeButtonColors(btnChoosePlanBusiness, InMouse: false, planChoose)
        
        changeButtonColors(btnRestore, InMouse: false, "Restore Subscriptions")
        
        changeButtonColors(btnProSave10, InMouse: false, "SAVE 10%")
        changeButtonColors(btnProSave20, InMouse: false, "SAVE 20%")
        changeButtonColors(btnBusinessSave10, InMouse: false, "SAVE 10%")
        changeButtonColors(btnBusinessSave20, InMouse: false, "SAVE 20%")
        
        changeSubLabelColor(lbBasicPrice, InMouse: false, basicPlanFee)
        changeSubLabelColor(lbProPrice , InMouse: false, ProPlanFee)
        changeSubLabelColor(lbBusinessPrice, InMouse: false, businessPlanFee)
    }
    
    //MARK: - CUSTOM METHODS -
    @objc func handleRestoreSession(notification: Notification) {
        print("restore session")
         print("session id: \(SubscriptionService.shared.currentSubscription?.productId)")
        //dialogOK(question: "Restore", text: "Your subscriptions are restored successfully")
    }
    
   @objc func handleSessionId(notification: Notification) {
        OperationQueue.main.addOperation { [weak self] in
            print("session id: \(SubscriptionService.shared.currentSubscription?.productId)")
        }
    }
    
    @objc func handleOptionsLoaded(notification: Notification) {
        DispatchQueue.main.async { [weak self] in
            self?.options = SubscriptionService.shared.options
            print("options :\(self?.options!)")
        }
    }
    
    @objc func handlePurchaseSuccessfull(notification: Notification) {
        DispatchQueue.main.async { [weak self] in
            print("handlePurchaseSuccessfull \(SubscriptionService.shared.currentSubscription?.productId)")
            notifCenter.post(name: NSNotification.Name.init(kPackageChanged), object: nil, userInfo: nil)
//            Plan.proID = "\((self?.selectedTag)!)"
//            if let currentSub = SubscriptionService.shared.currentSubscription{
//                Plan.proHashID = "\(currentSub.level.hashValue)"
//                Plan.planName = "\(currentSub.level.hashValue)"
//                Plan.planQueries = "3000"
//                Plan.purchaseDate = (currentSub.purchaseDate.description)
//                Plan.expireDate = (currentSub.expiresDate.description)
//                Plan.planUsedQueries =  "0"
//                print("selected tag = \((self?.selectedTag)!)")
//                self?.buyPlan((self?.selectedTag)!)
//            }
        }
    }
    
    func getTextForButtons(_ tag : Int) -> String{
        var buttonTitle = ""
        switch tag {
        case 8:
            buttonTitle = "CHOOSE PLAN"
            break
        case 1:
            buttonTitle =  "CHOOSE PLAN"
            break
        case 2:
             buttonTitle =  "SAVE 10%"
            break
        case 3:
             buttonTitle = "SAVE 20%"
            break
        case 4:
             buttonTitle = "CHOOSE PLAN"
            break
        case 5:
             buttonTitle = "SAVE 10%"
            break
        case 6:
             buttonTitle = "SAVE 20%"
            break
        default:
            
            break
        }
        return buttonTitle
    }
    
    func getTextForSelectedButton(_ tag : Int, inMouse: Bool) -> String{
        if tag == 10 {
            return "RESTORE SUBSCRIPTION"
        }
        let selectedPlan = (Plan.proID as NSString).intValue
        var buttonTitle = ""
        if tag == selectedPlan {
            buttonTitle = "PLAN SELECTED"
        }else{
            buttonTitle = getTextForButtons(tag)
        }
        return buttonTitle
    }
    
    func compareViews(_ view : NSBox) -> Int{
        var tag = -1
        if view.isEqual(firstBox){
            tag = 0
        }else if view.isEqual(secondBox){
            tag = 1
        }else if view.isEqual(thirdBox){
            tag = 2
        }
        return tag
    }
    
    func changeBackGround(_ view : NSBox, InMouse: Bool, inColor : String, outColor : String){
        if InMouse {
            view.backgroundColor = NSColor.init(hexString: inColor)
        }else{
            view.backgroundColor = NSColor.init(hexString: outColor)
        }
    }
    
    func changeButtonColors(_ button : DudNSButton, InMouse : Bool , _ text : String){
        let buttonText = getTextForSelectedButton(button.tag, inMouse: InMouse)
        if InMouse {
            button.backgroundColor = NSColor.white
            button.setText(text: buttonText, font:NSFont.init(name: "Montserrat-Medium", size: 12.0)!, color: NSColor.init(hexString: "E32733") )
            button.setBorderAttributes(color: NSColor.init(hexString: "E32733"), borderWidth: 1.0, cornerRadius: 18.0)
        }else{
            button.backgroundColor = NSColor.init(hexString: "E32733")
            button.setText(text: buttonText, font:NSFont.init(name: "Montserrat-Medium", size: 12.0)!, color: NSColor.white)
            button.setBorderAttributes(color: NSColor.white, borderWidth: 1.0, cornerRadius: 18.0)
        }
    }
    
    func changeTitleLabelColors(_ label : NSTextField, InMouse : Bool){
        if InMouse {
            label.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        }else{
            label.textColor = #colorLiteral(red: 0.4234803021, green: 0.4235576987, blue: 0.4234754443, alpha: 1)
        }
    }
    
    func changeSubLabelColor(_ label : NSTextField, InMouse : Bool, _ string : String){
        let font:NSFont? = NSFont(name: "Montserrat-Regular", size:16)
        let fontSuper:NSFont? = NSFont(name: "Montserrat-Regular", size:11)
        let paragraph = NSMutableParagraphStyle.init()
        paragraph.alignment = .center
        
        if InMouse {
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: string, attributes: [.font:font!, .foregroundColor : NSColor.white,.paragraphStyle:paragraph])
            attString.setAttributes([.font:fontSuper!,.baselineOffset:string.count,.foregroundColor : NSColor.white , .paragraphStyle:paragraph], range: NSRange(location:0,length:1))
            label.attributedStringValue = attString
        }else{
            let attString:NSMutableAttributedString = NSMutableAttributedString(string: string, attributes: [.font:font!, .foregroundColor : NSColor.init(hexString: "AAAAAA"), .paragraphStyle:paragraph])
            attString.setAttributes([.font:fontSuper!,.baselineOffset:string.count,.foregroundColor : NSColor.init(hexString: "E32733") , .paragraphStyle:paragraph], range: NSRange(location:0,length:1))
            label.attributedStringValue = attString
        }
    }
    
    func applyColors(_ tagOfView:Int, inMouse : Bool){
        switch tagOfView {
        case 0:
            changeBackGround(firstBoxTopContainer, InMouse: inMouse, inColor: "E32733", outColor: "EEEEEE")
            changeButtonColors(btnChoosePlanBasic, InMouse: inMouse, planSelected)
            changeTitleLabelColors(lbBasic, InMouse: inMouse)
            changeSubLabelColor(lbBasicPrice, InMouse: inMouse, basicPlanFee)
            break
        case 1:
            changeBackGround(secondBoxTopContainer, InMouse: inMouse, inColor: "E32733", outColor: "EEEEEE")
            changeButtonColors(btnChoosePlanPro, InMouse: inMouse, planChoose)
            changeTitleLabelColors(lbPro, InMouse: inMouse)
            changeSubLabelColor(lbProPrice, InMouse: inMouse, ProPlanFee)
            changeButtonColors(btnProSave10, InMouse: inMouse, "SAVE 10%")
            changeButtonColors(btnProSave20, InMouse: inMouse, "SAVE 20%")
            break
        case 2:
            changeButtonColors(btnBusinessSave10, InMouse: inMouse, "SAVE 10%")
            changeButtonColors(btnBusinessSave20, InMouse: inMouse, "SAVE 20%")
            changeBackGround(thirdBoxTopContainer, InMouse: inMouse, inColor: "E32733", outColor: "EEEEEE")
            changeButtonColors(btnChoosePlanBusiness, InMouse: inMouse, planChoose)
            changeTitleLabelColors(lbBusiness, InMouse: inMouse)
            changeSubLabelColor(lbBusinessPrice, InMouse: inMouse, businessPlanFee)
            break
        default:
            break
        }
    }
    
    func configureView(_ inMouse : Bool , _ view : NSBox){
        changeBackGround(view, InMouse: inMouse, inColor: "E32733", outColor:"FFFFFF")
        applyColors(compareViews(view),inMouse: inMouse)
    }
    
    private func findPaymentIndex(withProductIdentifier identifier: String) -> Int {
        return options!.index(where: {$0.product.productIdentifier == identifier})!
    }
    
    func purchaseSubscription(_ purchase : RegisteredPurchase){
        
        self.options = SubscriptionService.shared.options
        if options != nil {
            let index = findPaymentIndex(withProductIdentifier: inAppPrefix + "." + purchase.rawValue)
            selectedTag = purchase.hashValue + 1
            SubscriptionService.shared.purchase(subscription:options![index])
        }
    }
    
    //MARK: - WEB API -
    func buyPlan(_ selectedTag : Int){
            refreshToken(self, successHandler: { (success) in
                self.buyPlanProcess(selectedTag)
            }, failure: { (error) in
                dialogOK(question: "Error", text: "Failed in requesting.")
            })
    }
    func buyPlanProcess(_ selectedTag : Int){
        let param = [
            "token" : userToken.token,
            "act" : "changeplan",
            "userid" : User.userID,
            "planid" : selectedTag
        ]
            as [String : Any]
        showHud(self)
        NetworkManager.makeUserRequest(param, successHandler: { (response) in
            hideHud(self)
            let res = response as! [String : Any]
            if res["responce_type"] as? String == "success" {
                print("Plan Active :\(res)")
            }else{
                if res["status"] as? Int == tokenExpirationCode{
                    generateToken(self, successHandler: { (success) in
                       self.buyPlanProcess(selectedTag)
                    }, failure: { (error) in
                        dialogOK(question: "Error", text:"Error Occured in processing")
                    })
                }else{
                    dialogOK(question: "Error", text: res["data"] as! String)
                }
            }
        }) { (error) in
            hideHud(self)
            dialogOK(question: "Error", text: (error?.localizedDescription)!)
        }
    }
    
    //MARK: - ACTIONS -
    @IBAction func btnBasicPlanClicked(_ sender: NSButton) {
       // openItunesForSubScriptionOption()
        print("butn text :\(sender.title)")
        if sender.title == planChoose {
            if dialogOKCancel(question: "Basic Plan", text: "Seems you have plan active at the moment. Pease cancel that into settings to move on basic. After plan expiration, basic will be active."){
               openItunesForSubScriptionOption()
            }
        }
    }
    
    @IBAction func btnProPlanClicked(_ sender: Any) {
       purchaseSubscription(.pro)
    }
    
    @IBAction func btnProPlan6MonthsClicked(_ sender: Any) {
        purchaseSubscription(.pro6mo)
    }
    
    @IBAction func btnProPlan3MonthsClicked(_ sender: Any) {
        purchaseSubscription(.pro3mo)
    }
    
    @IBAction func btnChooseBusinessPlanClicked(_ sender: Any) {
        purchaseSubscription(.business)
    }
    
    @IBAction func btnBusinessPlan3MoClicked(_ sender: Any) {
        purchaseSubscription(.business3mo)
    }
    
    @IBAction func btnBusinessPlan6MoClicked(_ sender: Any) {
        purchaseSubscription(.business6mo)
    }
    
    @IBAction func btnRestoreClicked(_ sender: Any) {
        SubscriptionService.shared.restorePurchases()
    }
}

//MARK : - HOVER DELEGATE -
extension PlanBuyVC : HoverViewDelegate {
    func hoverViewMouse(_ inMouse: Bool, sender: NSBox) {
        self.configureView(inMouse, sender)
    }
}
extension PlanBuyVC {
    func alertWithTitle(_ title: String, message: String) -> NSAlert {
        let alert: NSAlert = NSAlert()
        alert.messageText = title
        alert.informativeText = message
        alert.alertStyle = NSAlert.Style.informational
        return alert
    }
    
    func showAlert(_ alert: NSAlert, handler: ((NSApplication.ModalResponse) -> Void)? = nil) {
        if let window = NSApplication.shared.keyWindow {
            alert.beginSheetModal(for: window) { (response: NSApplication.ModalResponse) in
                handler?(response)
            }
        } else {
            let response = alert.runModal()
            handler?(response)
        }
    }
}
