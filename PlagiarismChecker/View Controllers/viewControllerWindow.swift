//
//  viewControllerWindow.swift
//  PlagiarismChecker
//
//  Created by macbook on 19/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa

class homeWindow: NSWindowController {
  
    //MARK: - VIEW LIFE CYCLE -
    override func windowDidLoad() {
        super.windowDidLoad()
        setInitialVC()
    }
    
    //MARK: - CUSTOM METHODS -
    func setInitialVC(){
        if let window = self.window as? INAppStoreWindow{
            if User.userID.isEmpty{
                let introVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "IntroVC")) as? IntroVC
                window.contentViewController = introVC
                window.title = "Welcome To Plagiarism Checker"
                window.titleBarHeight = 20
                window.titleBarDrawingBlock = { (drawAsMain,rect,edge,clipingPath) in
                    if drawAsMain {
                        let gradiant = NSGradient.init(starting:Theme.appNavBarColor(), ending:Theme.appNavBarColor())
                        gradiant?.draw(in: rect, angle: 90)
                    }
                }
            }else{
                let homeVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "HomeVC")) as? HomeVC
                window.contentViewController = homeVC
                self.applyNavBar()
            }
        }
    }
    
    func applyNavBar(){
        if let window = self.window as? INAppStoreWindow, let screen = window.screen {
            window.titleBarHeight = 50
            let screenRect = screen.visibleFrame
            
            let halfSize = NSSize(width:screenRect.size.width * 0.5, height: screenRect.height)
            window.setFrame(NSRect.init(origin: NSPoint(x: screenRect.midX - halfSize.width / 2, y: screenRect.maxY), size: halfSize), display: true)
            window.maxSize = screenRect.size
            window.minSize = halfSize
            
            window.titleBarDrawingBlock = { (drawAsMain,rect,edge,clipingPath) in
                if drawAsMain {
                    let gradiant = NSGradient.init(starting:Theme.appNavBarColor(), ending:Theme.appNavBarColor())
                    gradiant?.draw(in: rect, angle: 90)
                }
            }
            window.backgroundColor = NSColor.white
            window.delegate = self
            let titleView = window.titleBarView
            let segmentSize = NSSize(width: 104, height: 50)
            let logoHeaderImage = NSImage.init(named:NSImage.Name(rawValue: "logo_header"))
            let imgView : NSImageView?
            if #available(OSX 10.12, *) {
                imgView = NSImageView.init(image:logoHeaderImage!)
            } else {
                // Fallback on earlier versions
                imgView = NSImageView.init()
                imgView?.image = logoHeaderImage!
            }
            
            imgView?.frame = NSRect(x:NSMidX((titleView?.bounds)!) - segmentSize.width/2,
                                    y:NSMidY((titleView?.bounds)!) / segmentSize.height,
                                    width: segmentSize.width, height: segmentSize.height)
            titleView?.addSubview(imgView!)
        }
    }
}
//MARK: - WINDOW DELEGATE -
extension homeWindow : NSWindowDelegate{
    func windowWillClose(_ notification: Notification) {
        print("window wil close")
    }
    
    func windowDidResize(_ notification: Notification) {
        if let window = self.window as? INAppStoreWindow{
            if window.titleBarView.subviews.count > 0 {
                let segmentSize = NSSize(width: 104, height: 50)
                let imgView = window.titleBarView.subviews[0] as? NSImageView
                imgView?.frame.origin.x = NSMidX(window.titleBarView.bounds) - segmentSize.width/2
            }
        }
    }
    
    func window(_ window: NSWindow, willPositionSheet sheet: NSWindow, using rect: NSRect) -> NSRect {
        print("rect \(rect)")
        var newRect = NSRect.zero
        if let window1 = window as? INAppStoreWindow{
           newRect = NSRect.init(origin: CGPoint.init(x: rect.origin.x, y: window1.frame.size.height -   window1.titleBarView.frame.size.height), size: rect.size)
        }
        
        return newRect
    }
}
