//
//  driveListViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 21/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import ObjectiveDropboxOfficial
import GoogleAPIClientForREST
import AlamofireImage

protocol LoginDelegate: class {
    func loginSuccessfullyCompleted(_ type:FileSystemType) -> Void
    func pdfFileSuccessfullyDownloaded(_ path:String) -> Void
    func otherFileSuccessfullyDownloaded(_ path:String) -> Void
}

class loginExternalFiles: NSViewController {

    //MARK: - Properties/Outlets -
    @IBOutlet weak var linkButton       : NSButton!
    @IBOutlet weak var googleDriveBtn   : NSButton!
    
    @IBOutlet weak var lbFileTitle  : NSTextField!
    @IBOutlet weak var imageView    : NSImageView!
    
    var delegate            :LoginDelegate?
    var fileSystemType      :FileSystemType?
    var googleDriveAuthVC   :GTMOAuth2WindowController?
    
    //MARK: - VIEW LIFE CYCE -
    override func viewDidLoad() {
        super.viewDidLoad()
         if(fileSystemType == .googleDrive) {
            VLCGoogleDriveController.sharedInstance().startSession()
           self.lbFileTitle.stringValue = "Sign in to Google drive to view and download files."
            self.imageView.image = #imageLiteral(resourceName: "login-google-drive")
        }else {
            VLCDropboxController.sharedInstance().startSession()
            self.imageView.image = #imageLiteral(resourceName: "login-drop-box")
            self.lbFileTitle.stringValue = " Sign in to Dropbox to view and download files."
            NotificationCenter.default.addObserver(self, selector: #selector(sessionWasUpdated), name: NSNotification.Name(rawValue: DropboxSessionWasAuthorized), object: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
   @objc func sessionWasUpdated() -> Void {
        if(self.delegate != nil) {
            self.delegate?.loginSuccessfullyCompleted(.dropBox)
        }
        self.dismissViewController(self)
    }
    
    //MARK: - CUSTOM METHODS -
    func signInGoogle(){
        let scope = "https://www.googleapis.com/auth/plus.me"
        let windowController : GTMOAuth2WindowController?
        windowController = GTMOAuth2WindowController.controller(withScope: scope, clientID: kVLCGoogleDriveClientID, clientSecret: kVLCGoogleDriveClientSecret, keychainItemName: kKeychainItemName, resourceBundle: Bundle.main) as? GTMOAuth2WindowController
        
        let params = ["hl":"en"]
        windowController?.signIn.additionalAuthorizationParameters = params
        
        let html = "<html><body><div align=center>Loading sign-in page...</div></body></html>"
        windowController?.initialHTMLString = html
        
        windowController?.signIn.shouldFetchGoogleUserProfile = true
        windowController?.signInSheetModal(for: self.view.window, completionHandler: { (auth, error) in
            if error != nil {
                print("error :\(String(describing: error?.localizedDescription))")
                return
            }
            print("auth: \(String(describing: auth?.accessToken))")
            print("profile : \(String(describing: windowController?.signIn.userProfile))")
        })
    }
    
    func createDropboxAuth() -> Void {
        DBClientsManager.authorize(fromControllerDesktop: NSWorkspace.shared, controller: self, openURL: { (url) in
            print("url:\(url)")
            NSWorkspace.shared.open(url)
        })
    }
    
    func createGoogleAuthController() -> GTMOAuth2WindowController {
        googleDriveAuthVC = (GTMOAuth2WindowController.controller(withScope: "https://www.googleapis.com/auth/drive", clientID: kVLCGoogleDriveClientID, clientSecret: kVLCGoogleDriveClientSecret, keychainItemName: kKeychainItemName, resourceBundle: Bundle.main) as! GTMOAuth2WindowController)
        
        return googleDriveAuthVC!
    }
    
    func presentGdriveVC(_ googleDriveAuthVC : GTMOAuth2WindowController ){
        let params = ["hl":"en"]
        googleDriveAuthVC.signIn.additionalAuthorizationParameters = params
        let html = "<html><body><div align=center>Loading sign-in page...</div></body></html>"
        googleDriveAuthVC.initialHTMLString = html
        
        googleDriveAuthVC.signIn.shouldFetchGoogleUserProfile = true
        googleDriveAuthVC.signInSheetModal(for: self.view.window, completionHandler: { (auth, error) in
            if error != nil {
                print("error :\(String(describing: error?.localizedDescription))")
                dialogOK(question:(error?.localizedDescription)! , text: "")
                return
            }
            print("auth: \(String(describing: auth?.accessToken))")
            print("profile : \(String(describing:googleDriveAuthVC.signIn.userProfile))")
            VLCGoogleDriveController.sharedInstance().driveService.authorizer = auth
            if(self.delegate != nil) {
                self.delegate?.loginSuccessfullyCompleted(.googleDrive)
            }
            self.dismissViewController(self)
        })
    }
    
    //MARK: - ACTIONS -
    @IBAction func btnLoginTapped(_ sender: Any) {
        if(self.fileSystemType == .googleDrive) {
            let vc = self.createGoogleAuthController()
            self.presentGdriveVC(vc)
        }else if(self.fileSystemType ==  .dropBox) {
            self.createDropboxAuth()
        }
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        dismissViewController(self)
    }
}
