//
//  TabViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 10/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa

class TabViewController: NSTabViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        self.view.wantsLayer = true
        self.view.backgroundColor = NSColor.white
    }
}
