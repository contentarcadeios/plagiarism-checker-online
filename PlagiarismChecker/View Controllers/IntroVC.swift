//
//  IntroVC.swift
//  PlagiarismChecker
//
//  Created by macbook on 13/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa

class IntroVC: NSViewController {
    
    //MARK: - PROPERTIES -
    @IBOutlet weak var btnSkip           : DudNSButton!
    @IBOutlet weak var btnLogin          : DudNSButton!
    @IBOutlet weak var btnUpgrade        : DudNSButton!
    @IBOutlet weak var restoreButton     : DudNSButton!
    @IBOutlet weak var btnForgetPassword : DudNSButton!
    
    //MARK: - VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        self.title = "Welcome To Plagiarism Checker"
        notifCenter.addObserver(self, selector: #selector(userLoginNotifRecieved(_:)), name: NSNotification.Name.init(USER_LOGGED_IN), object: nil)
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        
        self.view.wantsLayer = true
        self.view.backgroundColor = Theme.appNavBarColor()
        
        setupButtonLayout(self.btnForgetPassword, "Forgot Password?")
        setupButtonLayout(self.restoreButton, "Restore Subscriptions")
        setupButtonLayout(self.btnUpgrade, "REGISTER")
        setupButtonLayout(self.btnLogin, "LOGIN")
    }
    
    @objc func userLoginNotifRecieved(_ notif : Notification){
        self.btnSkipClicked(self.btnSkip)
    }
    
    func setupButtonLayout(_ button : DudNSButton, _ text : String){
        button.isBordered = false
        button.setText(text: text, font:NSFont.init(name: "Montserrat-Regular", size: 14.0)!, color: Theme.appNavBarColor())
        button.setBorderAttributes(color: NSColor.clear, borderWidth: 0, cornerRadius: 15)
        button.setButtonType(.momentaryChange)
        button.setBackgroundColor(color: NSColor.white)
    }
    
    func presentTabVC(selected Index: Int){
        let tabVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "TabViewController")) as? TabViewController
        tabVC?.selectedTabViewItemIndex = Index
        self.presentViewControllerAsModalWindow(tabVC!)
    }
    
    func presentForgotPasswordDialog(){
        let a = NSAlert()
        a.messageText = "Please enter your email."
        a.addButton(withTitle: "Send")
        a.addButton(withTitle: "Cancel")
        
        let inputTextField = NSTextField(frame: NSRect(x: 0, y: 0, width: 300, height: 24))
        inputTextField.placeholderString = "Email"
        a.accessoryView = inputTextField
        
        if (a.runModal() == .alertFirstButtonReturn){
            if !inputTextField.stringValue.isEmail {
                if dialogOKCancel(question: "Error", text: "Please enter valid email"){
                    presentForgotPasswordDialog()
                }
            }else{
                self.callForgetPasswordAPI(inputTextField.stringValue)
            }
        }
    }
    
    //MARK: - WEB APIS -
    func callForgetPasswordAPI(_ email : String){
        refreshToken(self, successHandler: { (success) in
            self.processForgetPasswordAPI(email)
        }, failure: { (error) in
            dialogOK(question: "Error", text: "Failed in requesting.")
        })
    }
    
    func processForgetPasswordAPI(_ email : String){
        let param = [
            "email" : email
        ]
        
        showHud(self)
        NetworkManager.forgetPassword(param, successHandler: { (response) in
            hideHud(self)
            let res = response as! [String : Any]
            if res["responce_type"] as? String == "success" {
                dialogOK(question: "Forget Password", text:"Email sent at your email address. Please check that for further instructions.")
            }
            else{
                dialogOK(question: "Error", text: res["data"] as! String)
            }
        }) { (error) in
            hideHud(self)
            dialogOK(question: "Error", text: (error?.localizedDescription)!)
        }
    }
    
    
    
    //MARK: - ACTIONS -
    @IBAction func btnRegisterClicked(_ sender: Any) {
        presentTabVC(selected: 1)
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        presentTabVC(selected: 0)
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        if let window = NSApplication.shared.mainWindow {
            let homeVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "HomeVC")) as? HomeVC
            window.contentViewController = homeVC
            if let windowController = window.windowController as? homeWindow {
                windowController.applyNavBar()
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        }
    }
    
    @IBAction func btnForgetPasswordClicked(_ sender: Any) {
        self.presentForgotPasswordDialog()
    }
    
    @IBAction func btnRestoreClicked(_ sender: Any) {
        //openItunesForSubScriptionOption()
        //SubscriptionService.shared.restorePurchases()
    }
    
    @IBAction func btnupgradeClicked(_ sender: Any) {
        let planVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "PlanBuyVC")) as? PlanBuyVC
        
        self.presentViewControllerAsModalWindow(planVC!)
    }
}
