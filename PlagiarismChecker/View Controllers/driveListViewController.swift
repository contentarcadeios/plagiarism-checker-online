//
//  driveListViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 21/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import ObjectiveDropboxOfficial
import GoogleAPIClientForREST
import AlamofireImage

class loginExternalFiles: NSViewController {

    //MARK: - Properties/Outlets -
    @IBOutlet weak var linkButton: NSButton!
    @IBOutlet weak var googleDriveBtn: NSButton!
    @IBOutlet weak var lbFileTitle: NSTextField!
    
    //var delegate:LoginDelegate?
    var controller:VLCCloudStorageController?
    var currentPath:String = ""
    var googleSelectedFile:GTLRDrive_File?
    var fileSystemType:FileSystemType?
    var selectedFile:Any?
    var dropboxSelectedFile:DBFILESMetadata?
    
    //MARK: - VIEW LIFE CYCE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
       // getStateOfAccountLink()x
    }
    
    
    //MARK: - CUSTOM METHODS -
    func signInGoogle(){
        let scope = "https://www.googleapis.com/auth/plus.me"
        let windowController : GTMOAuth2WindowController?
        windowController = GTMOAuth2WindowController.controller(withScope: scope, clientID: kVLCGoogleDriveClientID, clientSecret: kVLCGoogleDriveClientSecret, keychainItemName: kKeychainItemName, resourceBundle: Bundle.main) as? GTMOAuth2WindowController
        
        let params = ["hl":"en"]
        windowController?.signIn.additionalAuthorizationParameters = params
        
        let html = "<html><body><div align=center>Loading sign-in page...</div></body></html>"
        windowController?.initialHTMLString = html
        
        windowController?.signIn.shouldFetchGoogleUserProfile = true
        windowController?.signInSheetModal(for: self.view.window, completionHandler: { (auth, error) in
            if error != nil {
                print("error :\(String(describing: error?.localizedDescription))")
                return
            }
            print("auth: \(String(describing: auth?.accessToken))")
            print("profile : \(String(describing: windowController?.signIn.userProfile))")
        })
    }
    
    func getStateOfAccountLink(){
        if (DBClientsManager.authorizedClient() != nil) || (DBClientsManager.authorizedTeamClient() != nil){
            self.linkButton.title = "Unlink dropbox"
        }else{
            self.linkButton.title = "Link dropbox"
        }
    }
    
    func processAccountLinking(){
        if (DBClientsManager.authorizedClient() != nil) || (DBClientsManager.authorizedTeamClient() != nil){
            DBClientsManager.unlinkAndResetClients()
            self.linkButton.title = "Link dropbox"
        }else{
            DBClientsManager.authorize(fromControllerDesktop: NSWorkspace.shared, controller: self, openURL: { (url) in
                print("url:\(url)")
                NSWorkspace.shared.open(url)
                
            })
        }
    }
    //MARK: - ACTIONS -
    @IBAction func linkDropBoxTapped(_ sender: Any) {
        processAccountLinking()
    }
    
    @IBAction func gDriveButtonTapped(_ sender: Any) {
        signInGoogle()
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        dismissViewController(self)
    }
    
}
