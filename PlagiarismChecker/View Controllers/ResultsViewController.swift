//
//  ResultsViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 19/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import Quartz

//@objc protocol ResultsViewControllerDelegate {
//    func passResults(result: PlegiarismResult)
//}

class ResultsViewController: NSViewController {
    
    //MARK: - OUTLETS -
    @IBOutlet weak var tableView        : NSTableView!
    
    @IBOutlet weak var tableScrollView  : NSScrollView!
    
    @IBOutlet weak var plgBox               : NSView!
    @IBOutlet weak var uniqueBox            : NSView!
    @IBOutlet weak var resultBox            : NSView!
    @IBOutlet weak var plgContainerView     : NSView!
    @IBOutlet weak var uniqueContainerView  : NSView!
    @IBOutlet weak var resultView           : NSView!
    
    @IBOutlet weak var lbPlagResult     : NSTextField!
    @IBOutlet weak var lbUniqueResult   : NSTextField!
    @IBOutlet weak var lbUniqueRatio    : NSTextField!
    @IBOutlet weak var lnPlagResult     : NSTextField!
    
    @IBOutlet weak var uniqueContainerViewWidthConstraint   : NSLayoutConstraint!
    @IBOutlet weak var PlagContainerViewWidthConstraint     : NSLayoutConstraint!
    
    @IBOutlet weak var btnReport: DudNSButton!
    
    //MARK: - PROPERTIES -
    var plegiarisedDetails:[PlagiarisedDetails] = [PlagiarisedDetails]()
    var fieldLb = NSTextField()
    var results:PlegiarismResult?
    
    //MARK: - VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        tableView.target = self
        tableView.selectionHighlightStyle = .none
    }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        
        plgBox.backgroundColor = #colorLiteral(red: 0.9282422662, green: 0.195631057, blue: 0.2137799859, alpha: 1)
        resultView.backgroundColor = #colorLiteral(red: 0.9282422662, green: 0.195631057, blue: 0.2137799859, alpha: 1)
        uniqueBox.backgroundColor = #colorLiteral(red: 0, green: 0.6922230721, blue: 0.3826937675, alpha: 1)
        plgContainerView.backgroundColor = #colorLiteral(red: 0.9282422662, green: 0.195631057, blue: 0.2137799859, alpha: 1)
        uniqueContainerView.backgroundColor = #colorLiteral(red: 0, green: 0.6922230721, blue: 0.3826937675, alpha: 1)
        
        if (Plan.proID as NSString).intValue != 8{
            self.btnReport.isHidden = false
        }
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        btnReport.isBordered = false
        
        let monstFont = NSFont.init(name: "Montserrat-Regular", size: 11.0)!
        
        btnReport.setText(text: "Report", font:monstFont, color: NSColor.white)
        
        lbPlagResult.font =  monstFont
        lbUniqueResult.font = monstFont
        
        lbUniqueRatio.font = monstFont
        lnPlagResult.font = monstFont
        
        
        btnReport.backgroundColor = Theme.appNavBarColor()
        btnReport.setBorderAttributes(color: NSColor.clear, borderWidth: 0.0, cornerRadius: 20.0)
        btnReport.setButtonType(NSButton.ButtonType.momentaryChange)
        
        plgBox.wantsLayer = true
        plgBox.layer?.cornerRadius = plgBox.frame.size.width/2
        
        uniqueBox.wantsLayer = true
        uniqueBox.layer?.cornerRadius = uniqueBox.frame.size.width/2
        
        plgContainerView.wantsLayer = true
        plgContainerView.layer?.cornerRadius = 23
        
        uniqueContainerView.wantsLayer = true
        uniqueContainerView.layer?.cornerRadius = 23
        
        resultView.wantsLayer = true
        resultView.layer?.cornerRadius = 23
        
        resultBox.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        tableScrollView.wantsLayer = true
        tableScrollView.layer?.cornerRadius = 15
        
        tableView.wantsLayer = true
        tableView.layer?.cornerRadius = 15.0
    }
    
    //MARK: - PDF METHODS -
    public func generateAndOpenPDFHandler() {
        let pdf = SimplePDF(pdfTitle: "Plagiarism Checker", authorName: "https://smallseotools.com/")
        
        self.addDocumentCover(pdf: pdf)
        self.addDocumentContent(pdf: pdf)
        self.addHeadersFooters(pdf: pdf)
        
        // here we may want to save the pdf somewhere or show it to the user
        let tmpPDFPath = pdf.writePDFWithTableOfContents()
        
        let savePanel = NSSavePanel()
        savePanel.allowedFileTypes = ["pdf"]
        savePanel.begin { (response) in
            if response == .OK{
                if let url = savePanel.url {
                    do {
                        let filePath = url.path
                        let data = try Data.init(contentsOf: URL.init(fileURLWithPath: tmpPDFPath))
                        try data.write(to: URL.init(fileURLWithPath: filePath))
                    }catch {
                        print(error)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                            dialogOK(question: "Erorr!", text: error.localizedDescription)
                        })
                    }
                }
            }
        }
    }
    
    private func addDocumentCover(pdf: SimplePDF) {
        let titleFont = NSFont.boldSystemFont(ofSize: 48)
        let paragraphAlignment = NSMutableParagraphStyle()
        paragraphAlignment.alignment = .center
        
        let documentTitle = NSMutableAttributedString(string: "Report")
        let titleRange = NSMakeRange(0, documentTitle.length)
        
        documentTitle.addAttribute(NSAttributedStringKey.font, value: titleFont, range: titleRange)
        documentTitle.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphAlignment, range: titleRange)
        
        let documentATitle = NSMutableAttributedString(string: "\n\n\n\n\nPlaigairism Checker")
        let titleARange = NSMakeRange(0, documentATitle.length)
        
        documentATitle.addAttribute(NSAttributedStringKey.font, value: titleFont, range: titleARange)
        documentATitle.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphAlignment, range: titleRange)
        
        pdf.addAttributedString(attrString: documentATitle)
        pdf.addAttributedString(attrString: documentTitle)
        // we don't want anymore text on this page, so we force a page break
        pdf.startNewPage()
    }
    
    private func addDocumentContent(pdf: SimplePDF) {
        let regularFont = NSFont.systemFont(ofSize: 12)
        let boldFont = NSFont.boldSystemFont(ofSize: 12)
        let alignment = NSMutableParagraphStyle()
        alignment.alignment = NSTextAlignment.center
        
        let resultAttrString = NSMutableAttributedString(string:"Results\n")
       
        resultAttrString.addAttributes([NSAttributedStringKey.font :NSFont.boldSystemFont(ofSize: 15) , NSAttributedStringKey.paragraphStyle : alignment, NSAttributedStringKey.underlineColor : NSColor.black, NSAttributedStringKey.underlineStyle : 1.0], range: NSMakeRange(0, resultAttrString.length))
        
        pdf.addAttributedString(attrString: resultAttrString)
        
        let leftAlignment = NSMutableParagraphStyle()
        leftAlignment.alignment = NSTextAlignment.left
    
        if let plagVal = results?.plagPercent?.stringValue , let uniqueVal = results?.uniquePercent?.stringValue{
            let plagString = "Plagiarised: \(plagVal)%"
            let uniqString = "Unique: \(uniqueVal)%"

            let leftHeaderAttrString = NSMutableAttributedString(string:"\(plagString)                          \(uniqString)")
            leftHeaderAttrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: leftAlignment, range: NSMakeRange(0, leftHeaderAttrString.length))
            leftHeaderAttrString.addAttribute(NSAttributedStringKey.font, value: regularFont, range: NSMakeRange(0, leftHeaderAttrString.length))
          
            leftHeaderAttrString.addAttributes([NSAttributedStringKey.font :boldFont, NSAttributedStringKey.underlineColor : NSColor.black, NSAttributedStringKey.underlineStyle : 1.0], range: leftHeaderAttrString.mutableString.range(of: "Plagiarised"))
     
            leftHeaderAttrString.addAttributes([NSAttributedStringKey.font : boldFont,NSAttributedStringKey.underlineColor : NSColor.black, NSAttributedStringKey.underlineStyle : 1.0], range: leftHeaderAttrString.mutableString.range(of: "Unique"))
          
            pdf.addAttributedString(attrString: leftHeaderAttrString)
        }
        let queryAttrString = NSMutableAttributedString.init(string: "\nQueries:")
        queryAttrString.addAttributes([NSAttributedStringKey.font: boldFont, NSAttributedStringKey.paragraphStyle : leftAlignment],
                                      range: NSMakeRange(0, queryAttrString.length))
        queryAttrString.addAttributes([ NSAttributedStringKey.underlineColor : NSColor.black, NSAttributedStringKey.underlineStyle : 1.0],
                                      range: queryAttrString.mutableString.range(of: "Queries"))
        
        pdf.addAttributedString(attrString: queryAttrString)
        
        let linkcolor =  NSColor.init(hexString: "#0000EE")
        for i in (0..<plegiarisedDetails.count){
            let queryString = self.getQueryString(i)
            let detail = plegiarisedDetails[i]
            if let url = detail.url {
                let queryAttributedString = NSMutableAttributedString.init(string: "\n\(queryString)\n")
                
                let urlAttributeString = NSMutableAttributedString.init(string: "\(url)")
                urlAttributeString.addAttributes([NSAttributedStringKey.underlineStyle : 1.0, NSAttributedStringKey.underlineColor : linkcolor, NSAttributedStringKey.foregroundColor : linkcolor], range: NSRange.init(location: 0, length: urlAttributeString.length))
                
                queryAttributedString.append(urlAttributeString)
                pdf.addAttributedString(attrString: queryAttributedString)
            }
        }
    }
    
    private func addHeadersFooters(pdf: SimplePDF) {
        let regularFont = NSFont.systemFont(ofSize: 8)
        let boldFont = NSFont.boldSystemFont(ofSize: 8)
        let leftAlignment = NSMutableParagraphStyle()
        leftAlignment.alignment = NSTextAlignment.left
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        let dateString = dateFormatter.string(from: Date())
        
        // add some document information to the header, on left
        let leftHeaderString = "Author: Plagiarism Checker\nDate/Time: \(dateString)"
        let leftHeaderAttrString = NSMutableAttributedString(string: leftHeaderString)
        leftHeaderAttrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: leftAlignment, range: NSMakeRange(0, leftHeaderAttrString.length))
        leftHeaderAttrString.addAttribute(NSAttributedStringKey.font, value: regularFont, range: NSMakeRange(0, leftHeaderAttrString.length))
        leftHeaderAttrString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: leftHeaderAttrString.mutableString.range(of: "Author:"))
        leftHeaderAttrString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: leftHeaderAttrString.mutableString.range(of: "Date/Time:"))
        let header = SimplePDF.HeaderFooterText(type: .Header, pageRange: NSMakeRange(1, Int.max), attributedString: leftHeaderAttrString)
        pdf.headerFooterTexts.append(header)
        
        // add a logo to the header, on right
        let logoPath = Bundle.main.path(forResource:"appLogo" , ofType: "png")
        // NOTE: we can specify either the image or its path
        let rightLogo = SimplePDF.HeaderFooterImage(type: .Header, pageRange: NSMakeRange(1, Int.max),
                                                    imagePath: logoPath!, image:nil, imageHeight: 35, alignment: .right)
        pdf.headerFooterImages.append(rightLogo)
        
        // add page numbers to the footer (center aligned)
        let centerAlignment = NSMutableParagraphStyle()
        centerAlignment.alignment = .center
        let footerString = NSMutableAttributedString(string: "\(pdf.kPageNumberPlaceholder) of \(pdf.kPagesCountPlaceholder)")
        footerString.addAttribute(NSAttributedStringKey.paragraphStyle, value: centerAlignment, range: NSMakeRange(0, footerString.length))
        let footer = SimplePDF.HeaderFooterText(type: .Footer, pageRange: NSMakeRange(1, Int.max), attributedString: footerString)
        pdf.headerFooterTexts.append(footer)
        
        // add a link to your app may be
        let link = NSMutableAttributedString(string: "https://smallseotools.com/")
        link.addAttribute(NSAttributedStringKey.paragraphStyle, value: leftAlignment, range: NSMakeRange(0, link.length))
        let appLinkFooter = SimplePDF.HeaderFooterText(type: .Footer, pageRange: NSMakeRange(1, Int.max), attributedString: link)
        pdf.headerFooterTexts.append(appLinkFooter)
        
        // NOTE: we can specify either the image or its path
        let footerImage = SimplePDF.HeaderFooterImage(type: .Footer, pageRange: NSMakeRange(1, Int.max),
                                                      imagePath: logoPath!, image:nil, imageHeight: 20, alignment: .right)
        pdf.headerFooterImages.append(footerImage)
    }
    //MARK: - CUSTOM METHODS -
    @objc func doubleActionOnTableView(_ sender : Any){
        if plegiarisedDetails.count > 0 {
            let detail = plegiarisedDetails[tableView.clickedRow]
            print("detail url \(detail.url!)")
            if let url = URL(string:detail.url!), NSWorkspace.shared.open(url) {
                print("default browser was successfully opened")
            }
        }
    }
    
    func setResults(){
        if results != nil {
            for detail in (self.results?.details)! {
                if detail.unique == "false" && detail.plagiarisedDetails != nil {
                    plegiarisedDetails += detail.plagiarisedDetails!
                    
                }
            }
            self.tableView.reloadData()
            setupResultView()
        }
    }
    
    func setupResultView(){
        if let plagVal = results?.plagPercent?.stringValue , let uniqueVal = results?.uniquePercent?.stringValue{
            self.lbPlagResult.stringValue = "\(plagVal)%"
            self.lbUniqueResult.stringValue = "\(uniqueVal)%"
            
            let uniqConstant = CGFloat(((uniqueVal as NSString).floatValue)/100.0) *
                self.resultView.frame.width
            let plgConstant = CGFloat(((plagVal as NSString).floatValue)/100.0) *
                self.resultView.frame.width
            print ("uni constant \(uniqConstant) and plg \(plgConstant)")
            self.uniqueContainerViewWidthConstraint.constant = uniqConstant
            self.PlagContainerViewWidthConstraint.constant = plgConstant
        }
    }
    
    func getQueryString(_ row : Int) -> String{
        var queryString = ""
        let detail = plegiarisedDetails[row]
        var selectedDetail:QueryDetails?
        for rDetail in (self.results?.details)! {
            if rDetail.unique == "false" && rDetail.plagiarisedDetails != nil {
                var isBreak:Bool = false
                for pDetail in rDetail.plagiarisedDetails! {
                    if(pDetail == detail) {
                        selectedDetail = rDetail
                        queryString = rDetail.query!
                        isBreak = true
                        break
                    }
                }
                if(isBreak) {
                    break
                }
            }
            
        }
        return queryString
    }
    
    @IBAction func btnReportClicked(_ sender: Any) {
        generateAndOpenPDFHandler()
    }
    
    @objc func clickWeb(_ sender: NSButton) {
        if plegiarisedDetails.count > 0 {
            let detail = plegiarisedDetails[sender.tag]
            print("detail url \(detail.url!)")
            if let url = URL(string:detail.url!), NSWorkspace.shared.open(url) {
                print("default browser was successfully opened")
            }
        }
    }
}

//MARK: - TABLEVIEW DELEGATE / DATASOURCE -
extension ResultsViewController : NSTableViewDelegate, NSTableViewDataSource{
    func numberOfRows(in tableView: NSTableView) -> Int {
        return plegiarisedDetails.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let tblViewRow:plagResultTableCellView = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "plagResultTableCellView"), owner: self) as! plagResultTableCellView
        
        tblViewRow.lbQuery.stringValue = self.getQueryString(row)
        tblViewRow.viewQuery.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.937254902, alpha: 1)
        
        tblViewRow.lbQuery.lineBreakMode = .byWordWrapping
    tblViewRow.lbQuery.setContentCompressionResistancePriority(NSLayoutConstraint.Priority(rawValue: 250), for: .horizontal)
        
        tblViewRow.lbQuery.toolTip = self.getQueryString(row)
        tblViewRow.btnWeb.tag = row
        tblViewRow.btnWeb.action = #selector(clickWeb(_:))
        
        let detail = plegiarisedDetails[row]
        if let url = detail.url {
            tblViewRow.btnWeb.toolTip = url
        }
        return tblViewRow
    }
}
