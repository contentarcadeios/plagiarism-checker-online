//
//  ResultsWindowController.swift
//  PlagiarismChecker
//
//  Created by macbook on 19/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa

class ResultsWindowController: NSWindowController {
    var results:PlegiarismResult?
    override func windowDidLoad() {
        super.windowDidLoad()
        // --- set windows --- //
        setupWindows()
    }
    
    func setResults(){
        let contentVC = self.contentViewController as? ResultsViewController
        contentVC?.results = results!
        contentVC?.setResults()
    }

    func setupWindows(){
        if let window = self.window as? INAppStoreWindow, let screen = window.screen {
            window.titleBarHeight = 50
            
            let offsetFromLeftOfScreen: CGFloat = 200
            let offsetFromTopOfScreen: CGFloat = 200
            //2.
            let screenRect = screen.visibleFrame
            //3.
            let newOriginY = screenRect.maxY - window.frame.height - offsetFromTopOfScreen
            //4.
            let halfSize = NSSize(width:screenRect.size.width * 0.5, height: screenRect.height)
            window.setFrame(NSRect.init(origin: NSPoint(x: offsetFromLeftOfScreen, y: newOriginY), size: halfSize), display: true)
            window.maxSize = screenRect.size
            window.minSize = halfSize
            
            window.titleBarDrawingBlock = { (drawAsMain,rect,edge,clipingPath) in
                if drawAsMain {
                    let gradiant = NSGradient.init(starting:Theme.appNavBarColor(), ending:Theme.appNavBarColor())
                    gradiant?.draw(in: rect, angle: 90)
                }
            }
            window.delegate = self
            window.backgroundColor = NSColor.white
            let titleView = window.titleBarView
            let segmentSize = NSSize(width: 104, height: 50)
            let logoHeaderImage = NSImage.init(named:NSImage.Name(rawValue: "logo_header"))
            
            let imgView : NSImageView?
            if #available(OSX 10.12, *) {
                imgView = NSImageView.init(image:logoHeaderImage!)
            } else {
                // Fallback on earlier versions
                imgView = NSImageView.init()
                imgView?.image = logoHeaderImage!
            }
            
            imgView?.frame = NSRect(x:NSMidX((titleView?.bounds)!) - segmentSize.width/2,
                                   y:NSMidY((titleView?.bounds)!) / segmentSize.height,
                                   width: segmentSize.width,
                                   height: segmentSize.height)
            titleView?.addSubview(imgView!)
        }
    }
}

extension ResultsWindowController : NSWindowDelegate{
    func windowWillClose(_ notification: Notification) {
        let notification:Notification = Notification.init(name: Notification.Name(rawValue: kWindowClosed))
        NotificationCenter.default.post(notification)
    }
    
    func windowDidResize(_ notification: Notification) {
        if let window = self.window as? INAppStoreWindow{
            if window.titleBarView.subviews.count > 0 {
                let segmentSize = NSSize(width: 104, height: 50)
                let imgView = window.titleBarView.subviews[0] as? NSImageView
                imgView?.frame.origin.x = NSMidX(window.titleBarView.bounds) - segmentSize.width/2
            }
        }
    }
    
    func window(_ window: NSWindow, willPositionSheet sheet: NSWindow, using rect: NSRect) -> NSRect {
        print("rect \(rect)")
        var newRect = NSRect.zero
        if let window1 = window as? INAppStoreWindow{
            newRect = NSRect.init(origin: CGPoint.init(x: rect.origin.x, y: window1.frame.size.height -   window1.titleBarView.frame.size.height), size: rect.size)
        }
        return newRect
    }
}
