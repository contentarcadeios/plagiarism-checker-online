//
//  driveListVC.swift
//  PlagiarismChecker
//
//  Created by macbook on 22/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import GoogleAPIClientForREST
import ObjectiveDropboxOfficial
import AlamofireImage

class driveListVC: NSViewController {
    
    //MARK:- OUTLETS -
    @IBOutlet weak var lbTitle          : NSTextField!
    @IBOutlet weak var tableView        : NSTableView!
    @IBOutlet weak var tableScrollView  : NSScrollView!
    @IBOutlet weak var btnBack          : NSButton!
    @IBOutlet weak var spinner          : NSProgressIndicator!
    
    //MARK:- PROPERTIES -
    var delegate            :   LoginDelegate?
    var controller          :   VLCCloudStorageController?
    var currentPath         :   String = ""
    var googleSelectedFile  :   GTLRDrive_File?
    var fileSystemType      :   FileSystemType?
    var selectedFile        :   Any?
    var dropboxSelectedFile :   DBFILESMetadata?
    var hud                 :   MBProgressHUD?
    
    var firstTime: Bool  = false
    
    //MARK:- VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        tableView.target = self
        tableView.doubleAction = #selector(selectRow)
        tableView.selectionHighlightStyle = .none
        
        if(fileSystemType == .googleDrive) {
            controller = VLCGoogleDriveController.sharedInstance()
            self.lbTitle.stringValue = "Google Drive"
        }else {
            controller = VLCDropboxController.sharedInstance()
            self.lbTitle.stringValue = "DropBox"
        }
        
        controller?.delegate = self
        
        self.tableView.reloadData()
        self.refresh()
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        tableScrollView.wantsLayer = true
        tableScrollView.layer?.cornerRadius = 15
    }
    
    //MARK:- CUSTOM METHODS -
    func logout(){
        let response = dialogOKCancel(question: "Are you sure, you want to signout?", text:"")
        if response {
            print("user selected ok")
            controller?.logout()
            self.dismissViewController(self)
        }
    }
    
    func refresh() -> Void {
        self.showAnimator()       // MBProgressHUD.showAdded(to: self.tableView, animated: true)
        if currentPath.isEmpty || currentPath == "/" {
            btnBack.isHidden = true
        }else{
            btnBack.isHidden = false
        }
        controller?.requestDirectoryListing(atPath: currentPath)
    }
    
    @objc func selectRow(){
        self.selectTableRow(tableView.clickedRow)
    }
    
    func showAnimator(){
        spinner.isHidden = false
        spinner.startAnimation(self.view)
    }
    
    func hideAnimator(){
        spinner.stopAnimation(self.view)
        spinner.isHidden = true
    }
    
    //MARK: - ACTIONS -
    @IBAction func btnCloseClicked(_ sender: Any) {
        dismissViewController(self)
    }
    
    @IBAction func btnSignOutClicked(_ sender: Any) {
        self.logout()
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        if(self.currentPath != "" && self.currentPath != "/") {
            self.currentPath = (self.currentPath as NSString).deletingLastPathComponent
            print("current path : \(currentPath)")
            self.refresh()
        }
    }
}

//MARK:- TABLEVIEW DELEGATE / DATASOURCE -
extension driveListVC : NSTableViewDelegate, NSTableViewDataSource{
    func numberOfRows(in tableView: NSTableView) -> Int {
        var numberOfRows = 0
        if(controller != nil && controller?.currentListFiles != nil) {
            numberOfRows = (controller?.currentListFiles.count)!
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
         let tblViewRow:downloadFilesCellView = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "downloadFilesCellView"), owner: self) as! downloadFilesCellView
        
        if(controller != nil && controller?.currentListFiles != nil) {
            let item:Any? = self.controller?.currentListFiles[row]
            var name = ""
            if(self.fileSystemType == .googleDrive && item is GTLRDrive_File) {
                let fItem = item  as! GTLRDrive_File
                if(fItem.mimeType == "application/vnd.google-apps.folder") {
                    tblViewRow.fileIconImageView.image = #imageLiteral(resourceName: "btn-folder")
                    tblViewRow.btnDownload.isHidden = true
                    tblViewRow.verticalLine.isHidden = true
                }else{
                    tblViewRow.fileIconImageView.image = #imageLiteral(resourceName: "btn-file")
                    tblViewRow.btnDownload.isHidden = false
                    tblViewRow.verticalLine.isHidden = false
                }
                name = fItem.name!
            }else if(self.fileSystemType == .dropBox && item is DBFILESMetadata){
                if(item is DBFILESFolderMetadata) {
                    tblViewRow.fileIconImageView.image = #imageLiteral(resourceName: "btn-folder")
                    tblViewRow.btnDownload.isHidden = true
                    tblViewRow.verticalLine.isHidden = true
                }else{
                    tblViewRow.fileIconImageView.image = #imageLiteral(resourceName: "btn-file")
                    tblViewRow.btnDownload.isHidden = false
                    tblViewRow.verticalLine.isHidden = false
                }
                name = (item as! DBFILESMetadata).name
            }
            tblViewRow.btnDownload.tag = row
            tblViewRow.btnDownload.action = #selector(self.downloadFile(_:))
            tblViewRow.lbFileName.stringValue = name
        }
        return tblViewRow
    }
}

//MARK:- CLOUD STORADE DELEGATE -
extension driveListVC: VLCCloudStorageDelegate {
    func mediaListUpdated() {
       self.hideAnimator()
       // MBProgressHUD.hide(for: self.tableView, animated: true)
        self.tableView.reloadData()
    }
    
    func currentProgressInformation(_ progress: CGFloat, currentFile file: Any!) {
        print("progress:\(progress)")
        if(hud != nil) {
            hud?.progress = Float(progress)
        }
    }
    
    func operation(withProgressInformationStopped filePath: String!) {
        self.hideAnimator()
        MBProgressHUD.hide(for: self.tableView, animated: true)
        hud?.removeFromSuperview()
        hud = nil
        if(filePath != nil) {
            if((filePath as NSString).pathExtension == "pdf") {
                if(self.delegate != nil) {
                    self.delegate?.pdfFileSuccessfullyDownloaded(filePath)
                    self.dismissViewController(self)
                }
            }else {
                if(self.delegate != nil) {
                    self.delegate?.otherFileSuccessfullyDownloaded(filePath)
                    self.dismissViewController(self)
                }
            }
        }
        print(filePath)
    }
    
    func stopedWithError(_ error: Error!) {
        if(error.localizedDescription == "The operation couldn’t be completed. (com.google.HTTPStatus error 401.)") {
            self.controller?.logout()
            if(self.delegate != nil) {
                self.delegate?.loginSuccessfullyCompleted(.googleDrive)
            }
            self.dismissViewController(self)
        }else  {
            self.hideAnimator()
          //  MBProgressHUD.hide(for: self.tableView, animated: true)
            self.tableView.reloadData()
        }
    }
    
    @objc func selectTableRow(_ row : Int){
        if(self.controller != nil && self.controller?.currentListFiles != nil && row < (self.controller?.currentListFiles.count)!) {
            self.selectedFile = self.controller?.currentListFiles[row]
            if(self.fileSystemType == .googleDrive && self.selectedFile is GTLRDrive_File) {
                let fItem = self.selectedFile  as! GTLRDrive_File
                if(fItem.mimeType == "application/vnd.google-apps.folder") {
                    if(self.currentPath != "") {
                        self.currentPath += "/"
                    }
                    self.currentPath += (fItem.identifier)!
                    self.refresh()
                }
            }else if(self.fileSystemType == .dropBox && self.selectedFile is DBFILESMetadata) {
                let fItem = self.selectedFile  as! DBFILESMetadata
                if(fItem is DBFILESFolderMetadata) {
                    self.currentPath += "/"+(fItem.name)
                    self.refresh()
                }
            }
        }
    }
    
    @objc func downloadFile(_ sender : NSButton){
        let row = sender.tag
        if(self.controller != nil && self.controller?.currentListFiles != nil && row < (self.controller?.currentListFiles.count)!) {
            self.selectedFile = self.controller?.currentListFiles[row]
            self.showDownloadOptions()
        }
    }
    
    func showDownloadOptions() -> Void {
        let check = dialogOKCancel(question: "Download?", text: "")
        if check {
            hud = MBProgressHUD.showAdded(to: self.tableView, animated: true)
            hud?.mode = MBProgressHUDModeAnnularDeterminate
            if(self.fileSystemType == .googleDrive && self.controller is
                VLCGoogleDriveController) {
                (self.controller as! VLCGoogleDriveController).downloadFile(toDocumentFolder: self.selectedFile as! GTLRDrive_File)
            }else if(self.fileSystemType == .dropBox && self.controller is VLCDropboxController){
                (self.controller as! VLCDropboxController).downloadFile(toDocumentFolder: self.selectedFile as! DBFILESMetadata)
            }
        }
    }
}
