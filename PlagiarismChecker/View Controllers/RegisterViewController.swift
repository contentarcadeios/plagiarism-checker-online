//
//  RegisterViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 10/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa


class RegisterViewController: NSViewController {
    //MARK: - PROPERTIES
    var countriesArray = [[String : Any]]()
    var countryNames = [String]()
    var countryCodes = [String]()
    
    //MARK: - OUTLETS
    @IBOutlet weak var countryPopupButton       : NSPopUpButton!
    @IBOutlet weak var phoneCodeButton          : NSPopUpButton!
    
    @IBOutlet weak var passwordBox              : NSBox!
    @IBOutlet weak var emailbox                 : NSBox!
    @IBOutlet weak var addressBox               : NSBox!
    @IBOutlet weak var fullNameBox              : NSBox!
    @IBOutlet weak var phoneNumberBox           : NSBox!
    @IBOutlet weak var selectCountryBox         : NSBox!
    @IBOutlet weak var selectPhoneCodeBox       : NSBox!
    @IBOutlet weak var CityBox                  : NSBox!
    
    @IBOutlet weak var btnRegister: DudNSButton!
    
    @IBOutlet weak var tfFullName        : NSTextField!
    @IBOutlet weak var tfPhoneNumber     : NSTextField!
    @IBOutlet weak var tfPassword        : NSSecureTextField!
    
    @IBOutlet weak var tfEmail           : NSTextField!
    
    //MARK: - PROPERTIES
    override func viewDidLoad() {
        super.viewDidLoad()
        // View Background color
        self.view.wantsLayer = true
        self.view.backgroundColor = NSColor.white
        
        // phone textfield restrcicts only to number
        let formattor = IntegerFormatter.init()
        self.tfPhoneNumber.formatter = formattor
        
        // Read JSON
        readJson()
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        makeRoundCornerBox(fullNameBox, 18.0)
        makeRoundCornerBox(emailbox, 18.0)
        makeRoundCornerBox(passwordBox, 18.0)
        makeRoundCornerBox(phoneNumberBox, 18.0)
        makeRoundCornerBox(selectCountryBox, 18.0)
        makeRoundCornerBox(selectPhoneCodeBox, 18.0)
        makeRoundCornerBox(CityBox, 18.0)
        makeRoundCornerBox(addressBox, 18.0)
        
        btnRegister.wantsLayer = true
        btnRegister.setBackgroundColor(color: Theme.appNavBarColor())
        btnRegister.setText(text: "REGISTER", font:NSFont.init(name: "Montserrat-Medium", size: 14.0)!, color: NSColor.white)
        btnRegister.isBordered = false
        btnRegister.layer?.cornerRadius = 18.0
        
    }
   
    //MARK: - WEB APIS -
    func registerUser(){
        let param = [
            "act" : "reg",
            "name" : self.tfFullName.stringValue ,
            "email" : self.tfEmail.stringValue,
            "password" : self.tfPassword.stringValue
        ]
        showHud(self)
        NetworkManager.signUpUser(param, successHandler: { (response) in
            hideHud(self)
            let res = response as! [String : Any]
            if res["responce_type"] as? String == "success" {
                if let parentViewController = self.parent as? NSTabViewController {
                    dialogOK(question: "Registration", text: "Account verification email has been sent to your email. Kindly verify in order to login.")
                    
                    let loginVC = parentViewController.childViewControllers[0] as? LoginViewController
                    loginVC?.tfEmail.stringValue = self.tfEmail.stringValue
                    
                    parentViewController.selectedTabViewItemIndex = 0
                }
            }
            else{
                dialogOK(question: "Error", text: res["data"] as! String)
            }
            
        }) { (error) in
            hideHud(self)
            dialogOK(question: "Error", text: (error?.localizedDescription)!)
        }
    }
    
     //MARK: - CUSTOM METHODS -
    func validateFields() -> Bool {
        if tfFullName.stringValue.isEmpty {
            dialogOK(question: "Error", text: "Please enter Full Name")
            return false
        }else if tfEmail.stringValue.isEmpty {
            dialogOK(question: "Error", text: "Please enter email")
            return false
        }else if !tfEmail.stringValue.isEmail{
            dialogOK(question: "Error", text: "Please enter valid email")
            return false
        }else if tfPassword.stringValue.isEmpty{
            dialogOK(question: "Error", text: "Password should not be empty")
            return false
        }else if tfPassword.stringValue.count < 6 {
            dialogOK(question: "Error", text: "Password should be minimum 6 characters")
            return false
        }
        return true
    }
    func makeRoundCornerBox(_ box : NSBox,_ radius : CGFloat){
        box.borderColor = #colorLiteral(red: 0.8940202594, green: 0.8941736817, blue: 0.8940106034, alpha: 1)
        box.cornerRadius = radius
    }
    
    private func readJson() {
        do {
            if let file = Bundle.main.url(forResource: "countries", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print("dictionary :\(object)")
                } else if let object = json as? [Any] {
                    // json is an array
                    countriesArray = object as! [[String : Any]]
                    
                    countryPopupButton.removeAllItems()
                    phoneCodeButton.removeAllItems()
                    
                    if countriesArray.count > 0 {
                        for county in countriesArray{
                            let name = county["name"] as! String
                            let dialCode = county["dial_code"] as! String
                            
                            countryNames.append(name)
                            countryCodes.append(dialCode)
                            
                            countryPopupButton.addItem(withTitle: name)
                            phoneCodeButton.addItem(withTitle:dialCode )
                        }
                    }
                } else {
            print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func findIndexInCountries(_ string : String) -> Int{
        return countryPopupButton.indexOfItem(withTitle: string)
    }
    
    func findIndexInPhoneCode(_ string : String) -> Int{
        return phoneCodeButton.indexOfItem(withTitle: string)
    }
    
    func getCountryObject(_ key : String,_ value : String) -> [String : Any]?{
        var countryObj : [String:Any]?
        for country in countriesArray where country[key] as? String == value {
            print("country: \(country)")
            countryObj = country
        }
        return countryObj!
    }
    
    //MARK: - ACTIONS -
    @IBAction func btnRegisterClicked(_ sender: Any) {
        if validateFields() {
              refreshToken(self, successHandler: { (success) in
                    self.registerUser()
                }, failure: { (error) in
                    dialogOK(question: "Error", text: "Failed in requesting.")
                })
            }
    }
    
    @IBAction func countryPopupButtonSelectedChanged(_ sender: NSPopUpButton) {
        if  let countryObj = getCountryObject("name", sender.titleOfSelectedItem!){
            let index = findIndexInPhoneCode(countryObj["dial_code"] as! String)
            phoneCodeButton.selectItem(at: index)
        }
    }
    
    @IBAction func phoneCodePopUpButtonSelectedChanged(_ sender: NSPopUpButton) {
        if  let countryObj = getCountryObject("dial_code", sender.titleOfSelectedItem!){
            let index = findIndexInCountries(countryObj["name"] as! String)
            countryPopupButton.selectItem(at: index)
        }
    }
}
