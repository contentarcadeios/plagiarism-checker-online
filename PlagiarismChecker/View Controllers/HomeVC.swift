//
//  ViewController.swift
//  PlagiarismChecker
//
//  Created by Talha Ejaz on 12/15/17.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import SwiftSoup

class HomeVC: NSViewController {
    //MARK: - Properties -
    @IBOutlet var textView              : PlaceholderTextView!
  
    @IBOutlet weak var textScrollView   : NSScrollView!
    @IBOutlet weak var lbWarning        : NSTextField!
    
    @IBOutlet weak var btnReadURL       : DudNSButton!
    @IBOutlet weak var btnChangePlan    : DudNSButton!
    @IBOutlet weak var btnClear         : DudNSButton!
    @IBOutlet weak var btnCheckPlag     : DudNSButton!
    
    var options: [Subscription]?
    
    //MARK: - VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(resultWindowClosed(_:)), name: NSNotification.Name(rawValue: kWindowClosed), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.urlReadNotifRecieved(_:)), name: NSNotification.Name(rawValue: kURLRead), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setWarningText), name: NSNotification.Name(rawValue: kPackageChanged), object: nil)
        
        self.setWarningText()
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @objc func resultWindowClosed(_ notif : Notification){
        self.view.window!.standardWindowButton(NSWindow.ButtonType.closeButton)!.isEnabled = true
    }
    
    @objc func urlReadNotifRecieved(_ notif : Notification){
        self.processStripOutHTML((notif.userInfo!["url"] as? String)!)
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        // Set up btn
        btnChangePlan.isBordered = false
        btnChangePlan.setText(text: "Change Plan", font:NSFont.init(name: "Montserrat-Regular", size: 11.0)!, color: Theme.appNavBarColor())
        btnChangePlan.setButtonType(NSButton.ButtonType.momentaryChange)
        
        btnReadURL.isBordered = false
        btnReadURL.setText(text: "Read from URL", font:NSFont.init(name: "Montserrat-Regular", size: 11.0)!, color: Theme.appNavBarColor())
        btnReadURL.setButtonType(NSButton.ButtonType.momentaryChange)
        
        btnCheckPlag.setText(text: "Check Plagiarism", font:NSFont.init(name: "Montserrat-Regular", size: 14.0)!, color: NSColor.white)
        btnCheckPlag.wantsLayer = true
        btnCheckPlag.isBordered = false
        btnCheckPlag.setBackgroundColor(color: Theme.appNavBarColor())
        btnCheckPlag.layer?.cornerRadius = 20.0
        
        btnClear.setText(text: "Clear", font: NSFont.init(name: "Montserrat-Regular", size: 14.0)!, color: NSColor.white)
        btnClear.wantsLayer = true
        btnClear.isBordered = false
        btnClear.setBackgroundColor(color: Theme.appNavBarColor())
        btnClear.layer?.cornerRadius = 20.0
        
        self.textScrollView.wantsLayer = true
        self.textScrollView.layer?.cornerRadius = 15.0
        self.textView.textContainerInset = NSSize(width:10, height:10)
        self.textView.placeholderAttributedString = NSAttributedString.init(string: MSG_PLACEHOLDER_TEXT)
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
    }
    
    //MARK: - CUSTOM METHODS -
    @objc func setWarningText(){
        self.lbWarning.stringValue = "Words Limit: NO more than \(Plan.words) per search*"
        if (Plan.proID as NSString).intValue != 8{
            self.btnReadURL.isHidden = false
        }
    }
    
    @objc func processStripOutHTML(_ urlString : String){
        var html : String?
        guard let myURL = URL(string: urlString) else {
            dialogOK(question:"Error" , text: "\(urlString) doesn't seem to be a valid URL")
            return
        }
        do {
            html = try String(contentsOf: myURL, encoding: .utf8)
            guard let doc: Document = try? SwiftSoup.parse(html!) else{return}//parse html
            guard let text = try? doc.text() else {return}
            self.clearTextView()
            self.textView.textStorage?.append(NSAttributedString.init(string:text))
        }catch Exception.Error(let type, let message){
            dialogOK(question:"Error" , text:message)
        }catch{
            dialogOK(question:"Error" , text: "\(urlString) is not readable.")
        }
    }
    
    func processURL (_ url : URL){
        let fileExtension = url.path.getPathExtension()
        switch checkExtension(fileExtension) {
        case 1:
            let image = NSImage(byReferencing:url)
            self.readTextFromImage(image)
            break
        case 2:
            self.readTextFromPdf(url)
            break
        case 3 :
            do {
                let text = try String(contentsOf: url, encoding: .utf8)
                self.clearTextView()
                self.textView.textStorage?.append(NSAttributedString.init(string:text))
            }
            catch {/* error handling here */
                print("error")
                dialogOK(question: MSG_TITLE_ERROR, text: MSG_CANT_READ)
            }
            break
            
        case 4 :
            self.readTextFromDocFile(url)
            break
        default:
            break
        }
    }
    
    func checkExtension(_ fileExtenstion : String) -> Int {
        var type : Int = -1
        switch fileExtenstion {
        case "jpg","png","pct", "bmp", "tiff":
            type = 1
            break
        case "pdf":
            type = 2
            break
        case "rtfd","txt","rtf":
            type = 3
            break
        case "doc","docx":
            type = 4
            break
        default:
            break
        }
        return type
    }
    
    func showAlert(_ title : String, _ msg : String){
        let a = NSAlert.init()
        a.messageText = title
        a.informativeText = msg
        a.addButton(withTitle: "Read")
        a.addButton(withTitle: "Cancel")
        
        a.alertStyle = .informational
        
        a.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
            if modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn {
                print("Document deleted")
            }
        })
    }
    
    func readTextFromImage(_ image : NSImage){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ParsedResult.getParsedTextFromImage(image, successHandler: { (result) in
            print(result)
            MBProgressHUD.hide(for: self.view, animated: true)
            if result.parsedResult != nil {
                if (result.parsedResult?.count)! > 0 && result.parsedResult?[0].text != "" {
                    print("text : \(String(describing: result.parsedResult?[0].text))")
                    if let text = result.parsedResult?[0].text{
                        self.clearTextView()
                        self.textView.textStorage?.append(NSAttributedString.init(string:text))
                    }
                    else{
                        dialogOK(question: MSG_TITLE_ERROR, text: MSG_CANT_READ)
                    }
                }
                else {
                    dialogOK(question: MSG_TITLE_ERROR, text: result.errorMessage!)
                }
            }
            else if (result.errorMessage != nil ) {
                dialogOK(question: MSG_TITLE_ERROR, text: result.errorMessage!)
            }else{
                dialogOK(question: MSG_TITLE_ERROR, text:MSG_CANT_READ)
            }
            
        }, failure: { (error) in
            print(error!)
            MBProgressHUD.hide(for: self.view, animated: true)
            dialogOK(question: MSG_TITLE_ERROR, text: MSG_NETWORK_ERROR)
        })
    }
    
    func readTextFromPdf(_ url : URL){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        ParsedResult.getParsedTextFromPDF(url.path, successHandler: { (result) in
            print(result)
            MBProgressHUD.hide(for: self.view, animated: true)
            if result.parsedResult != nil {
                if (result.parsedResult?.count)! > 0 && result.parsedResult?[0].text != "" {
                    print("text : \(String(describing: result.parsedResult?[0].text))")
                    if let text = result.parsedResult?[0].text{
                        self.clearTextView()
                        self.textView.textStorage?.append(NSAttributedString.init(string:text))
                    }else{
                        dialogOK(question: MSG_TITLE_ERROR, text: MSG_CANT_READ)
                    }
                }
                else {
                    dialogOK(question: MSG_TITLE_ERROR, text: MSG_CANT_READ)
                }
            }
            else if (result.errorMessage != nil) {
                dialogOK(question: MSG_TITLE_ERROR, text: result.errorMessage!)
            }else{
                dialogOK(question: MSG_TITLE_ERROR, text:MSG_CANT_READ)
            }
        }, failure: { (error) in
            print(error!)
            MBProgressHUD.hide(for: self.view, animated: true)
            dialogOK(question: MSG_TITLE_ERROR, text: MSG_NETWORK_ERROR)
        })
    }
    
    func readTextFromDocFile(_ url : URL){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        DocFileReader.readDocFile(url.path, successHandler: { (fileReader) in
            print(fileReader)
            MBProgressHUD.hide(for: self.view, animated: true)
            if fileReader.data != nil && fileReader.responceType == "success" {
                if let text = fileReader.data {
                    self.clearTextView()
                    self.textView.textStorage?.append(NSAttributedString.init(string:text))
                }else{
                    dialogOK(question: MSG_TITLE_ERROR, text: MSG_CANT_READ)
                }
            }
            else {
                dialogOK(question: MSG_TITLE_ERROR, text: fileReader.data!)
            }
            
        }, failure: { (error) in
            print(error)
            MBProgressHUD.hide(for: self.view, animated: true)
            dialogOK(question: MSG_TITLE_ERROR, text: MSG_CANT_READ)
        })
    }
    
    func openPanel(){
        if let url = NSOpenPanel().selectUrl {
            print("file selected: \(url.path)")
            do {
                self.processURL(url)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
        } else {
            print("file selection was canceled")
        }
    }
    
    func modelLoginVC(_ fileSystem : FileSystemType){
        let storyBoard = NSStoryboard.init(name: NSStoryboard.Name(rawValue: "Main"), bundle: Bundle.main)
        let vc = storyBoard.instantiateController(withIdentifier:NSStoryboard.SceneIdentifier(rawValue: "loginExternalFiles")) as? loginExternalFiles
        vc?.fileSystemType = fileSystem
        vc?.delegate = self
        if let window = NSApplication.shared.mainWindow {
            window.contentViewController?.presentViewControllerAsSheet(vc!)
        }
    }
    
    func modelFilesVC(_ fileSystem : FileSystemType){
        let storyBoard = NSStoryboard.init(name: NSStoryboard.Name(rawValue: "Main"), bundle: Bundle.main)
        let vc = storyBoard.instantiateController(withIdentifier:NSStoryboard.SceneIdentifier(rawValue: "driveListVC")) as? driveListVC
        vc?.fileSystemType = fileSystem
        vc?.delegate = self
        if let window = NSApplication.shared.mainWindow {
            window.contentViewController?.presentViewControllerAsSheet(vc!)
        }
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier?.rawValue == SEGUE_RESULTS {
            if let nextVC = segue.destinationController as? ResultsWindowController{
                let result = sender as? PlegiarismResult
                nextVC.results = result!
                nextVC.setResults()
            }
        }
    }
    
    func clearTextView(){
        self.textView.textStorage?.setAttributedString(NSAttributedString.init(string: ""))
    }
    
    //MARK: - WEB APIS METHODS -
    func updateQueries(){
        if userToken.token.isEmpty{
            generateToken(self, successHandler: { (success) in
                self.updateQueriesProcess()
            }, failure: { (error) in
                dialogOK(question: "Error", text: "Failed in requesting.")
            })
        }else{
            self.updateQueriesProcess()
        }
    }
    
    func updateQueriesProcess(){
//        let param = [
//            "token" : userToken.token,
//            "act" : "updatequeries",
//            "userid" : User.userID,
//            "planid" : Plan.proID
//        ]
//        showHud(self)
//        NetworkManager.makeUserRequest(param, successHandler: { (response) in
//            hideHud(self)
//            let res = response as! [String : Any]
//            if res["responce_type"] as? String == "success" {
//                print("query update successfully:\(res)")
//                let data = res["data"] as! [String : Any]
//                let usedQueries = "\(data["usedqueries"]!)"
//                Plan.planUsedQueries = usedQueries
//            }else{
//                if res["status"] as? Int == 401{
//                    refreshToken(self, successHandler: { (success) in
//                        self.updateQueriesProcess()
//                    }, failure: { (error) in
//                        dialogOK(question: "Error", text:"Error Occured in processing")
//                    })
//                }else if let errorText = res["data"] as? String{
//
//                    dialogOK(question: "Error", text: errorText)
//                }
//            }
//        }) { (error) in
//            hideHud(self)
//            dialogOK(question: "Error", text: (error?.localizedDescription)!)
//        }
    }
    
    //MARK: - ACTIONS -
    @IBAction func btnReadFromURLClicked(_ sender: Any) {
        let readVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "URLReadVC")) as? URLReadVC
        readVC?.delegate = self
        self.presentViewControllerAsSheet(readVC!)
    }
    
    @IBAction func btnChangePlanClicked(_ sender: Any) {
        let planVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "PlanBuyVC")) as? PlanBuyVC
        
        self.presentViewControllerAsModalWindow(planVC!)
    }
    
    @IBAction func btnClearTapped(_ sender: Any) {
        clearTextView()
        
    }
    
    @IBAction func googleDriveActionLinked(_ sender: Any) {
        if(VLCGoogleDriveController.sharedInstance().isAuthorized) {
            print("gDrive is attached")
            self.modelFilesVC(FileSystemType.googleDrive)
        }else{
            self.modelLoginVC(FileSystemType.googleDrive)
        }
    }
    
    @IBAction func dropboxAccountMenuTapped(_ sender: Any) {
        if(VLCDropboxController.sharedInstance().isAuthorized) {
            self.modelFilesVC(FileSystemType.dropBox)
        }else{
            self.modelLoginVC(FileSystemType.dropBox)
        }
    }
    
    @IBAction func btnOpenTapped(_ sender: Any) {
        openPanel()
    }
    
    @IBAction func btnCheckPlagairismTapped(_ sender: NSButton) {
        
        if Plan.planUsedQueries >  Plan.planQueries{
            dialogOK(question: "Error", text: "You have reached queries limit, please check plans.")
            return
        }

        if let textStorage = textView.textStorage {
            if textStorage.characters.count > 0 {
                print("text count > 0")
                textView.isEditable = false
                showHud(self)
                Plegiarism.getPlegiarismResults(textStorage.string, successHandler: { (plegairism) in
                    self.textView.isEditable = true
                    hideHud(self)
                    
                    if (plegairism.data) != nil{
                       
                            Plan.planUsedQueries += 1
                        self.view.window!.standardWindowButton(NSWindow.ButtonType.closeButton)!.isEnabled = false
                        self.performSegue(withIdentifier:NSStoryboardSegue.Identifier(rawValue: SEGUE_RESULTS), sender: plegairism.data)
                    }
                    else{
                            dialogOK(question:"Error", text: "Words limit exceeded or some error occured in processing. Please try again later.")
                    }
                    
                }, failure: { (error) in
                    self.textView.isEditable = true
                    hideHud(self)
                    dialogOK(question: (error?.localizedDescription)!, text: "")
                })
            }
            else{
                dialogOK(question: "Please add text", text: "")
            }
        }
    }
}

extension HomeVC: LoginDelegate {
    func loginSuccessfullyCompleted(_ type: FileSystemType) {
        if(type == .googleDrive) {
            print("login type is : googleDrive")
            self.modelFilesVC(FileSystemType.googleDrive)
        }else {
            print("login type is : dropbox")
            self.modelFilesVC(FileSystemType.dropBox)
        }
    }
    
    func pdfFileSuccessfullyDownloaded(_ path: String) {
        print("path:\(path)")
        self.readTextFromPdf(URL.init(string: path.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!)
    }
    
    func otherFileSuccessfullyDownloaded(_ path: String) {
        self.processURL(URL.init(string: path.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!)
    }
    
    private func findPaymentIndex(withProductIdentifier identifier: String) -> Int {
        return options!.index(where: {$0.product.productIdentifier == identifier})!
    }
}

extension HomeVC : URLReadVCDelegate {
    func urlAdded(_ urlString: String) {
        print("url: \(urlString)")
        self.processStripOutHTML(urlString)
    }
}
