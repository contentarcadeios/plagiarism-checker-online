//
//  LoginViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 10/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa

class LoginViewController: NSViewController {

    //MARK: - PROPERTIES
    @IBOutlet weak var emailBox     : NSBox!
    @IBOutlet weak var passwordBox  : NSBox!
    
    @IBOutlet weak var tfEmail      : NSTextField!
 
    @IBOutlet weak var btnLogin     : DudNSButton!
    @IBOutlet weak var tfPassword   : NSSecureTextField!
    
    //MARK: VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        self.view.wantsLayer = true
        self.view.backgroundColor = NSColor.white
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        emailBox.borderColor = #colorLiteral(red: 0.8940202594, green: 0.8941736817, blue: 0.8940106034, alpha: 1)
        emailBox.cornerRadius = 18.0
        
        passwordBox.borderColor = #colorLiteral(red: 0.8940202594, green: 0.8941736817, blue: 0.8940106034, alpha: 1)
        passwordBox.cornerRadius = 18.0
        
        btnLogin.wantsLayer = true
        btnLogin.setBackgroundColor(color: Theme.appNavBarColor())
        btnLogin.setText(text: "LOGIN", font:NSFont.init(name: "Montserrat-Medium", size: 14.0)!, color: NSColor.white)
        btnLogin.isBordered = false
        btnLogin.layer?.cornerRadius = 18.0
    }
    
    //MARK: - CUSTOM METHODS -
    func validateFields() -> Bool {
        if tfEmail.stringValue.isEmpty{
            dialogOK(question: "Error", text: "Please enter email")
            return false
        }
        if !tfEmail.stringValue.isEmail{
            dialogOK(question: "Error", text: "Please enter valid email")
            return false
        }
        if tfPassword.stringValue.isEmpty{
            dialogOK(question: "Error", text: "Password should not be empty")
            return false
        }
        if tfPassword.stringValue.count < 6 {
            dialogOK(question: "Error", text: "Password should be minimum 6 characters")
            return false
        }
        return true
    }
    
    //MARK: - WEB API METHODS
    func loginUser(){
        let param = [
            "email" : self.tfEmail.stringValue,
            "password" : self.tfPassword.stringValue
        ]
        showHud(self)
        NetworkManager.loginUser(param, successHandler: { (response) in
            hideHud(self)
            print(response as! [String:Any])
            let res = response as! [String : Any]
            if res["responce_type"] as? String == "success"
            {
                if let data = res["data"] as? [String : Any]
                {
                    if let user = data["userData"] as? [String : Any]
                    {
                        User.mapData(data: user)
                    }
                    Plan.mapData(data: data)
                    
                    self.dismissViewController(self.parent!)
                    notifCenter.post(name: NSNotification.Name.init(USER_LOGGED_IN), object: nil)
                }
            }
            else
            {
                    dialogOK(question: "Error", text: res["data"] as! String)
            }
                
            }) { (error) in
                hideHud(self)
                dialogOK(question: "Error", text: (error?.localizedDescription)!)
        }
    }

    //MARK: - ACTIONS -
    @IBAction func btnLoginClicked(_ sender: Any) {
        if validateFields(){
            self.loginUser()
        }
    }
}
