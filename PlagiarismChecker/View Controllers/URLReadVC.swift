//
//  URLReadVC.swift
//  PlagiarismChecker
//
//  Created by macbook on 13/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa

protocol URLReadVCDelegate {
  func urlAdded(_ urlString : String)
}

class URLReadVC: NSViewController {
    //MARK: - OUTLETS -
    @IBOutlet weak var tfURL    : NSTextField!
    
    //MARK: - PROPERTIES -
    var delegate                : URLReadVCDelegate?
    
    //MARK: - VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        tfURL.wantsLayer = true
    }
    
    //MARK: - ACTIONS -
    @IBAction func btnReadClicked(_ sender: Any) {
        if tfURL.stringValue.isEmpty{
            tfURL.backgroundColor = NSColor.red
        }else{
            if tfURL.stringValue.isValidURL(){
                tfURL.backgroundColor = NSColor.white
                notifCenter.post(name: NSNotification.Name.init(rawValue: kURLRead), object: nil, userInfo: ["url":tfURL.stringValue])
               // self.delegate?.urlAdded(tfURL.stringValue)
                self.dismissViewController(self)
            }else{
                tfURL.backgroundColor = NSColor.red
            }
        }
    }
}
