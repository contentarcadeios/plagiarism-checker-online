//
//  PreferencesViewController.swift
//  PlagiarismChecker
//
//  Created by macbook on 09/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa


class PreferencesViewController: NSViewController {
    
    //MARK: - PROPERTIES
    @IBOutlet weak var btnUpgrade       : DudNSButton!
    @IBOutlet weak var btnLogin         : DudNSButton!
    @IBOutlet weak var btnLogout        : DudNSButton!
    
    @IBOutlet weak var userLoginView    : NSView!
    
    @IBOutlet weak var lbUserName       : NSTextField!
    @IBOutlet weak var lbEmail          : NSTextField!
    @IBOutlet weak var lbJoinDate       : NSTextField!
    
    @IBOutlet weak var lbPackage            : NSTextField!
    @IBOutlet weak var lbTotalQueries       : NSTextField!
    @IBOutlet weak var lbUsedQueries        : NSTextField!
    @IBOutlet weak var lbRemainingQueries   : NSTextField!
    @IBOutlet weak var lbPurchaseDAte       : NSTextField!
    @IBOutlet weak var lbExpireDate         : NSTextField!
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        self.view.wantsLayer = true
        self.view.backgroundColor = NSColor.white
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getUserDetailAndUpdateUI), name: NSNotification.Name(rawValue: kPackageChanged), object: nil)
        self.updateScreen()
        self.getUserDetailAndUpdateUI()
        
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        userLoginView.wantsLayer = true
        userLoginView.backgroundColor = NSColor.white
        
        btnUpgrade.setBackgroundColor(color: Theme.appNavBarColor())
        btnUpgrade.setText(text: "UPGRAGE PLAN", font:NSFont.init(name: "Montserrat-Medium", size: 12.0)!, color: NSColor.white)
        btnUpgrade.isBordered = false
        
        btnLogin.setBackgroundColor(color: Theme.appNavBarColor())
        btnLogin.setText(text: "LOGIN USER", font:NSFont.init(name: "Montserrat-Medium", size: 12.0)!, color: NSColor.white)
        btnLogin.isBordered = false
        
        
        btnLogout.setBackgroundColor(color: Theme.appNavBarColor())
        btnLogout.setText(text: "LOGOUT", font:NSFont.init(name: "Montserrat-Medium", size: 12.0)!, color: NSColor.white)
        btnLogout.isBordered = false
    }
    
    //MARK: - WEB API METHODS

    @objc func getUserDetailAndUpdateUI(){
        let param = [
            "token" : User.accessToken
        ]
        showHud(self)
        NetworkManager.getUserDetail(param, successHandler: { (response) in
            hideHud(self)
            let res = response as! [String : Any]
            if res["responce_type"] as? String == "success" {
                if let data = res["data"] as? [String : Any]{
                    if let user = data["userData"] as? [String : Any] {
                        User.mapData(data: user)
                    }
                    Plan.mapData(data: data)
                    self.updateScreen()
                }
            }
            else{
                if let errorText = res["data"] as? String
                {
                    dialogOK(question: "Error", text: errorText)
                }
                else
                {
                    dialogOK(question: "Error", text: "An Unknown error occured. Please try again.")
                }
            }

        }) { (error) in
            hideHud(self)
            dialogOK(question: "Error", text: (error?.localizedDescription)!)
        }
    }
    
    //MARK: - CUSTOM METHODS
    func updateScreen(){
        // User Section
        self.lbUserName.stringValue = User.userName
        self.lbEmail.stringValue = User.email
        self.lbJoinDate.stringValue = User.joinDate
        
        // Plan Section
        self.lbPackage.stringValue = Plan.planName
        self.lbTotalQueries.stringValue = "\(Plan.planQueries)"
        self.lbPurchaseDAte.stringValue = Plan.purchaseDate
        self.lbExpireDate.stringValue = Plan.expireDate
        self.lbUsedQueries.stringValue = "\(Plan.planUsedQueries)"
        
        self.lbRemainingQueries.stringValue = "\(Plan.planQueries - Plan.planUsedQueries)"
    }
    
    func goToBaseScreenAfterLogOut()
    {
        self.dismissViewController(self)
        if let window = NSApplication.shared.mainWindow  as? INAppStoreWindow{
            let homeVC = window.contentViewController as? HomeVC
            homeVC?.view.removeFromSuperview()
            homeVC?.removeFromParentViewController()
            
            let introVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "IntroVC")) as? IntroVC
            window.contentViewController = introVC
            window.title = "Welcome To Plagiarism Checker"
            window.titleBarHeight = 20
        }
    }
    //MARK: - ACTIONS -
    @IBAction func btnUpgradeTapped(_ sender: Any) {
        let planVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "PlanBuyVC")) as? PlanBuyVC
        self.presentViewControllerAsModalWindow(planVC!)
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        
        if User.accessToken.isEmpty
        {
            return
        }
        let param = [
            "token" : User.accessToken
        ]

        showHud(self)
        
        NetworkManager.logoutUser(param, successHandler: { (response) in
            hideHud(self)
            User.clearuserData()
            Plan.clearUserPlanData()
            self.goToBaseScreenAfterLogOut()
            
        }) { (error) in
            hideHud(self)
            dialogOK(question: "Error", text: (error?.localizedDescription)!)
        }
    }
    
    @IBAction func btnLoginTapped(_ sender: Any) {
        let tabVC = self.storyboard?.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier.init(rawValue: "TabViewController")) as? TabViewController
        self.presentViewControllerAsModalWindow(tabVC!)
    }    
}

