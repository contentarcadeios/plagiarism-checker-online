//
//  Theme.swift
//  HSK
//
//  Created by Anees ur Rehman on 29/08/2016.
//  Copyright © 2016 consultant. All rights reserved.
//

import AppKit

class Theme: NSObject {
    
    static func tabSelectionColor() -> NSColor {
        return .white
    }
    
    static func appNavBarColor() -> NSColor {
        return getColor(red: 229.0, green: 25.0, blue: 42.0)
    }
    // MARK: Helping Methods
    
    static func getColor(red: Float, green: Float, blue: Float) -> NSColor {
        return NSColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1.0)
    }
    
    static func setupAppearance() -> Void {
        
      /*  UIApplication.shared.statusBarStyle    = .lightContent
        
        UINavigationBar.appearance().tintColor              = whiteColor()
        UINavigationBar.appearance().barTintColor           = themeColorGreen()
        UINavigationBar.appearance().isTranslucent            = false
        UINavigationBar.appearance().titleTextAttributes    = [NSForegroundColorAttributeName: whiteColor()]
        
        //        UIToolbar.appearance().barTintColor = themeColorBlack()
        //        UIToolbar.appearance().tintColor = themeColorGreen()
        UIToolbar.appearance().barTintColor = themeColorGreen()
        UIToolbar.appearance().tintColor = themeColorBlack()
        UIToolbar.appearance().isTranslucent = false
        
        UITabBar.appearance().barTintColor = themeColorBlack()
        UITabBar.appearance().tintColor = themeColorGreen()
        UITabBar.appearance().isTranslucent = false*/
        
    }

}
