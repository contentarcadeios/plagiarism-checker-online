//
//  APIResult.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 4/25/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class APIResult: NSObject, Mappable {
    
    var errorDetails:String?
    var errorMessage:String?
    var parsedResult:[ParsedResult]?
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        errorDetails <- map["ErrorDetails"]
        errorMessage <- map["ErrorMessage"]
        parsedResult <- map["ParsedResults"]
    }
    
}

