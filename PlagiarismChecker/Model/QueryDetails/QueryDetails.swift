//
//  PlagiarisedQuery.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 5/11/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class QueryDetails: NSObject, Mappable {
    
    var query:String?
    var error:NSNumber?
    var unique:String?
    var plagiarisedDetails: [PlagiarisedDetails]?
    var hashQuery:String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        query <- map["query"]
        error <- map["error"]
        unique <- map["unique"]
        hashQuery <- map["hash_query"]
        plagiarisedDetails <- map["webs"]
    }
}
