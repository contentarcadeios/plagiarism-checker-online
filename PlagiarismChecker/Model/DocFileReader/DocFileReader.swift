//
//  DocFileReader.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 10/18/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit

import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class DocFileReader: NSObject, Mappable {

    var responceType:String?
    var data:String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        responceType <- map["responce_type"]
        data <- map["data"]
    }
    
    class func readDocFile (_ filePath:String,successHandler:((_ result:DocFileReader) -> Void)!,
                                     failure: ((_ error: NSError?) -> Void)!) -> Void {
        
        let urlString = "\(URL_PLAGIARISM_FILE)"
        var memeType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
        var name = "file.docx"
        if((filePath as NSString).pathExtension == "doc") {
            memeType = "application/msword"
            name = "file.doc"
        }else if((filePath as NSString).pathExtension == "txt") {
            name = "file.txt"
        }
        print((filePath as NSString).pathExtension)
        NetworkManager.uploadFileWithObject(.post, mapperClass:DocFileReader.self , _keyPath: "", url: urlString,memeType: memeType,fileName: name, path: filePath, successHandler: { (response, result) in
            successHandler(result!)
        }, failure: { (error) in
            failure(error)
        })
    }
    
}
