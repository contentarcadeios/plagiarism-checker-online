//
//  ParsedResult.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 4/25/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class ParsedResult: NSObject, Mappable {
    
    var errorDetails:String?
    var errorMessage:String?
    var text:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        errorDetails <- map["ErrorDetails"]
        errorMessage <- map["ErrorMessage"]
        text <- map["ParsedText"]
    }
    
    class func getParsedTextFromImage (_ image:NSImage,successHandler:((_ result:APIResult) -> Void)!,
                               failure: ((_ error: NSError?) -> Void)!) -> Void {
        
        let urlString = "\(URL_OCR)"
        var param:[String:Any] = [String:Any]()
        param["apikey"] = OCRAPIKEY
      
        NetworkManager.uploadPhotoWithObject(.post, mapperClass: APIResult.self, _keyPath:"" , url: urlString, params: param, image: image, successHandler: { (response, results) in
            successHandler(results!)
        }, failure: { (error) in
            failure(error)
        })
    }
    
    class func getParsedTextFromPDF (_ path:String,successHandler:((_ result:APIResult) -> Void)!,
                                       failure: ((_ error: NSError?) -> Void)!) -> Void {
        let urlString = "\(URL_OCR)"
        var param:[String:Any] = [String:Any]()
        param["apikey"] = OCRAPIKEY
        
        NetworkManager.uploadFileWithObject(.post, mapperClass:  APIResult.self, _keyPath: "", url: urlString, params:param, path: path, successHandler: { (respone, results) in
            successHandler(results!)
        }) { (error) in
            failure(error)
        }
        
    }
}

