/*****************************************************************************
 * VLCGoogleDriveController.m
 * VLC for iOS
 *****************************************************************************
 * Copyright (c) 2013 VideoLAN. All rights reserved.
 * $Id$
 *
 * Authors: Carola Nitz <nitz.carola # googlemail.com>
 *          Felix Paul Kühne <fkuehne # videolan.org>
 *          Soomin Lee <TheHungryBu # gmail.com>
 *
 * Refer to the COPYING file of the official project for license.
 *****************************************************************************/

#import "VLCGoogleDriveController.h"
#import "NSString+SupportedMedia.h"
#import "VLCMediaFileDiscoverer.h"
#import <SSKeychain/SSKeychain.h>
#import "VLCConstants.h"

@interface VLCGoogleDriveController ()
{
    GTLRServiceTicket *_fileListTicket;
    GTLRDrive_File* _currentGoogleDriveFileDownload;
    NSArray *_currentFileList;
    GTMSessionFetcher* _currentFetcherRequest;
    NSMutableArray *_listOfGoogleDriveFilesToDownload;
    BOOL _downloadInProgress;
    NSString* _currentDownloadedFilePath;
    NSString *_nextPageToken;
    NSString *_folderId;
    
    CGFloat _averageSpeed;
    NSTimeInterval _startDL;
    NSTimeInterval _lastStatsUpdate;
}

@end

@implementation VLCGoogleDriveController

#pragma mark - session handling

+ (instancetype)sharedInstance
{
    static VLCGoogleDriveController *sharedInstance = nil;
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedInstance = [VLCGoogleDriveController new];
    });
    
    return sharedInstance;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        _downloadInProgress = NO;
        
    }
    return self;
}
- (void)startSession
{
    [self restoreFromSharedCredentials];
    self.driveService = [GTLRDriveService new];
    self.driveService.authorizer = [GTMOAuth2WindowController authForGoogleFromKeychainForName:kKeychainItemName clientID:kVLCGoogleDriveClientID clientSecret:kVLCGoogleDriveClientSecret];
  //  self.driveService.authorizer = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kVLCGoogleDriveClientID clientSecret:kVLCGoogleDriveClientSecret];
}

- (void)stopSession
{
    [_fileListTicket cancelTicket];
    _nextPageToken = nil;
    _currentFileList = nil;
}

- (void)logout
{
    [GTMOAuth2WindowController removeAuthFromKeychainForName:kKeychainItemName];
    self.driveService.authorizer = nil;
    NSUbiquitousKeyValueStore *ubiquitousStore = [NSUbiquitousKeyValueStore defaultStore];
    [ubiquitousStore setString:nil forKey:kVLCStoreGDriveCredentials];
    [ubiquitousStore synchronize];
    [self stopSession];
    if ([self.delegate respondsToSelector:@selector(mediaListUpdated)])
        [self.delegate mediaListUpdated];
}

- (BOOL)isAuthorized
{
    if (!self.driveService) {
        [self startSession];
    }
    BOOL ret = [((GTMOAuth2Authentication *)self.driveService.authorizer) canAuthorize];
    if (ret) {
        [self shareCredentials];
    }
    return ret;
}

- (void)shareCredentials
{
    /* share our credentials */
    NSString *credentials = [SSKeychain passwordForService:kKeychainItemName account:@"OAuth"]; // kGTMOAuth2AccountName
    if (credentials == nil)
        return;
    
    NSUbiquitousKeyValueStore *ubiquitousStore = [NSUbiquitousKeyValueStore defaultStore];
    [ubiquitousStore setString:credentials forKey:kVLCStoreGDriveCredentials];
    [ubiquitousStore synchronize];
}

- (BOOL)restoreFromSharedCredentials
{
    NSUbiquitousKeyValueStore *ubiquitousStore = [NSUbiquitousKeyValueStore defaultStore];
    [ubiquitousStore synchronize];
    NSString *credentials = [ubiquitousStore stringForKey:kVLCStoreGDriveCredentials];
    if (!credentials)
        return NO;
    
    [SSKeychain setPassword:credentials forService:kKeychainItemName account:@"OAuth"]; // kGTMOAuth2AccountName
    return YES;
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
    
    NSAlert * alert = [[NSAlert alloc] init];
    alert.messageText = NSLocalizedString(title, "");
    alert.informativeText = NSLocalizedString(message, "");
    alert.alertStyle = NSAlertStyleWarning;
    [alert addButtonWithTitle:@"OK"];
    [alert runModal];
//    VLCAlertView *alert = [[VLCAlertView alloc] initWithTitle: title
//                                                      message: message
//                                                     delegate: nil
//                                            cancelButtonTitle: @"OK"
//                                            otherButtonTitles: nil];
//    [alert show];
}

#pragma mark - file management

- (BOOL)canPlayAll
{
    return NO;
}

- (void)requestDirectoryListingAtPath:(NSString *)path
{
    if (self.isAuthorized) {
        //we entered a different folder so discard all current files
        if (![path isEqualToString:_folderId])
            _currentFileList = nil;
        [self listFilesWithID:path];
    }
}

- (BOOL)hasMoreFiles
{
    return _nextPageToken != nil;
}

- (void)downloadFileToDocumentFolder:(GTLRDrive_File *)file
{
    if (file == nil)
        return;
    
    if ([file.mimeType isEqualToString:@"application/vnd.google-apps.folder"]) return;
    
    if (!_listOfGoogleDriveFilesToDownload)
        _listOfGoogleDriveFilesToDownload = [[NSMutableArray alloc] init];
    
    [_listOfGoogleDriveFilesToDownload addObject:file];
    
    if ([self.delegate respondsToSelector:@selector(numberOfFilesWaitingToBeDownloadedChanged)])
        [self.delegate numberOfFilesWaitingToBeDownloadedChanged];
    
    [self _triggerNextDownload];
}

- (void)listFilesWithID:(NSString *)folderId
{
    _fileList = nil;
    _folderId = folderId;
    GTLRDriveQuery_FilesList *query;
    NSString *parentName = @"root";
    
    query = [GTLRDriveQuery_FilesList query];
    query.pageToken = _nextPageToken;
    //the results don't come in alphabetical order when paging. So the maxresult (default 100) is set to 1000 in order to get a few more files at once.
    //query.pageSize = 1000;
    
    query.fields = @"files(*)";
    
    if (![_folderId isEqualToString:@""]) {
        parentName = [_folderId lastPathComponent];
    }
    query.q = [NSString stringWithFormat:@"'%@' in parents", parentName];
    
    _fileListTicket = [self.driveService executeQuery:query
                                    completionHandler:^(GTLRServiceTicket *ticket,
                                                        GTLRDrive_FileList *fileList,
                                                        NSError *error) {
                                        if (error == nil) {
                                            _fileList = fileList;
                                            _nextPageToken = fileList.nextPageToken;
                                            _fileListTicket = nil;
                                            [self _listOfGoodFilesAndFolders];
                                        } else {
                                          
                                            [self showAlert:NSLocalizedString(@"GDRIVE_ERROR_FETCHING_FILES",nil) message:error.localizedDescription];
                                            if ([self.delegate respondsToSelector:@selector(stopedWithError:)])
                                                [self.delegate stopedWithError:error];
                                        }
                                    }];
    
    
    
}

// Process the response and display output
- (void)displayResultWithTicket:(GTLRServiceTicket *)ticket
             finishedWithObject:(GTLRDrive_FileList *)result
                          error:(NSError *)error {
    if (error == nil) {
        //        NSMutableString *output = [[NSMutableString alloc] init];
        //        if (response.files.count > 0) {
        //            [output appendString:@"Files:\n"];
        //            int count = 1;
        //            for (GTLRDrive_File *file in result.files) {
        //                [filesString appendFormat:@"%@ (%@)\n", file.name, file.identifier];
        //                count++;
        //            }
        //        } else {
        //            [output appendString:@"No files found."];
        //        }
        //        self.output.text = output;
    } else {
        NSMutableString *message = [[NSMutableString alloc] init];
        [message appendFormat:@"Error getting presentation data: %@\n", error.localizedDescription];
        [self showAlert:@"Error" message:message];
    }
}

- (BOOL)isObjectAlreadayExists:(id)file {
    
    if([file isKindOfClass:[GTLRDrive_File class]]) {
        if((GTLRDrive_File*)file == self.currentGoogleDriveFileDownload) {
            return YES;
        }else {
            if(_listOfGoogleDriveFilesToDownload) {
                NSInteger index = [_listOfGoogleDriveFilesToDownload indexOfObject: file];
                return (index != NSNotFound);
            }
        }
    }
    return NO;
}

- (void)removeFileFromQueue:(id) file {
    if([file isKindOfClass:[GTLRDrive_File class]]) {
        [_listOfGoogleDriveFilesToDownload removeObject:file];
        if ([self.delegate respondsToSelector:@selector(operationWithProgressInformationStopped:)]) {
            [self.delegate operationWithProgressInformationStopped:NULL];
        }
    }
}

- (void)cancelCurrentDownload {
    if(_currentFetcherRequest != NULL) {
        [_currentFetcherRequest stopFetching];
        _currentFetcherRequest = NULL;
    }
    _currentGoogleDriveFileDownload = NULL;
    _currentDownloadedFilePath = NULL;
    if ([self.delegate respondsToSelector:@selector(operationWithProgressInformationStopped:)]) {
        [self.delegate operationWithProgressInformationStopped:NULL];
    }
    
    _downloadInProgress = NO;
    [self _triggerNextDownload];
}

- (void)streamFile:(GTLRDrive_File *)file
{
    NSString *token = ((GTMOAuth2Authentication *)self.driveService.authorizer).accessToken;
    NSString *downloadString = [file.webContentLink stringByAppendingString:[NSString stringWithFormat:@"&access_token=%@",token]];
//    VLCPlaybackController *vpc = [VLCPlaybackController sharedInstance];
//    [vpc playURL:[NSURL URLWithString:downloadString] successCallback:nil errorCallback:nil];
}

- (void)_triggerNextDownload
{
    
    if (_listOfGoogleDriveFilesToDownload.count > 0 && !_downloadInProgress) {
        
        [self _reallyDownloadFileToDocumentFolder:_listOfGoogleDriveFilesToDownload[0]];
        _currentGoogleDriveFileDownload = _listOfGoogleDriveFilesToDownload[0];
        [_listOfGoogleDriveFilesToDownload removeObjectAtIndex:0];
        
    }
}

- (void)_reallyDownloadFileToDocumentFolder:(GTLRDrive_File *)file
{
    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [searchPaths[0] stringByAppendingFormat:@"/%@", file.name];
    _currentDownloadedFilePath = filePath;
    [self loadFile:file intoPath:filePath];
    
    if ([self.delegate respondsToSelector:@selector(operationWithProgressInformationStarted)])
        [self.delegate operationWithProgressInformationStarted];
    
    _downloadInProgress = YES;
}

- (BOOL)_supportedFileExtension:(NSString *)filename
{
    NSString *realStr = [NSString stringWithString:filename];
    if ([filename isKindOfClass: [NSString class]] && [realStr isSupportedTextFormat])
        return YES;
    
    return NO;
}

- (void)_listOfGoodFilesAndFolders
{
    NSMutableArray *listOfGoodFilesAndFolders = [[NSMutableArray alloc] init];
    
    for (GTLRDrive_File *driveFile in _fileList)
    {
        if (driveFile.trashed.boolValue) {
            continue;
        }
        
        BOOL isDirectory = [driveFile.mimeType isEqualToString:@"application/vnd.google-apps.folder"];
        BOOL supportedFile = [self _supportedFileExtension:driveFile.name];
        
        if (isDirectory || supportedFile)
            [listOfGoodFilesAndFolders addObject:driveFile];
        
    }
    _currentFileList = [NSArray arrayWithArray:listOfGoodFilesAndFolders];
    
    if ([_currentFileList count] <= 10 && [self hasMoreFiles]) {
        [self listFilesWithID:_folderId];
        return;
    }
    
    NSLog(@"found filtered metadata for %lu files", (unsigned long)_currentFileList.count);
    
    //the files come in a chaotic order so we order alphabetically
    NSArray *sortedArray = [_currentFileList sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        
        NSString *first = [(GTLRDrive_File *)a name];
        NSString *second = [(GTLRDrive_File *)b name];
        return [first compare:second];
    }];
    _currentFileList = sortedArray;
    
    if ([self.delegate respondsToSelector:@selector(mediaListUpdated)])
        [self.delegate mediaListUpdated];
}

- (void)loadFile:(GTLRDrive_File*)file intoPath:(NSString*)destinationPath
{
    
    NSString *exportURLStr =  [NSString stringWithFormat:@"https://www.googleapis.com/drive/v3/files/%@?alt=media",
                               file.identifier];
    
    if ([exportURLStr length] > 0) {
        _currentFetcherRequest = [self.driveService.fetcherService fetcherWithURLString:exportURLStr];
        _currentFetcherRequest.authorizer = self.driveService.authorizer;
        
        _currentFetcherRequest.destinationFileURL = [NSURL fileURLWithPath:destinationPath isDirectory:YES];
        
        // Fetcher logging can include comments.
        [_currentFetcherRequest setCommentWithFormat:@"Downloading \"%@\"", file.name];
        __weak GTMSessionFetcher *weakFetcher = _currentFetcherRequest;
        _startDL = [NSDate timeIntervalSinceReferenceDate];
        _currentFetcherRequest.downloadProgressBlock = ^(int64_t bytesWritten,
                                                         int64_t totalBytesWritten,
                                                         int64_t totalBytesExpectedToWrite) {
            if ((_lastStatsUpdate > 0 && ([NSDate timeIntervalSinceReferenceDate] - _lastStatsUpdate > .5)) || _lastStatsUpdate <= 0) {
                [self calculateRemainingTime:totalBytesWritten expectedDownloadSize:totalBytesExpectedToWrite];
                _lastStatsUpdate = [NSDate timeIntervalSinceReferenceDate];
            }
            
            CGFloat progress = (CGFloat)totalBytesWritten / (CGFloat)totalBytesExpectedToWrite;
            if ([self.delegate respondsToSelector:@selector(currentProgressInformation: currentFile:)])
                [self.delegate currentProgressInformation:progress currentFile:file];
        };
        
        [_currentFetcherRequest beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
            if (error == nil) {
                //TODO: show something nice than an annoying alert
                //[self showAlert:NSLocalizedString(@"GDRIVE_DOWNLOAD_SUCCESSFUL_TITLE",nil) message:NSLocalizedString(@"GDRIVE_DOWNLOAD_SUCCESSFUL",nil)];
                [self downloadSuccessful];
            } else {
                [self showAlert:NSLocalizedString(@"GDRIVE_ERROR_DOWNLOADING_FILE_TITLE",nil) message:NSLocalizedString(@"GDRIVE_ERROR_DOWNLOADING_FILE",nil)];
                [self downloadFailedWithError:error];
            }
        }];
        
    }
}

- (void)calculateRemainingTime:(CGFloat)receivedDataSize expectedDownloadSize:(CGFloat)expectedDownloadSize
{
    CGFloat lastSpeed = receivedDataSize / ([NSDate timeIntervalSinceReferenceDate] - _startDL);
    CGFloat smoothingFactor = 0.005;
    _averageSpeed = isnan(_averageSpeed) ? lastSpeed : smoothingFactor * lastSpeed + (1 - smoothingFactor) * _averageSpeed;
    
    CGFloat RemainingInSeconds = (expectedDownloadSize - receivedDataSize) / _averageSpeed;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:RemainingInSeconds];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    NSString  *remainingTime = [formatter stringFromDate:date];
    if ([self.delegate respondsToSelector:@selector(updateRemainingTime:)])
        [self.delegate updateRemainingTime:remainingTime];
}

- (void)downloadSuccessful
{
    /* update library now that we got a file */
    NSLog(@"DriveFile download was successful");
    [[VLCMediaFileDiscoverer sharedInstance] performSelectorOnMainThread:@selector(updateMediaList) withObject:nil waitUntilDone:NO];
    
    _currentGoogleDriveFileDownload = NULL;
    if ([self.delegate respondsToSelector:@selector(operationWithProgressInformationStopped:)])
        [self.delegate operationWithProgressInformationStopped:_currentDownloadedFilePath];
    _downloadInProgress = NO;
    _currentFetcherRequest = NULL;
    _currentDownloadedFilePath = NULL;
    [self _triggerNextDownload];
}

- (void)downloadFailedWithError:(NSError*)error
{
    NSLog(@"DriveFile download failed with error %li", (long)error.code);
    if ([self.delegate respondsToSelector:@selector(operationWithProgressInformationStopped:)])
        [self.delegate operationWithProgressInformationStopped:_currentDownloadedFilePath];
    _downloadInProgress = NO;
    _currentDownloadedFilePath = NULL;
    [self _triggerNextDownload];
}

#pragma mark - VLC internal communication and delegate

- (NSArray *)currentListFiles
{
    return _currentFileList;
}

- (NSInteger)numberOfFilesWaitingToBeDownloaded
{
    if (_listOfGoogleDriveFilesToDownload)
        return _listOfGoogleDriveFilesToDownload.count;
    
    return 0;
}
@end
