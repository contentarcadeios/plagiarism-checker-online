/*****************************************************************************
 * VLCGoogleDriveController.h
 * VLC for iOS
 *****************************************************************************
 * Copyright (c) 2013 VideoLAN. All rights reserved.
 * $Id$
 *
 * Authors: Carola Nitz <nitz.carola # googlemail.com>
 *          Felix Paul Kühne <fkuehne # videolan.org>
 *
 * Refer to the COPYING file of the official project for license.
 *****************************************************************************/

#import <GoogleAPIClientForREST/GTLRDrive.h>
#import "GTMOAuth2WindowController.h"
#import "VLCCloudStorageController.h"
#import "VLCGoogleDriveConstants.h"

@interface VLCGoogleDriveController : VLCCloudStorageController

@property (nonatomic, retain) GTLRDriveService *driveService;
@property (nonatomic, retain) GTLRDrive_FileList *fileList;

@property (nonatomic, readonly) NSMutableArray *listOfGoogleDriveFilesToDownload;
@property (nonatomic, readonly) GTLRDrive_File *currentGoogleDriveFileDownload;
- (void)stopSession;
- (void)streamFile:(GTLRDrive_File *)file;
- (void)downloadFileToDocumentFolder:(GTLRDrive_File *)file;
- (BOOL)hasMoreFiles;

@end

