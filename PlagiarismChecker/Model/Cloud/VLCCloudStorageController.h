//
//  VLCCloudStorageController.h
//  VLC for iOS
//
//  Created by Carola Nitz on 31/12/14.
//  Copyright (c) 2014 VideoLAN. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VLCCloudStorageDelegate <NSObject>

@required
- (void)mediaListUpdated;

@optional
- (void)operationWithProgressInformationStarted;
- (void)currentProgressInformation:(CGFloat)progress currentFile:(id)file;
- (void)updateRemainingTime:(NSString *)time;
- (void)operationWithProgressInformationStopped:(NSString*)filePath;
- (void)numberOfFilesWaitingToBeDownloadedChanged;
- (void)sessionWasUpdated;
- (void)stopedWithError:(NSError *)error;
@end

@interface VLCCloudStorageController : NSObject

@property (nonatomic, weak) id<VLCCloudStorageDelegate> delegate;
@property (nonatomic, readwrite) BOOL isAuthorized;
@property (nonatomic, readonly) NSArray *currentListFiles;
@property (nonatomic, readonly) BOOL canPlayAll;

+ (instancetype)sharedInstance;

- (void)startSession;
- (BOOL)isObjectAlreadayExists:(id)file;
- (void)cancelCurrentDownload;
- (void)removeFileFromQueue:(id) file;
- (void)logout;
- (void)requestDirectoryListingAtPath:(NSString *)path;

@end
