//
//  PlagiarisedDetails.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 5/11/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class PlagiarisedDetails: NSObject, Mappable {
    
    var title:String?
    var url:String?
    var desc:String?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        url <- map["url"]
        desc <- map["des"]
    }
}
