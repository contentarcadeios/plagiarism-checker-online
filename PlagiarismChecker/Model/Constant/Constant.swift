//
//  Constant.swift
//  HSK
//
//  Created by Anees ur Rehman on 29/08/2016.
//  Copyright © 2016 consultant. All rights reserved.
//

import Foundation
import Cocoa
import AppKit
import StoreKit

/// App Singelton Methods

let userDefaults:UserDefaults = UserDefaults.standard
let notifCenter:NotificationCenter = NotificationCenter.default
let appDelegate: AppDelegate = NSApplication.shared.delegate as! AppDelegate
let appScreen = NSScreen.main

/// Constants
let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//let deviceScale:CGFloat = NSScreen.main.scale
let receiptURL = Bundle.main.appStoreReceiptURL
let MSG_HOME_TITLE = "Plagiarism Checker"
let MSG_PLACEHOLDER_TEXT = "Paste your text here"
let MSG_ENG_LANG_ONLY = "OCR works for English language only."
let MSG_CANCEL = "Cancel"
let MSG_NOTE = "Note"
let MSG_OK = "OK"
let MSG_TITLE_ERROR = "Error"
let MSG_NETWORK_ERROR = "Network Failed!"
let MSG_CANT_READ = "Can't read text"

let SEGUE_RESULTS = "showResults"
let USER_LOGGED_IN = "userloggedIn"
let USER_LOGGED_OUT = "userLoggedOut"

let inAppPrefix = "com.ca.plagChecker"

enum RegisteredPurchase: String {
    case pro
    case pro3mo
    case pro6mo
    case business
    case business3mo
    case business6mo
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = NSScreen.main?.frame.size.width
    static let SCREEN_HEIGHT        = NSScreen.main?.frame.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH!, ScreenSize.SCREEN_HEIGHT!)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH!, ScreenSize.SCREEN_HEIGHT!)
}

enum FileSystemType:Int {
    case  googleDrive = 1, dropBox = 2
}

//====================================================================================================
let DropboxSessionWasAuthorized = "DropboxSessionWasAuthorized"
let PRODUCT_PRO = "com.ca.plagiarismchecker.adremove"
let OCRAPIKEY = "e45342ab0788957"
let kVLCGoogleDriveClientID = "848519720378-0reo8cahe6s6d9k0pe49taukobtjsbcd.apps.googleusercontent.com"
let kVLCGoogleDriveClientSecret = "WbiBYrpDQoQ8qMacJ3W9v1TY"
let kKeychainItemName = "com.contentarcade.PlagiarismCheckerMac"
let kDropboxAppKey = "tzpze9pl5cim3o4"
let kWindowClosed = "windowIsClosed"
let kURLRead = "URL_READ_NOTIF"
let kPackageChanged = "PACKAGE_CHANGED"

/*   URLs     */

let API_BASE_URL        = "https://plagiarismsoftware.org/software/api/"
let API_USERAUTH        = "\(API_BASE_URL)login"
let API_USERSIGNUP      = "\(API_BASE_URL)register"
let API_GETUSERDETAIL   = "\(API_BASE_URL)userdetail"
let API_FORGOTPASSWORD  = "\(API_BASE_URL)forgetpassword"
let API_LOGOUT          = "\(API_BASE_URL)logout"
/****************************************************************/

let URL_OCR = "https://api.ocr.space/parse/image"
let URL_PLEGIARISM = "https://plagiarismsoftware.org/app/api/checkPlag"
let URL_PLAGIARISM_FILE = "https://plagiarismsoftware.org/app/api/checkPlagFile"
let URL_GENERATE_TOKEN = "https://plagiarismsoftware.org/software/api/api_auth/generate"
let URL_REFRESH_TOKEN = "https://plagiarismsoftware.org/software/api/api_auth/refresh"
let URL_REQUEST_USER = "https://plagiarismsoftware.org/software/api/ios_req"
let URL_SUBSCRPTION_SERVER = "https://plagiarismsoftware.org/macipn"

let MSG_MESSAGE = "Message"
let tokenExpirationCode = 422
//MARK: - METHODS -
func showHud(_ delegate : NSViewController?){
    if let d = delegate {
        MBProgressHUD.showAdded(to: d.view, animated: true)
    }
}

func hideHud(_ delegate : NSViewController?){
    if let d = delegate {
         MBProgressHUD.hideAllHUDs(for: d.view, animated: true)
    }
}

func generateToken(_ delegate : NSViewController?,successHandler:((_ response: AnyObject?) -> Void)!,failure: ((_ _error: NSError?) -> Void)! ){
    showHud(delegate)
    NetworkManager.generateToken(successHandler: { (response) in
        hideHud(delegate)
        let res = response as! [String : Any]
        if res["status"] as? Int == 200 {
            userToken.token = (res["token"] as? String)!
            successHandler("success" as AnyObject)
        }else{
            failure(nil)
        }
    }) { (error) in
        hideHud(delegate)
        dialogOK(question: "Error", text: (error?.localizedDescription)!)
        failure(error)
    }
}

func refreshToken(_ delegate : NSViewController? ,successHandler:((_ response: AnyObject?) -> Void)!,failure: ((_ _error: NSError?) -> Void)!){
    showHud(delegate)
    NetworkManager.refreshToken(successHandler: { (response) in
        hideHud(delegate)
        let res = response as! [String : Any]
        if res["status"] as? Int == 200 {
            userToken.token = (res["token"] as? String)!
            successHandler("success" as AnyObject)
        }else{
            failure(nil)
        }
    }) { (error) in
        hideHud(delegate)
         dialogOK(question: "Error", text: (error?.localizedDescription)!)
        failure(error)
    }
}

func alertWithTitle(_ title: String, message: String) -> NSAlert {
    let alert: NSAlert = NSAlert()
    alert.messageText = title
    alert.informativeText = message
    alert.alertStyle = NSAlert.Style.informational
    return alert
}

func showAlert(_ alert: NSAlert, handler: ((NSApplication.ModalResponse) -> Void)? = nil) {
    if let window = NSApplication.shared.keyWindow {
        alert.beginSheetModal(for: window) { (response: NSApplication.ModalResponse) in
            handler?(response)
        }
    } else {
        let response = alert.runModal()
        handler?(response)
    }
}

func getAppSecretForWebApisWithMD5() -> String{
    let appSecret = "ca.iospc" + Bundle.main.bundleIdentifier!
    let md5String = appSecret.md5()
    return (md5String?.toBase64())!
}

func getAppSecretForWebApisWithOutMD5() -> String{
    let appSecret = "ca.iospc" + Bundle.main.bundleIdentifier!
    return appSecret.toBase64()
}

func dialogOKCancel(question: String, text: String) -> Bool {
    let alert = NSAlert()
    alert.messageText = question
    alert.informativeText = text
    alert.alertStyle = .warning
    alert.addButton(withTitle: "OK")
    alert.addButton(withTitle: "Cancel")
    return alert.runModal() == .alertFirstButtonReturn
}

func dialogOK(question: String, text: String){
    let alert = NSAlert()
    alert.messageText = question
    alert.informativeText = text
    alert.alertStyle = .warning
    alert.addButton(withTitle: "OK")
    alert.runModal()
}

func openItunesForSubScriptionOption(){
    let urlStr = "https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions"
    NSWorkspace.shared.open(URL.init(string: urlStr)!)
}

func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
    guard let url = URL(string : "macappstore://itunes.apple.com/app/plagiarism-checker/id\(appId)?mt=8") else {
        completion(false)
        return
    }
        completion(NSWorkspace.shared.open(url))
}

func openApps(completion: @escaping ((_ success: Bool)->())) {
    //macappstore
    guard let url = URL(string : "macappstore://itunes.apple.com/developer/tausif-akram/id1212263193") else {
        completion(false)
        return
    }
    completion(NSWorkspace.shared.open(url))
}
