//
//  HoverView.swift
//  PlagiarismChecker
//
//  Created by macbook on 09/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Foundation

protocol HoverViewDelegate {
    func hoverViewMouse(_ inMouse : Bool, sender : NSBox)
}

class HoverView: NSBox {
    
    var delegate : HoverViewDelegate?
    @IBOutlet weak var boxTopContainer: NSBox!
    
    var trackingArea: NSTrackingArea?
    var mouseInside: Bool = false {
        didSet {
            needsDisplay = true
        }
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        // Drawing code here.
        needsDisplay = true
    }
    
    override func updateTrackingAreas() {
        super.updateTrackingAreas()
        ensureTrackingArea()
        if !trackingAreas.contains(trackingArea!) {
            addTrackingArea(trackingArea!)
        }
    }
    
    func ensureTrackingArea() {
        if trackingArea == nil {
            trackingArea = NSTrackingArea(rect: NSZeroRect, options: [.inVisibleRect, .activeAlways, .mouseEnteredAndExited], owner: self, userInfo: nil)
        }
    }
    
    override func mouseEntered(with event: NSEvent) {
        self.delegate?.hoverViewMouse(true, sender: self)
        mouseInside = true
    }
    
    override func mouseExited(with event: NSEvent) {
         self.delegate?.hoverViewMouse(false, sender: self)
        mouseInside = false
    }
    
    class func gradientWithTargetColor(targetColor: NSColor) -> NSGradient? {
        return NSGradient.init(starting: targetColor, ending: targetColor)
    }
}
