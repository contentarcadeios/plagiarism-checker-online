//
//  PlegiarismResult.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 4/26/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class PlegiarismResult: NSObject, Mappable {
    
    var totalQueries:NSNumber?
    var plagPercent:NSNumber?
    var uniquePercent:NSNumber?
    var details:[QueryDetails]?
    var queries:[String]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        totalQueries <- map["totalQueries"]
        plagPercent <- map["plagPercent"]
        uniquePercent <- map["uniquePercent"]
        details <- map["details"]
        queries <- map["queries"]
    }
}
