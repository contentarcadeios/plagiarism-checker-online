//
//  Plegiarism.swift
//  Plagiarism
//
//  Created by Talha Ejaz on 4/26/17.
//  Copyright © 2017 contentarcade. All rights reserved.
//

import AppKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class Plegiarism: NSObject, Mappable {
    
    var responceType:String?
    var data:PlegiarismResult?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        responceType <- map["responce_type"]
        data <- map["data"]
        
    }
    class func getPlegiarismResults (_ data:String,successHandler:((_ result:Plegiarism) -> Void)!,
                                       failure: ((_ error: NSError?) -> Void)!) -> Void {
        
        let urlString = "\(URL_PLEGIARISM)"
        var param:[String:Any] = [String:Any]()
        param["data"] = data
        param["Device"] = "Mac"
        
        NetworkManager.apiCommunication(_apiMethod: .post, _mapperClass: Plegiarism.self, _keyPath: "", _url: urlString, parameters: param,encoding:URLEncoding.default,successHandler: { (response, results) in
            successHandler(results!)
        }, failure: { (error) in
            failure(error)
        })
    }
}
