//
//  CustomObjects.swift
//  PlagiarismChecker
//
//  Created by macbook on 09/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Foundation
import Cocoa

class PlaceholderTextView: NSTextView {
    @objc var placeholderAttributedString: NSAttributedString?
}

class IntegerFormatter : NumberFormatter {
    override func isPartialStringValid(_ partialString: String, newEditingString newString: AutoreleasingUnsafeMutablePointer<NSString?>?, errorDescription error: AutoreleasingUnsafeMutablePointer<NSString?>?) -> Bool {
        
        if partialString.isEmpty {
            return true
        }
        
        // Optional: limit input length
        /*
         if partialString.characters.count>3 {
         return false
         }
         */
        
        // Actual check
        return Int(partialString) != nil
    }
}
// USER
class User:NSObject
{
    static var userID:String {
        get{
            if ( UserDefaults.standard.object(forKey: "userID") != nil)
            {
                return UserDefaults.standard.object(forKey: "userID") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "userID")
        }
    }
    static var accessToken:String {
        get{
            if ( UserDefaults.standard.object(forKey: "accessToken") != nil)
            {
                return UserDefaults.standard.object(forKey: "accessToken") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "accessToken")
        }
    }
    static var selectedPlanID:String {
        get{
            if ( UserDefaults.standard.object(forKey: "userID") != nil)
            {
                return UserDefaults.standard.object(forKey: "userID") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "userID")
        }
    }
    
    static var userName : String{
        get{
            if ( UserDefaults.standard.object(forKey: "userName") != nil)
            {
                return UserDefaults.standard.object(forKey: "userName") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "userName")
        }
    }
    
    static var joinDate : String{
        get{
            if ( UserDefaults.standard.object(forKey: "join_date") != nil)
            {
                return UserDefaults.standard.object(forKey: "join_date") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "join_date")
        }
    }
    
    static var email:String {
        get{
            if ( UserDefaults.standard.object(forKey: "email") != nil)
            {
                return UserDefaults.standard.object(forKey: "email") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "email")
        }
    }
    
    static func mapData(data:[String:Any])
    {
        User.userID         = data["user_id"]       as! String
        User.userName       = data["user_name"]     as! String
        User.email          = data["user_email"]    as! String
        User.joinDate       = data["join_date"]     as! String
        User.accessToken    = data["token"]         as! String
    }
    
    static func clearuserData()
    {
        User.userID         = ""
        User.userName       = ""
        User.email          = ""
        User.joinDate       = ""
        User.accessToken    = ""
    }
    
}

// User Token
class userToken:NSObject
{
    static var token:String {
        get{
            if ( UserDefaults.standard.object(forKey: "token") != nil)
            {
                return UserDefaults.standard.object(forKey: "token") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "token")
        }
    }
}

// PLAN DETAILS
class Plan:NSObject
{
    static var proID:String {
        get{
            if ( UserDefaults.standard.object(forKey: "proID") != nil)
            {
                return UserDefaults.standard.object(forKey: "proID") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "proID")
        }
    }
    static var proHashID:String {
        get{
            if ( UserDefaults.standard.object(forKey: "proHashID") != nil)
            {
                return UserDefaults.standard.object(forKey: "proHashID") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "proHashID")
        }
    }
    static var planName : String{
        get{
            if ( UserDefaults.standard.object(forKey: "PlanName") != nil)
            {
                return UserDefaults.standard.object(forKey: "PlanName") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "PlanName")
        }
    }
    
    static var planQueries : Int{
        get{
            return UserDefaults.standard.integer(forKey: "PlanQueries")
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "PlanQueries")
        }
    }
    
    static var planUsedQueries : Int{
        get{

            return UserDefaults.standard.integer(forKey: "UsedQueries")
        }
        set{
            
            UserDefaults.standard.set(newValue, forKey: "UsedQueries")
        }
    }
    
    static var purchaseDate : String{
        get{
            if ( UserDefaults.standard.object(forKey: "purchaseDate") != nil)
            {
                return UserDefaults.standard.object(forKey: "purchaseDate") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "purchaseDate")
        }
    }
    
    static var expireDate : String{
        get{
            if ( UserDefaults.standard.object(forKey: "expireDate") != nil)
            {
                return UserDefaults.standard.object(forKey: "expireDate") as! String
            }
            else
            {
                return ""
            }
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "expireDate")
        }
    }
    
    static var words:Int {
        get{
            return UserDefaults.standard.integer(forKey: "words")
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "words")
        }
    }
    
    static func mapData(data:[String:Any])
    {
        Plan.proID              = data["pro_id"]        as! String
        Plan.planName           = data["pro_name"]      as! String
        Plan.purchaseDate       = data["purchase_date"] as! String
        Plan.expireDate         = data["expire_date"]   as! String
        
        Plan.planQueries        = Int(data["pro_queries"]   as! String)!
        Plan.planUsedQueries    = Int(data["used_queries"]  as! String)!
        Plan.words              = Int(data["words"]         as! String)!
    }
    static func clearUserPlanData()
    {
        Plan.proID              = ""
        Plan.planName           = ""
        Plan.planQueries        = 0
        Plan.purchaseDate       = ""
        Plan.expireDate         = ""
        Plan.planUsedQueries    = 0
        Plan.words              = 0
    }
}

