//
//  PaymentManager.swift
//  Content Arcade
//
//  Created by Talha Ejaz on 06/01/2017.
//  Copyright © 2017 consultant. All rights reserved.
//

import StoreKit
import AppKit

public typealias ProductIdentifier = String
public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> ()

open class PaymentManager : NSObject  {
    
    static let sharedManager:PaymentManager = PaymentManager()
    static let IAPHelperPurchaseNotification = "IAPHelperPurchaseNotification"
    static let IAPHelperFailNotification = "IAPHelperFailNotification"
    var productIdentifiers: Set<ProductIdentifier> = Set<ProductIdentifier>()
    fileprivate var purchasedProductIdentifiers = Set<ProductIdentifier>()
    fileprivate var productsRequest: SKProductsRequest?
    fileprivate var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    public override init() {
        super.init()
        var set = Set<ProductIdentifier>()
        set.insert(PRODUCT_PRO)
        self.productIdentifiers = set
        SKPaymentQueue.default().add(self)
    }
    
    public init(productIds: Set<ProductIdentifier>) {
        productIdentifiers = productIds
        for productIdentifier in productIds {
            let purchased = UserDefaults.standard.bool(forKey: productIdentifier)
            if purchased {
                purchasedProductIdentifiers.insert(productIdentifier)
                print("Previously purchased: \(productIdentifier)")
            } else {
                print("Not purchased: \(productIdentifier)")
            }
        }
        super.init()
        SKPaymentQueue.default().add(self)
    }
    
}

// MARK: - StoreKit API

extension PaymentManager {
    
    public func requestProducts(completionHandler: @escaping ProductsRequestCompletionHandler) {
        
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self
        productsRequest!.start()
    }
    
    public func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        if SKPaymentQueue.canMakePayments() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(payment)
        }else {
            print("Can not make payment")
        }
    }
    
    public func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
    
    public class func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    public func restorePurchases() {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

// MARK: - SKProductsRequestDelegate

extension PaymentManager: SKProductsRequestDelegate {
    
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let products = response.products
        print("Loaded list of products...")
        print(products)
        
        productsRequestCompletionHandler?(true, products)
        clearRequestAndHandler()
        
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
    }
    
    private func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

// MARK: - SKPaymentTransactionObserver

extension PaymentManager: SKPaymentTransactionObserver {
    
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
     print(queue.transactions.count)

         //NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManager.IAPHelperPurchaseNotification), object: PRODUCT_PRO)
        let myDict = [MSG_MESSAGE:MSG_CANCEL]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManager.IAPHelperFailNotification), object:myDict)
    }
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        let myDict = [MSG_MESSAGE:error.localizedDescription]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManager.IAPHelperFailNotification), object: myDict)
    }

    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            }
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        print("complete...")
        deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
//        do {
//            let receipt = try Data(contentsOf: receiptURL!) //force unwrap is safe here, control can't land here if receiptURL is nil
//            validateAppReceipt(receipt)
//        } catch {
//            // still no receipt, possible but unlikely to occur since this is the "success" delegate method
//        }
    }
   
    private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        
        print("restore... \(productIdentifier)")
        deliverPurchaseNotificationFor(identifier: productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as? NSError {
            if transactionError.code != SKError.paymentCancelled.rawValue {
                print("Transaction Error: \(transaction.error?.localizedDescription)")
                let myDict = [MSG_MESSAGE:transaction.error?.localizedDescription]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManager.IAPHelperFailNotification), object: myDict)
            }else {
                let myDict = [MSG_MESSAGE:MSG_CANCEL]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManager.IAPHelperFailNotification), object:myDict)
            }
        }
        
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    func getAppReceipt() {
        guard let receiptURL = receiptURL else {  /* receiptURL is nil, it would be very weird to end up here */  return }
        do {
            let receipt = try Data(contentsOf: receiptURL)
            validateAppReceipt(receipt)
        } catch {
            // there is no app receipt, don't panic, ask apple to refresh it
            let appReceiptRefreshRequest = SKReceiptRefreshRequest(receiptProperties: nil)
            appReceiptRefreshRequest.delegate = self
            appReceiptRefreshRequest.start()
            // If all goes well control will land in the requestDidFinish() delegate method.
            // If something bad happens control will land in didFailWithError.
        }
    }
    func validateAppReceipt(_ receipt: Data) {
        
        /*  Note 1: This is not local validation, the app receipt is sent to the app store for validation as explained here:
         https://developer.apple.com/library/content/releasenotes/General/ValidateAppStoreReceipt/Chapters/ValidateRemotely.html#//apple_ref/doc/uid/TP40010573-CH104-SW1
         Note 2: Refer to the url above. For good reasons apple recommends receipt validation follow this flow:
         device -> your trusted server -> app store -> your trusted server -> device
         In order to be a working example the validation url in this code simply points to the app store's sandbox servers.
         Depending on how you set up the request on your server you may be able to simply change the
         structure of requestDictionary and the contents of validationURLString.
         */
        let base64encodedReceipt = receipt.base64EncodedString()
        let requestDictionary = ["receipt-data":base64encodedReceipt,"password":"23d68f0931224eb681518c6781432638"]
        guard JSONSerialization.isValidJSONObject(requestDictionary) else {  print("requestDictionary is not valid JSON");  return }
        do {
            let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
            let validationURLString = "https://sandbox.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
            guard let validationURL = URL(string: validationURLString) else { print("the validation url could not be created, unlikely error"); return }
            let session = URLSession(configuration: URLSessionConfiguration.default)
            var request = URLRequest(url: validationURL)
            request.httpMethod = "POST"
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
            let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
                if let data = data , error == nil {
                    do {
                        let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
                        print("success. here is the json representation of the app receipt: \(appReceiptJSON)")
                        // if you are using your server this will be a json representation of whatever your server provided
                    } catch let error as NSError {
                        print("json serialization failed with error: \(error)")
                    }
                } else {
                    print("the upload task returned an error: \(error)")
                }
            }
            task.resume()
        } catch let error as NSError {
            print("json serialization failed with error: \(error)")
        }
    }
    
    private func deliverPurchaseNotificationFor(identifier: String?) {
        guard let identifier = identifier else { return }
        
        purchasedProductIdentifiers.insert(identifier)
        UserDefaults.standard.set(true, forKey: identifier)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: PaymentManager.IAPHelperPurchaseNotification), object: identifier)
    }
}


