//
//  SimplePDFUtilities.swift
//
//  Created by Muhammad Ishaq on 22/03/2015
//

import Foundation
import ImageIO
import Cocoa

extension CIImage {
        func flippedImage(horizontal: Bool = false) -> CIImage? {
                
                var result : CIImage?
                
                if let transform = CIFilter(name:"CIAffineTransform") {
                        transform.setValue(self, forKey:"inputImage")
                        
                        let affineTransform = NSAffineTransform();
                        let xy : (CGFloat, CGFloat) = horizontal ? (-1,1) : (1,-1)
                    affineTransform.scaleX(by: xy.0, yBy:xy.1)
                        transform.setValue(affineTransform, forKey:"inputTransform")
                        
                    result = transform.value(forKey: "outputImage") as? CIImage
                }
                return result
                
        }
        
    func drawInCGContext(context: CGContext!, destRect: CGRect!) {
            let ci = CIContext.init(cgContext: context, options: nil)
            ci.draw(self, in: destRect, from: self.extent)
        }
        
        class func fromNSImage(i: NSImage?) -> CIImage? {
                guard let img = i else {return nil}
                // convert NSImage to bitmap
            if let tiffData = img.tiffRepresentation {
                        if let bitmap = NSBitmapImageRep.init(data: tiffData) {
                                return CIImage(bitmapImageRep: bitmap)
                        }
                }
                return nil
        }
}

extension Bundle {
    var versionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    var appName : String? {
        return infoDictionary?["CFBundleName"] as? String
    }
}

class SimplePDFUtilities {
    class func getApplicationVersion() -> String {
        if let shortVersionString = Bundle.main.versionNumber , let build = Bundle.main.buildNumber{
            return "(\(shortVersionString) Build: \(build))"
        }
        return ""
    }
    
    class func getApplicationName() -> String {
        if let name = Bundle.main.appName {
            return "(\(name))"
        }
        return ""
    }
    
    class func pathForTmpFile(fileName: String) -> NSURL {
        let tmpDirURL = NSURL(fileURLWithPath: NSTemporaryDirectory())
        let pathURL = tmpDirURL.appendingPathComponent(fileName)
        return pathURL as! NSURL
    }
    
    class func renameFilePathToPreventNameCollissions(url: NSURL) -> String {
        let fileManager = FileManager()
        
        // append a postfix if file name is already taken
        var postfix = 0
        var newPath = url.path!
        while(fileManager.fileExists(atPath: newPath)) {
            postfix = postfix + 1
            
            let pathExtension = url.pathExtension
            newPath = (url.deletingPathExtension?.path)!
            newPath = newPath.appendingFormat(" \(postfix)")
            var newPathURL = NSURL(fileURLWithPath: newPath)
            newPathURL = newPathURL.appendingPathExtension(pathExtension!) as! NSURL
            newPath = newPathURL.path!
        }
        
        return newPath
    }
    
    class func getImageProperties(imagePath: String) -> Dictionary<NSObject, AnyObject>? {
        let imageURL = NSURL(fileURLWithPath: imagePath)
        let imageSourceRef = CGImageSourceCreateWithURL(imageURL, nil)
        let props = CGImageSourceCopyPropertiesAtIndex(imageSourceRef!, 0, nil) as Dictionary?
        return props
    }
    
    class func getNumericListAlphabeticTitleFromInteger(value: Int) -> String {
        let base:Int = 26
        let unicodeLetterA :UnicodeScalar = "\u{0061}" // a
        var mutableValue = value
        var result = ""
        repeat {
            let remainder = mutableValue % base
            mutableValue = mutableValue - remainder
            mutableValue = mutableValue / base
            let unicodeChar = UnicodeScalar(remainder + Int(unicodeLetterA.value))
            result = String(describing: unicodeChar) + result
        
        } while mutableValue > 0
        
        return result
    }
    
    class func generateThumbnail(imageURL: NSURL, size: CGSize, callback: @escaping  (_ thumbnail: NSImage, _ fromURL: NSURL, _ size: CGSize) -> Void) {
        DispatchQueue.global(qos: .default).async(execute: {
            if let imageSource = CGImageSourceCreateWithURL(imageURL, nil) {
                let options = [
                    kCGImageSourceThumbnailMaxPixelSize as String: max(size.width, size.height),
                    kCGImageSourceCreateThumbnailFromImageIfAbsent as String: true
                    ] as [String : Any]
                
                let scaledImage = NSImage(cgImage: CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary)!, size: size)
                DispatchQueue.main.async(execute: {
                    callback(scaledImage, imageURL, size)
                })
                
            }
        })
    }
}

