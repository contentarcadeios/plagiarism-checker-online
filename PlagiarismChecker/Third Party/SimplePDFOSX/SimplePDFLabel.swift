//
//  PDFLabel.swift
//
//  Created by Muhammad Ishaq on 22/03/2015.
//

import Cocoa

class SimplePDFLabel: NSTextField {

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
       // let isPDF = !CGRectIsEmpty(CGPDFPageGetBoxRect(nil, CGPDFBox.MediaBox))
        
        if(!(self.layer?.shouldRasterize)! && (self.backgroundColor == nil || (self.backgroundColor?.cgColor.alpha)! == 0)) {
            self.draw(self.bounds)
        }
    }
}
