//
//  SimplePDF.swift
//
//  Created by Muhammad Ishaq on 22/03/2015.
//

import Foundation
import Cocoa
import ImageIO
import CoreText
import QuartzCore

/**
*  Generate Simple Documents with Images. TOC gets generated and put at (roughly) specified page index
*/
public class SimplePDF {
        // MARK: - Document Structure
       public  class DocumentStructure {
                // MARK: - FunctionCall (sort of a "Command" pattern)
             public  enum FunctionCall : CustomStringConvertible {
                        case addH1(string: String, backgroundBoxColor: NSColor?)
                        case addH2(string: String, backgroundBoxColor: NSColor?)
                        case addH3(string: String, backgroundBoxColor: NSColor?)
                        case addH4(string: String, backgroundBoxColor: NSColor?)
                        case addH5(string: String, backgroundBoxColor: NSColor?)
                        case addH6(string: String, backgroundBoxColor: NSColor?)
                        case addBodyText(string: String, backgroundBoxColor: NSColor?)
                        case startNewPage
                        case addImages(imagePaths:[String], imageCaptions: [String], imagesPerRow:Int, spacing:CGFloat, padding:CGFloat)
                        case addImagesRow(imagePaths: [String], imageCaptions: [NSAttributedString], columnWidths: [CGFloat],
                                spacing: CGFloat, padding: CGFloat, captionBackgroundColor: NSColor?, imageBackgroundColor: NSColor?)
                        case addAttributedStringsToColumns(columnWidths: [CGFloat], strings: [NSAttributedString], horizontalPadding: CGFloat, allowSplitting: Bool, backgroundColor: NSColor?)
                        case addView(view: NSView)
                        
                public var description: String {
                                get {
                                        switch(self) {
                                        case .addH1(let string, _):
                                                return "addH1 (\(string))"
                                        case .addH2(let string, _):
                                                return "addH2 (\(string))"
                                        case .addH3(let string, _):
                                                return "addH3 (\(string))"
                                        case .addH4(let string, _):
                                                return "addH4 (\(string))"
                                        case .addH5(let string, _):
                                                return "addH5 (\(string))"
                                        case .addH6(let string, _):
                                                return "addH6 (\(string))"
                                        case .addBodyText(let string, _):
                                            let index = string.index(string.startIndex, offsetBy:25)
                                            let substring = string.substring(to:index)
                                            
                                                return "addBodyText (\(substring))"
                                        case .startNewPage:
                                                return "startNewPage"
                                        case .addImages:
                                                return "addImages"
                                        case .addImagesRow:
                                                return "addImagesRow"
                                        case .addAttributedStringsToColumns:
                                                return "addAttributedStringsToColumns"
                                        case .addView:
                                                return "addView"
                                        }
                                        
                                }
                        }
                        
                        func execute(pdf: PDFWriter, calculationOnly: Bool = true) -> NSRange {
                                var pageRange = NSMakeRange(0, 0)
                                switch(self) {
                                case .addH1(let string, let backgroundBoxColor):
                                    pageRange = pdf.addH1(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .addH2(let string, let backgroundBoxColor):
                                    pageRange = pdf.addH2(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .addH3(let string, let backgroundBoxColor):
                                    pageRange = pdf.addH3(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .addH4(let string, let backgroundBoxColor):
                                    pageRange = pdf.addH4(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .addH5(let string, let backgroundBoxColor):
                                    pageRange = pdf.addH5(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .addH6(let string, let backgroundBoxColor):
                                    pageRange = pdf.addH6(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .addBodyText(let string, let backgroundBoxColor):
                                    pageRange = pdf.addBodyText(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                                case .startNewPage:
                                    pageRange = pdf.startNewPage(calculationOnly: calculationOnly)
                                case .addImages(let imagePaths, let imageCaptions, let imagesPerRow, let spacing, let padding):
                                    pageRange = pdf.addImages(imagePaths: imagePaths, imageCaptions: imageCaptions, imagesPerRow: imagesPerRow, spacing: spacing, padding: padding, calculationOnly: calculationOnly)
                                case .addImagesRow(let imagePaths, let imageCaptions, let columnWidths, let spacing, let padding, let captionBackgroundColor, let imageBackgroundColor):
                                    pageRange = pdf.addImagesRow(imagePaths: imagePaths, imageCaptions: imageCaptions, columnWidths: columnWidths, spacing: spacing, padding: padding, captionBackgroundColor: captionBackgroundColor, imageBackgroundColor: imageBackgroundColor, calculationOnly: calculationOnly)
                                case .addAttributedStringsToColumns(let columnWidths, let strings, let horizontalPadding, let allowSplitting, let backgroundColor):
                                    pageRange = pdf.addAttributedStringsToColumns(columnWidths: columnWidths, strings: strings, horizontalPadding: horizontalPadding, allowSplitting: allowSplitting, backgroundColor: backgroundColor, calculationOnly: calculationOnly)
                                case .addView(let view):
                                    pageRange = pdf.addView(view: view, calculationOnly: calculationOnly)
                                }
                                
                                return pageRange
                        }
                        
                        func getTableOfContentsInfo() -> (TextStyle, String?) {
                                switch(self) {
                                case .addH1(let string, _):
                                        return (.H1, string)
                                case .addH2(let string, _):
                                        return (.H2, string)
                                case .addH3(let string, _):
                                        return (.H3, string)
                                case .addH4(let string, _):
                                        return (.H4, string)
                                case .addH5(let string, _):
                                        return (.H5, string)
                                case .addH6(let string, _):
                                        return (.H6, string)
                                default:
                                        return (.BodyText, nil)
                                }
                        }
                        
                }
                
                // MARK: - Document Node
                public class DocumentElement {
                        var functionCall: FunctionCall
                        var pageRange: NSRange
                        
                        init(functionCall: FunctionCall, pageRange: NSRange) {
                                self.functionCall = functionCall
                                self.pageRange = pageRange
                        }
                        
                        func executeFunctionCall(pdf: PDFWriter, calculationOnly: Bool = true) -> NSRange {
                            self.pageRange = self.functionCall.execute(pdf: pdf, calculationOnly: calculationOnly)
                                return self.pageRange
                        }
                }
                
                // MARK: - TableOfContentsNode
                public class TableOfContentsElement {
                        var attrString: NSAttributedString
                        var pageIndex: Int
                        
                        init(attrString: NSAttributedString, pageIndex: Int) {
                                self.attrString = attrString
                                self.pageIndex = pageIndex
                        }
                }
                
                public var document = Array<DocumentElement>()
                public var tableOfContents = Array<TableOfContentsElement>()
                public var tableOfContentsPagesRange = NSMakeRange(0, 0)
                
                var tableOfContentsOnPage = 1
                var biggestHeadingToIncludeInTOC = TextStyle.H1
                var smallestHeadingToIncludeInTOC = TextStyle.H6
                
                // NOTE: this page only fills in the page numbers as if TOC would never be inserted into the document
                // actual page numbers are calculated within the drawTableOfContentsCall
                public func generateTableOfContents() -> Array<TableOfContentsElement> {
                        var tableOfContents = Array<TableOfContentsElement>()
                        
                        var pageIndex = -1
                    for i in (0..<document.count){
                        let docNode = document[i]
                        pageIndex += docNode.pageRange.location
                        
                        let (textStyle, label) = docNode.functionCall.getTableOfContentsInfo()
                        
                        if let heading = label {
                            if(textStyle.rawValue >= biggestHeadingToIncludeInTOC.rawValue && textStyle.rawValue <= smallestHeadingToIncludeInTOC.rawValue) {
                                // TODO: create a properly formatted string
                                let tocNode = TableOfContentsElement(attrString: NSAttributedString(string: heading), pageIndex: pageIndex)
                                tableOfContents.append(tocNode)
                                //XCGLogger.debug("TOC: \(pageIndex) \(heading)")
                            }
                        }
                        
                        pageIndex = pageIndex + docNode.pageRange.length
                   
                        if let heading = label {
                            if(textStyle.rawValue >= biggestHeadingToIncludeInTOC.rawValue && textStyle.rawValue <= smallestHeadingToIncludeInTOC.rawValue) {
                                // TODO: create a properly formatted string
                                let tocNode = TableOfContentsElement(attrString: NSAttributedString(string: heading), pageIndex: pageIndex)
                                tableOfContents.append(tocNode)
                                //XCGLogger.debug("TOC: \(pageIndex) \(heading)")
                            }
                        }
                        
                        pageIndex =  pageIndex + docNode.pageRange.length
                    }
    
                        return tableOfContents
                }
                /*
                var pagesCount: Int {
                get {
                if(document.count == 0) {
                XCGLogger.warning("document doesn't have any elements, pagesCount would not be accurate")
                }
                if((tableOfContentsPagesRange.location + tableOfContentsPagesRange.length) == 0) {
                XCGLogger.warning("table of contents not laid out, pagesCount would not be accurate")
                }
                var pagesCount = 0
                for (var i = 0; i < document.count; i++) {
                let docNode = document[i]
                //XCGLogger.debug("\(i) \(docNode.functionCall) \(StringFromRange(docNode.pageRange))")
                pagesCount += (docNode.pageRange.location + docNode.pageRange.length)
                }
                
                pagesCount += (tableOfContentsPagesRange.location + tableOfContentsPagesRange.length)
                
                return pagesCount
                }
                }*/
        }
        
        // MARK: - Text Style
        public enum TextStyle: Int {
                case H1 = 0
                case H2 = 1
                case H3 = 2
                case H4 = 3
                case H5 = 4
                case H6 = 5
                case BodyText = 6
        }
        
        // MARK: - Text Formatter
        public class DefaultTextFormatter {
                func attributedStringForStyle(string: String, style: TextStyle) -> NSAttributedString {
                        let attrString = NSMutableAttributedString(string: string)
                        
                        let paragraphStyle = NSMutableParagraphStyle()
                        switch(style) {
                        case .H1:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.boldSystemFont(ofSize: 24), range: NSMakeRange(0, attrString.length))
                                paragraphStyle.alignment = .center
                        case .H2:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.boldSystemFont(ofSize: 20), range: NSMakeRange(0, attrString.length))
                                paragraphStyle.alignment = .center
                        case .H3:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.boldSystemFont(ofSize: 16), range: NSMakeRange(0, attrString.length))
                                paragraphStyle.alignment = .center
                        case .H4:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.boldSystemFont(ofSize: 14), range: NSMakeRange(0, attrString.length))
                        case .H5:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.boldSystemFont(ofSize: 12), range: NSMakeRange(0, attrString.length))
                        case .H6:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.boldSystemFont(ofSize: 10), range: NSMakeRange(0, attrString.length))
                        case .BodyText:
                            attrString.addAttribute(NSAttributedStringKey.font, value:NSFont.systemFont(ofSize: 10), range: NSMakeRange(0, attrString.length))
                        }
                        
                    attrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attrString.length))
                        return attrString
                }
                
        }
        
        // MARK: - PDFWriter
        public class PDFWriter {
                public var textFormatter: DefaultTextFormatter
                
                public var pageSize: PageSize
                public var pageOrientation: PageOrientation
                public var leftMargin:CGFloat
                public var rightMargin: CGFloat
                public var topMargin: CGFloat
                public var bottomMargin: CGFloat
                
                public var currentPage = -1
                public var pagesCount = 0
            public var currentLocation = CGPoint.init(x: CGFloat.greatestFiniteMagnitude, y: CGFloat.greatestFiniteMagnitude)
            
                public var availablePageRect = CGRect.zero
                public var pdfContext: CGContext?
                
                var headerFooterTexts: Array<HeaderFooterText>
                var headerFooterImages: Array<HeaderFooterImage>
                
                let kPageNumberPlaceholder: String
                let kPagesCountPlaceholder: String
                let kStandardSpacing: CGFloat
                let kDefaultBackgroundBoxColor: NSColor
                
                init(textFormatter: DefaultTextFormatter, pageSize: PageSize, pageOrientation: PageOrientation, leftMargin: CGFloat, rightMargin: CGFloat,
                        topMargin: CGFloat, bottomMargin: CGFloat, pagesCount: Int, headerFooterTexts: Array<HeaderFooterText>,
                        headerFooterImages: Array<HeaderFooterImage>, kPageNumberPlaceholder: String, kPagesCountPlaceholder: String,
                        kStandardSpacing: CGFloat, kDefaultBackgroundBoxColor: NSColor) {
                                self.textFormatter = textFormatter
                                self.pageSize = pageSize
                                self.pageOrientation = pageOrientation
                                self.leftMargin = leftMargin
                                self.rightMargin = rightMargin
                                self.topMargin = topMargin
                                self.bottomMargin = bottomMargin
                                
                                self.pagesCount = pagesCount
                                self.headerFooterImages = headerFooterImages
                                self.headerFooterTexts = headerFooterTexts
                                
                                self.kPageNumberPlaceholder = kPageNumberPlaceholder
                                self.kPagesCountPlaceholder = kPagesCountPlaceholder
                                self.kStandardSpacing = kStandardSpacing
                                self.kDefaultBackgroundBoxColor = kDefaultBackgroundBoxColor
                                
                                let bounds = getPageBounds()
                                let origin = CGPoint(x: bounds.origin.x + leftMargin, y: bounds.origin.y + topMargin)
                                let size = CGSize(width: bounds.size.width - (leftMargin + rightMargin),
                                        height: bounds.size.height - (topMargin + bottomMargin))
                                self.availablePageRect = CGRect(origin: origin, size: size)
                }
                
                var availablePageSize: CGSize {
                        get { return availablePageRect.size }
                }
                
                public func openPDF(path: String, title: String?,  author: String?) -> NSError? {
                        var pageRect = getPageBounds()
                        
                        var documentInfo = [kCGPDFContextCreator as String: "\(SimplePDFUtilities.getApplicationName()) \(SimplePDFUtilities.getApplicationVersion())"]
                        if let a = author {
                                documentInfo[kCGPDFContextAuthor as String] = a
                        }
                        if let t = title {
                                documentInfo[kCGPDFContextTitle as String] = t
                        }
                        
                        let url = NSURL(fileURLWithPath: path)
                    pdfContext = CGContext(url, mediaBox: &pageRect, documentInfo as CFDictionary)
                        
                        //currentLocation.y = CGFloat.max
                        //startNewPage()
                        return nil
                }
                
                
                public func closePDF() {
                        if pageOpen {
                            pdfContext!.endPDFPage()
                                pageOpen = false
                        }
                    pdfContext!.closePDF()
                }
                
                public var pageOpen = false
                
                public func startNewPage(calculationOnly: Bool = false) -> NSRange {
                        if(calculationOnly == false) {
                                if pageOpen {
                                    pdfContext!.endPDFPage()
                                        pageOpen = false
                                }
                            pdfContext!.beginPDFPage(nil)
                            pdfContext?.translateBy(x: 0, y: getPageBounds().size.height)
                            pdfContext?.scaleBy(x: 1, y: -1)
                            pageOpen = true
                        }
                        currentPage = currentPage + 1
                        currentLocation = CGPoint.zero
                        if(calculationOnly == false) {
                                addPageHeaderFooter()
                        }
                        
                        return NSMakeRange(1, 0)
                }
                
                // MARK: Headers and Footers
                public func addPageHeaderFooter() {
                        /*if(pagesCount == 0) {
                        XCGLogger.warning("pages count not assigned, if it's printed in a header/footer, it would be wrong.")
                        }*/
                        // draw top line
                    drawLine(p1: CGPoint.init(x: availablePageRect.origin.x, y: availablePageRect.origin.y-10), p2: CGPoint.init(x: availablePageRect.origin.x + availablePageRect.size.width, y: availablePageRect.origin.y - 10))
                    
                        // draw bottom line
                    drawLine(p1: CGPoint(x:availablePageRect.origin.x,y: availablePageRect.origin.y + availablePageRect.size.height + 1),
                             p2: CGPoint(x:availablePageRect.origin.x + availablePageRect.size.width, y:availablePageRect.origin.y + availablePageRect.size.height + 1))
                    for  i in (0..<self.headerFooterTexts.count){
                        var text = self.headerFooterTexts[i]
                        let textString = NSMutableAttributedString(attributedString: text.attributedString)
                        textString.mutableString.replaceOccurrences(of: kPageNumberPlaceholder, with: "\(currentPage + 1)", options: [], range: NSMakeRange(0, textString.length))
                        textString.mutableString.replaceOccurrences(of: kPagesCountPlaceholder, with: "\(pagesCount)", options: [], range: NSMakeRange(0, textString.length))
                        text.attributedString = textString
                        if NSLocationInRange(currentPage, text.pageRange) {
                            switch(text.type) {
                            case .Header:
                                addHeaderText(header: text)
                            case .Footer:
                                addFooterText(footer: text)
                            }
                        }

                    }
                    for  i in (0..<self.headerFooterImages.count){
                                let image = self.headerFooterImages[i]
                                if(image.imagePath.isEmpty && image.image == nil) {
                                        print("ERROR: image path is empty and image is null, skipping")
                                        continue
                                }
                                if NSLocationInRange(currentPage, image.pageRange) {
                                        switch(image.type) {
                                        case .Header:
                                            addHeaderImage(header: image)
                                        case .Footer:
                                            addFooterImage(footer: image)
                                        }
                                }
                        }
                }
                
                public func addHeaderText(header: HeaderFooterText) {
                        let availableHeight = topMargin - 11
                        let framesetter = CTFramesetterCreateWithAttributedString(header.attributedString)
                    var suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), nil, CGSize(width:availablePageRect.width, height:CGFloat.greatestFiniteMagnitude), nil)
                        if(suggestedSize.height > availableHeight) {
                                suggestedSize.height = availableHeight
                        }
                        
                        let textRect = CGRect(x: availablePageRect.origin.x, y: availableHeight - suggestedSize.height, width: availablePageRect.width, height: suggestedSize.height)
                        
                    drawHeaderFooterText(framesetter: framesetter, textRect: textRect)
                }
                
                public func addFooterText(footer: HeaderFooterText) {
                        let availableHeight = bottomMargin - 2
                        let framesetter = CTFramesetterCreateWithAttributedString(footer.attributedString)
                    var suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), nil, CGSize(width:availablePageRect.width, height:CGFloat.greatestFiniteMagnitude), nil)
                        if(suggestedSize.height > availableHeight) {
                                suggestedSize.height = availableHeight
                        }
                        
                        let textRect = CGRect(x: availablePageRect.origin.x, y: availablePageRect.origin.y + availablePageRect.size.height + 2, width: availablePageRect.width, height: suggestedSize.height)
                        
                    drawHeaderFooterText(framesetter: framesetter, textRect: textRect)
                        
                }
                
            public func drawHeaderFooterText(framesetter: CTFramesetter, textRect: CGRect) {
                var textRect = textRect
                textRect = convertRectToCoreTextCoordinates(r:textRect)
                        
                        let context = pdfContext
                        let bounds = getPageBounds()
                
                context?.textMatrix = .identity
                context?.translateBy(x: 0, y: bounds.size.height)
                context?.scaleBy(x: 1.0, y: -1.0)
            
                let textPath = CGMutablePath()
                textPath.addRect(textRect)
                let frameRef = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), textPath, nil)
                CTFrameDraw(frameRef, context!)
                // flip it back
                context?.scaleBy(x: 1.0, y: -1.0)
                context?.translateBy(x: 0, y: -bounds.size.height)
                }
                
                public func addHeaderImage(header: HeaderFooterImage) {
                        let availableHeight = topMargin - 11
                        var imageHeight = header.imageHeight
                        if(imageHeight > availableHeight) {
                                imageHeight = availableHeight
                        }
                        var image = header.image
                        if(image == nil) {
                                image = NSImage(contentsOfFile: header.imagePath)
                                if(image == nil) {
                                        print("ERROR: Unable to read image: \(header.imagePath)")
                                        return
                                }
                        }
                        
                        let y = availableHeight - imageHeight
                    drawHeaderFooterImage(alignment: header.alignment, image: image!, imageHeight: imageHeight, y: y)
                }
                
                public func addFooterImage(footer: HeaderFooterImage) {
                        let availableHeight = bottomMargin - 2
                        var imageHeight = footer.imageHeight
                        if(imageHeight > availableHeight) {
                                imageHeight = availableHeight
                        }
                        var image = footer.image
                        if(image == nil) {
                                image = NSImage(contentsOfFile: footer.imagePath)
                                if(image == nil) {
                                        print("ERROR: Unable to read image: \(footer.imagePath)")
                                        return
                                }
                        }
                        let y = availablePageRect.origin.y + availablePageRect.size.height + 2
                    drawHeaderFooterImage(alignment: footer.alignment, image: image!, imageHeight: imageHeight, y: y)
                }
                
                public func drawHeaderFooterImage(alignment: NSTextAlignment, image: NSImage, imageHeight: CGFloat, y: CGFloat) {
                        var x:CGFloat = 0
                    let imageWidth = aspectFitWidthForHeight(size: image.size, height: imageHeight)
                        switch(alignment) {
                        case .left:
                                x = availablePageRect.origin.x
                                break;
                        case .center:
                                x = availablePageRect.origin.x + ((availablePageRect.size.width - imageWidth) / 2)
                                break;
                        default: // align right
                                x = (availablePageRect.origin.x + availablePageRect.size.width) - imageWidth
                                break;
                        }
                        
                        let imageRect = CGRect(x: x, y: y, width: imageWidth, height: imageHeight)
                    CIImage.fromNSImage(i: image)?.flippedImage()?.drawInCGContext(context: pdfContext!, destRect: imageRect)
                }
                
                // MARK: - Document elements
            public func addH1(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                var backgroundBoxColor = backgroundBoxColor
                let attrString = textFormatter.attributedStringForStyle(string: string, style: .H1)
                        if(backgroundBoxColor == nil) {
                                backgroundBoxColor = kDefaultBackgroundBoxColor
                        }
                return addAttributedString(attrString:attrString, allowSplitting: false, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                
            public func addH2(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                var backgroundBoxColor = backgroundBoxColor
                let attrString = textFormatter.attributedStringForStyle(string: string, style: .H2)
                        if(backgroundBoxColor == nil) {
                                backgroundBoxColor = kDefaultBackgroundBoxColor
                        }
                return addAttributedString(attrString: attrString, allowSplitting: false, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                
            public func addH3(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                var backgroundBoxColor = backgroundBoxColor
                let attrString = textFormatter.attributedStringForStyle(string: string, style: .H3)
                        if(backgroundBoxColor == nil) {
                                backgroundBoxColor = kDefaultBackgroundBoxColor
                        }
                return addAttributedString(attrString: attrString, allowSplitting: false, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                public func addH4(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                    let attrString = textFormatter.attributedStringForStyle(string: string, style: .H4)
                    return addAttributedString(attrString: attrString, allowSplitting: false, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                public func addH5(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                    let attrString = textFormatter.attributedStringForStyle(string: string, style: .H5)
                    return addAttributedString(attrString: attrString, allowSplitting: false, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                
                public func addH6(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                    let attrString = textFormatter.attributedStringForStyle(string: string, style: .H6)
                    return addAttributedString(attrString: attrString, allowSplitting: false, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                public func addBodyText(string: String, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                    let attrString = textFormatter.attributedStringForStyle(string: string, style: .BodyText)
                    return addAttributedString(attrString: attrString, allowSplitting: true, backgroundBoxColor: backgroundBoxColor, calculationOnly: calculationOnly)
                        
                }
                
            public func addImages( imagePaths:[String], imageCaptions: [String], imagesPerRow:Int = 3, spacing:CGFloat = 2, padding:CGFloat = 5, calculationOnly: Bool = false) -> NSRange {
                var imagePaths = imagePaths
                assert(imagePaths.count == imageCaptions.count, "image paths and image captions don't have same number of elements")
                        
                        var funcCallRange = NSMakeRange(0, 0)
                        
                        var columnWidths = Array<CGFloat>()
                        let singleColumnWidth = availablePageRect.size.width / CGFloat(imagesPerRow)
                for _ in (0..<imagesPerRow){
                    columnWidths.append(singleColumnWidth)
                }
      
                var attributedImageCaptions = Array<NSAttributedString>()
                for  i in (0..<imageCaptions.count){
                    let mutableCaption = NSMutableAttributedString(attributedString: textFormatter.attributedStringForStyle(string: imageCaptions[i], style: .H6))
                    /* this doesn't work since captions are drawn using CTLine
                     let paragraphStyle = NSMutableParagraphStyle()
                     paragraphStyle.alignment = .Center
                     mutableCaption.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, mutableCaption.length)) */
                    attributedImageCaptions.append(mutableCaption)
                }
                
                        var rowIndex = 0
                        repeat {
                                var itemsToGet = imagesPerRow
                                if(imagePaths.count < itemsToGet){
                                        itemsToGet = imagePaths.count
                                }
                                let rowImages = Array(imagePaths[0..<itemsToGet])
                                let rowCaptions = Array(attributedImageCaptions[0..<itemsToGet])
                                imagePaths[0..<itemsToGet] = []
                                attributedImageCaptions[0..<itemsToGet] = []
                                
                            let thisRange = addImagesRow(imagePaths: rowImages, imageCaptions: rowCaptions, columnWidths: Array(columnWidths[0..<itemsToGet]), spacing: spacing, padding: padding, captionBackgroundColor: kDefaultBackgroundBoxColor, calculationOnly: calculationOnly)
                                
                                if(rowIndex == 0) {
                                        funcCallRange = thisRange
                                }
                                else {
                                        funcCallRange.length += (thisRange.location + thisRange.length)
                                }
                                rowIndex = rowIndex + 1
                                
                        } while(imagePaths.count > 0)
                        return funcCallRange
                }
                
                public func addImagesRow(imagePaths: [String], imageCaptions: [NSAttributedString], columnWidths: [CGFloat],
                        spacing: CGFloat = 2, padding: CGFloat = 5, captionBackgroundColor: NSColor? = nil,
                        imageBackgroundColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                                assert(imagePaths.count == imageCaptions.count && imageCaptions.count == columnWidths.count,
                                        "image paths, image captions and column widths don't have same number of elements")
                                
                                var funcCallRange = NSMakeRange(0, 0)
                                
                                var imageProperties = Array<NSDictionary>()
                    
                    for  i in (0..<imagePaths.count){
                        if(imagePaths[i].isEmpty) {
                            imageProperties.append(NSDictionary())
                            continue
                        }
                        let thisImageProperties = SimplePDFUtilities.getImageProperties(imagePath: imagePaths[i])
                        imageProperties.append(thisImageProperties! as NSDictionary)
                    }

                                
                                var maxLineHeight:CGFloat = 0
                    for  i in (0..<imageCaptions.count){
                        let thisCaption = imageCaptions[i]
                        if(thisCaption.length == 0) {
                            continue
                        }
                        let line = CTLineCreateWithAttributedString(thisCaption)
                        let lineBounds = CTLineGetBoundsWithOptions(line, CTLineBoundsOptions.useGlyphPathBounds)
                        
                        if(lineBounds.size.height > maxLineHeight) {
                            maxLineHeight = lineBounds.size.height
                        }
                    }
                                
                                // start a new page if needed
                    
                    for  i in (0..<imagePaths.count) {
                                        let thisWidth = columnWidths[i] - (2 * padding)
                                    let availableSpace = CGSize(width: CGFloat.greatestFiniteMagnitude, height: availablePageRect.size.height - currentLocation.y)
                                        
                                        let thisProperties = imageProperties[i]
                                        if thisProperties.allKeys.count == 0 {
                                                continue
                                        }
                                        let imageWidth = thisProperties[kCGImagePropertyPixelWidth as String] as! CGFloat
                                        let imageHeight = thisProperties[kCGImagePropertyPixelHeight as String] as! CGFloat
                                        
                                        let imageSize = CGSize(width: imageWidth, height:imageHeight)
                                        
                                    let fitHeight = self.aspectFitHeightForWidth(size: imageSize, width: thisWidth)
                                        if(fitHeight + maxLineHeight > availableSpace.height) {
                                                funcCallRange.location = 1
                                            startNewPage(calculationOnly: calculationOnly)
                                                break
                                        }
                                }
                                
                                currentLocation.y += padding
                                var maxHeightRendered: CGFloat = 0
                                var loc = currentLocation
                    for  i in (0..<imagePaths.count) {
                                        // render the label
                                        var currentY = loc.y
                                        let thisCaption = imageCaptions[i]
                                        let thisWidth = columnWidths[i]
                                        var availableSpace = CGSize(width: thisWidth - (2 * padding), height: availablePageRect.size.height - loc.y)
                                        
                                        if(thisCaption.length != 0) {
                                                let line = CTLineCreateWithAttributedString(thisCaption)
                                                let truncationToken = CTLineCreateWithAttributedString(NSAttributedString(string:"…"))
                                            let truncatedLine = CTLineCreateTruncatedLine(line, Double(availableSpace.width), CTLineTruncationType.end, truncationToken)
                                                
                                                if(calculationOnly == false) {
                                                        if(captionBackgroundColor != nil) {
                                                                let originalTextRect = CGRect(x: availablePageRect.origin.x + loc.x + padding, y: availablePageRect.origin.y + currentY,
                                                                        width: thisWidth - (2 * padding), height: maxLineHeight + spacing)
                                                            drawRect(rect: originalTextRect, fillColor: captionBackgroundColor)
                                                        }
                                                        
                                                }
                                                let originalPoint = CGPoint(x: availablePageRect.origin.x + loc.x + padding, y: availablePageRect.origin.y + currentY)
                                            var textPoint = convertPointToCoreTextCoordinates(p: originalPoint)
                                                
                                                // since we need to provide the base line coordinates to CoreText, we should subtract the maxLineHeight
                                                textPoint.y -= maxLineHeight
                                                
                                                if(calculationOnly == false) {
                                                        // flip context
                                                        let context = pdfContext
                                                        let bounds = getPageBounds()
                                                        context?.textMatrix = .identity
                                                        context?.translateBy(x: bounds.origin.x, y: bounds.size.height)
                                                        context?.scaleBy(x: 1.0, y: -1.0)
                                                    
                                                        context?.textPosition = textPoint
                                                    
                                                        CTLineDraw(truncatedLine!, context!)
                                                        
                                                        // flip it back
                                                    context?.scaleBy(x: 1.0, y: -1.0)
                                                    context?.translateBy(x: -bounds.origin.x, y: -bounds.size.height)
                                                  
                                                }
                                        }
                                        
                                        currentY += (maxLineHeight + spacing)
                                        availableSpace.height -= (maxLineHeight + spacing)
                                        
                                        // render the image
                                        let thisProperties = imageProperties[i]
                                        
                                        if(thisProperties.allKeys.count == 0) {
                                                // advance to next column
                                                loc.x += thisWidth
                                                continue
                                        }
                                        
                                        let imageWidth = thisProperties[kCGImagePropertyPixelWidth as String] as! CGFloat
                                        let imageHeight = thisProperties[kCGImagePropertyPixelHeight as String] as! CGFloat
                                        
                                        let originalImageSize = CGSize(width: imageWidth, height:imageHeight)
                                        
                                    let fitHeight = self.aspectFitHeightForWidth(size: originalImageSize, width: availableSpace.width)
                                        var imageSizeToRender = CGSize(width: availableSpace.width, height: fitHeight)
                                        if(fitHeight > availableSpace.height) {
                                            let fitWidth = self.aspectFitWidthForHeight(size: originalImageSize, height: availableSpace.height)
                                                imageSizeToRender = CGSize(width: fitWidth, height: availableSpace.height)
                                        }
                                        
                                        if(calculationOnly == false) {
                                                if(imageBackgroundColor != nil) {
                                                        let bgRect = CGRect(x: availablePageRect.origin.x + loc.x + padding, y: availablePageRect.origin.y + currentY,
                                                                width: availableSpace.width, height: availableSpace.height)
                                                    drawRect(rect: bgRect, fillColor: imageBackgroundColor)
                                                }
                                        }
                                        
                                        let imageX = availablePageRect.origin.x + loc.x + padding + ((availableSpace.width - imageSizeToRender.width) / 2)
                                        let imageY = availablePageRect.origin.y + currentY
                                        let imageRect = CGRect(x: imageX, y: imageY, width: imageSizeToRender.width, height: imageSizeToRender.height)
                                        
                                        if(calculationOnly == false) {
                                            let ciimg = CIImage(contentsOf:NSURL(fileURLWithPath:imagePaths[i]) as URL)
                                            ciimg?.flippedImage()?.drawInCGContext(context: pdfContext!, destRect: imageRect)
                                        }
                                        
                                        // advance to next column
                                        loc.x += thisWidth
                                        
                                        let totalHeight = (maxLineHeight + imageSizeToRender.height + spacing)
                                        
                                        if(totalHeight  > maxHeightRendered) {
                                                maxHeightRendered = totalHeight
                                        }
                                }
                                currentLocation.y += maxHeightRendered
                                currentLocation.y += kStandardSpacing
                                return funcCallRange
                }
                
                public func addAttributedStringsToColumns(columnWidths: [CGFloat], strings: [NSAttributedString], horizontalPadding: CGFloat = 5, allowSplitting: Bool = true, backgroundColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                        assert(columnWidths.count == strings.count, "columnWidths and strings array don't have same number of elements")
                        
                        var funcCallRange = NSMakeRange(0, 0)
                        
                        var ranges = Array<CFRange>() // tracks range for each column
                    var framesetters = Array<CTFramesetter>() // tracks framsetter for each column
                    for  i in (0..<strings.count){
                        ranges.append(CFRangeMake(0, 0))
                        framesetters.append(CTFramesetterCreateWithAttributedString(strings[i]))
                    }
 
                        
                        var availableSpace = CGSize.zero
                        if((availablePageRect.size.height - currentLocation.y) <= 0) {
                                funcCallRange.location = 1
                            startNewPage(calculationOnly: calculationOnly)
                        }
                        else {
                                // decide if we start start a new page
                                if(allowSplitting == false) {
                                        var allStringsFitOnThisPage = true
                                    for  i in (0..<ranges.count){
                                        let thisWidth = columnWidths[i]
                                        let thisString = strings[i]
                                        
                                        availableSpace = CGSize(width: thisWidth - (2 * horizontalPadding), height: availablePageRect.size.height - currentLocation.y)
                                        
                                        if canFitAttributedString(attrString: thisString, size: availableSpace) == false {
                                            allStringsFitOnThisPage = false
                                            break
                                        }
                                    }
                      
                                        
                                        if(allStringsFitOnThisPage == false) {
                                                var allStringsFitOnANewPage = true
                                            for  i in (0..<ranges.count){
                                                let thisWidth = columnWidths[i]
                                                let thisString = strings[i]
                                                
                                                availableSpace = CGSize(width: thisWidth - (2 * horizontalPadding), height: availablePageRect.size.height)
                                                
                                                if canFitAttributedString(attrString: thisString, size: availableSpace) == false {
                                                    allStringsFitOnANewPage = false
                                                    break
                                                }
                                            }
                        
                                                
                                        if(allStringsFitOnANewPage) {
                                                startNewPage(calculationOnly: calculationOnly)
                                                        funcCallRange.location = 1
                                                }
                                        }
                                }
                        }
                        
                        var done = false
                        repeat {
                                var loc = currentLocation
                                var maxHeightRendered:CGFloat = 0
                        
                            for  i in (0..<columnWidths.count) {
                                        var thisRange = ranges[i]
                                        let thisFramesetter = framesetters[i]
                                        let thisWidth = columnWidths[i]
                                        let thisString = strings[i]
                                        // skip the column if it has been rendered completely
                                        if(thisRange.location >= thisString.length) {
                                                loc.x += thisWidth
                                                continue
                                        }
                                        
                                        availableSpace = CGSize(width: thisWidth - (2 * horizontalPadding), height: availablePageRect.size.height - loc.y)
                                        // if height is -ve, CTFramesetterSuggestFrameSizeWithConstraints doesn't return an empty fitRange
                                        if(availableSpace.height <= 0) {
                                                break
                                        }
                                        var fitRange = CFRangeMake(0, 0)
                                        let suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(thisFramesetter, thisRange, nil, availableSpace, &fitRange)
                                        
                                        // draw string
                                        var originalTextRect = CGRect(x: availablePageRect.origin.x + loc.x, y: availablePageRect.origin.y + loc.y,
                                                width: thisWidth, height: availableSpace.height)
                                        if(calculationOnly == false) {
                                                if(backgroundColor != nil) {
                                                        let boxRect = CGRect(x: originalTextRect.origin.x, y: originalTextRect.origin.y, width: availableSpace.width, height: suggestedSize.height)
                                                    drawRect(rect: boxRect, fillColor: backgroundColor)
                                                }
                                        }
                                        
                                        originalTextRect.origin.x += horizontalPadding
                                        originalTextRect.size.width = availableSpace.width
                                        
                                    let textRect = convertRectToCoreTextCoordinates(r: originalTextRect)
                                        
                                        if(calculationOnly == false) {
                                                // flip context
                                                let context = pdfContext
                                                let bounds = getPageBounds()
                                            context?.textMatrix = .identity
                                            context?.translateBy(x: bounds.origin.x, y: bounds.size.height)
                                            context?.scaleBy(x: 1.0, y: -1.0)
                                            
                                            let textPath = CGMutablePath()
                                            textPath.addRect(textRect)
                                            let frameRef = CTFramesetterCreateFrame(thisFramesetter, thisRange, textPath, nil)
                                                
                                                CTFrameDraw(frameRef, context!)
                                                
                                                // flip it back
                                            context?.scaleBy(x: 1.0, y: -1.0)
                                            context?.translateBy(x: -bounds.origin.x
                                                , y: -bounds.size.height)
                                            
                                        }
                                        
                                        thisRange.location = thisRange.location + fitRange.length
                                        ranges[i] = thisRange
                                        
                                        // if we couldn't render whole string, that means we have utilised all avialable height
                                        if(thisRange.location < thisString.length) {
                                                maxHeightRendered = availableSpace.height
                                        }
                                        else {
                                                if(suggestedSize.height >= maxHeightRendered) {
                                                        maxHeightRendered = suggestedSize.height
                                                }
                                        }
                                        
                                        loc.x += thisWidth
                                }
                                
                                var shouldAddNewPage = false
                                
                                done = true
                            
                            for  i in (0..<ranges.count){
                                        let thisRange = ranges[i]
                                        let thisString = strings[i]
                                        if thisRange.location < thisString.length {
                                                done = false
                                                shouldAddNewPage = true
                                                break
                                        }
                                }
                                
                                if(shouldAddNewPage) {
                                    startNewPage(calculationOnly: calculationOnly)
                                        funcCallRange.length = funcCallRange.length + 1
                                }
                                else {
                                        currentLocation.y += maxHeightRendered
                                        currentLocation.y += kStandardSpacing
                                }
                                
                        } while (!done)
                        return funcCallRange
                }
                
                
                public func addAttributedString(attrString: NSAttributedString, allowSplitting:Bool = true, backgroundBoxColor: NSColor? = nil, calculationOnly: Bool = false) -> NSRange {
                    return addAttributedStringsToColumns(columnWidths: [availablePageRect.size.width], strings: [attrString], horizontalPadding: 0.0, allowSplitting: allowSplitting, backgroundColor: backgroundBoxColor, calculationOnly: calculationOnly)
                }
                
                public func addView(view: NSView, calculationOnly: Bool = false) -> NSRange {
                        // Here's how I work with NSRange in these functions
                        // location is startPageOffset
                        // length is how many pages are added, so (length + location) = last page index
                        
                        var range = NSMakeRange(0, 0) // a view is always
                        if(currentLocation.y > 0) {
                                range.location = 1
                            startNewPage(calculationOnly: calculationOnly)
                        }
                        
                        if(calculationOnly == false) {
                                let context = pdfContext
                            view.layer!.render(in: context!)
                        }
                        
                        // one view per page, set Y to maximum so that next call inserts a page
                        currentLocation.y = availablePageSize.height
                        return range
                }
                
                public func drawTableofContents(document:DocumentStructure, calculationOnly:Bool = true) -> NSRange {
                        if(document.tableOfContents.count == 0) {
                                return NSMakeRange(0, 0)
                        }
                        
                        var funcRange = NSMakeRange(0, 0)
                        if(currentLocation.y > 0) {
                                funcRange.location = 1
                            startNewPage(calculationOnly: calculationOnly)
                        }
                        
                    let headingRange = addH3(string: "Table of Contents", backgroundBoxColor: nil, calculationOnly: calculationOnly)
                        funcRange.length += (headingRange.location + headingRange.length)
                    
                    for  i in (0..<document.tableOfContents.count){
                                let tocNode = document.tableOfContents[i]
                                
                                // NOTE: on very first call to this function, page numbers would not be correct in table of contents because we don't know
                                // how many pages TOC would take. It does not matter however because the very first call would be "calculationOnly" anyway
                                var tocAdjustedPageNumber = tocNode.pageIndex
                                if(tocNode.pageIndex >= document.tableOfContentsOnPage) {
                                        tocAdjustedPageNumber += (document.tableOfContentsPagesRange.location + document.tableOfContentsPagesRange.length)
                                }
                                
                                let pageNumberAttrString = NSMutableAttributedString(string: "\(tocAdjustedPageNumber + 1)")
                                let paragraphStyle = NSMutableParagraphStyle()
                            paragraphStyle.alignment = .right
                            pageNumberAttrString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, pageNumberAttrString.length))
                                
                                let col2Width:CGFloat = 50
                                let col1Width = availablePageSize.width - col2Width
                                
                            let range = addAttributedStringsToColumns(columnWidths: [col1Width, col2Width], strings: [tocNode.attrString, pageNumberAttrString], horizontalPadding: 5, allowSplitting: false, backgroundColor: nil, calculationOnly: calculationOnly)
                                
                                funcRange.length += (range.location + range.length)
                        }
                        
                        // this is to force a page break before new element
                        currentLocation.y = availablePageSize.height
                        
                        return funcRange
                }
                
                
                // MARK: - Drawing
            public func drawRect(rect: CGRect, fillColor: NSColor? = nil) {
                var fillColor = fillColor
                if(fillColor == nil) {
                                fillColor = NSColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
                        }
                        let context = pdfContext
                        context?.setFillColor(red: 0/255, green:  0/255, blue:  0/255, alpha: 1.0)
                        context?.fill(rect)
                        //CGContextStrokeRect(context, rect)
                }
                
            public func drawLine(p1: CGPoint, p2:CGPoint, color: NSColor? = nil, strokeWidth: CGFloat = 1) {
                var color = color
                if(color == nil) {
                                color = NSColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
                        }
                    let context = pdfContext
                    context?.setStrokeColor((color?.cgColor)!)
                    context?.setLineWidth(strokeWidth)
                    context?.move(to: p1)
                    context?.addLine(to: p2)
                    context?.drawPath(using: .stroke)
    
                }
                
                // MARK: - Utilities
                public func canFitAttributedString(attrString: NSAttributedString, size:CGSize) -> Bool {
                        // if height is -ve, CTFramesetterSuggestFrameSizeWithConstraints doesn't return an empty fitRange
                        if(size.height <= 0) {
                                return false
                        }
                        
                        let frameSetter = CTFramesetterCreateWithAttributedString(attrString)
                        var fitRangeFullSize = CFRangeMake(0, 0)
                        CTFramesetterSuggestFrameSizeWithConstraints(frameSetter, CFRangeMake(0, 0), nil, size, &fitRangeFullSize)
                        if(fitRangeFullSize.length < attrString.length) {
                                return false
                        }
                        return true
                }
                
            public func convertRectToCoreTextCoordinates( r: CGRect) -> CGRect {
                var r = r
                let bounds = getPageBounds()
                        r.origin.y = bounds.size.height - (r.size.height + r.origin.y)
                        return r
                }
                
            public func convertPointToCoreTextCoordinates( p: CGPoint) -> CGPoint {
                var p = p
                let bounds = getPageBounds()
                        p.y = bounds.size.height - p.y
                        return p
                }
                
                public func aspectFitHeightForWidth(size: CGSize, width: CGFloat) -> CGFloat {
                        let ratio = size.width / width
                        let newHeight = size.height / ratio
                        return newHeight
                }
                
                public func aspectFitWidthForHeight(size: CGSize, height: CGFloat) -> CGFloat {
                        let ratio = size.height / height
                        let newWidth = size.width / ratio
                        return newWidth
                }
                
                public func getPageBounds() -> CGRect {
                        let size = self.pageSize.asCGSize()
                        if(pageOrientation == .Portrait) {
                            return CGRect.init(origin: .zero, size: size)
                        }
                        else {
                            return CGRect.init(origin: .zero, size: size)
                        }
                }
        }
        
        // MARK: - PageSize
        public enum PageSize {
                case Letter
                case A4
                case Custom(size: CGSize)
                
                func asCGSize() -> CGSize {
                        switch(self) {
                        case .Letter:
                            return CGSize(width:612, height:792)
                        case .A4:
                            return CGSize(width:595,height:842)
                        case .Custom(let size):
                                return size
                        }
                }
        }
        
        // MARK: - PageOrientation
        public enum PageOrientation {
                case Portrait
                case Landscape
        }
        
        // MARK: - HeaderFooterType
        public enum HeaderFooterType {
                case Header
                case Footer
        }
        
        // MARK: - HeaderFooterText
        public struct HeaderFooterText {
                var type = HeaderFooterType.Header
                var pageRange = NSMakeRange(0, 0)
                var attributedString: NSAttributedString
        }
        
        // MARK: - HeaderFooterImage
        public struct HeaderFooterImage {
                var type = HeaderFooterType.Header
                var pageRange = NSMakeRange(0, 0)
                var imagePath = ""
                var image: NSImage?
                var imageHeight: CGFloat
                var alignment =  NSTextAlignment.left
        }
        
        // MARK: - SimplePDF vars
        public (set) public var textFormatter: DefaultTextFormatter
        public (set) public var pageSize: PageSize
        public (set) public var pageOrientation: PageOrientation
        public (set) public var leftMargin:CGFloat
        public (set) public var rightMargin: CGFloat
        public (set) public var topMargin: CGFloat
        public (set) public var bottomMargin: CGFloat
        
        public var headerFooterTexts = Array<HeaderFooterText>()
        public var headerFooterImages = Array<HeaderFooterImage>()
        
        public let kPageNumberPlaceholder = "{{PAGE_NUMBER}}"
        public let kPagesCountPlaceholder = "{{PAGES_COUNT}}"
        public let kStandardSpacing:CGFloat = 8
        public let kDefaultBackgroundBoxColor = NSColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
        
        public var availablePageSize: CGSize {
                get { return pdfWriter.availablePageSize }
        }
        
        public var pageIndexToShowTOC: Int {
                get { return document.tableOfContentsOnPage }
                set { document.tableOfContentsOnPage = newValue }
        }
        
        public var biggestHeadingToIncludeInTOC: TextStyle {
                get { return document.biggestHeadingToIncludeInTOC }
                set { document.biggestHeadingToIncludeInTOC = newValue }
        }
        public var smallestHeadingToIncludeInTOC: TextStyle {
                get { return document.smallestHeadingToIncludeInTOC }
                set { document.smallestHeadingToIncludeInTOC = newValue }
        }
        
        public var pdfFilePath: String
        public var authorName: String
        public var pdfTitle: String
        
        public var document = DocumentStructure()
        public var pdfWriter: PDFWriter
        
        public var currentPage : Int {
                get { return self.pdfWriter.currentPage }
        }
        
        /*
        public var pagesCount: Int {
        get { return self.document.pagesCount
        }
        }*/
        
        // MARK: - SimplePDF methods
        init(pdfTitle: String, authorName: String, pageSize: PageSize = .A4, pageOrientation: PageOrientation = .Portrait,
                leftMargin:CGFloat = 36, rightMargin:CGFloat = 36, topMargin: CGFloat = 72, bottomMargin: CGFloat = 36, textFormatter: DefaultTextFormatter = DefaultTextFormatter()) {
                        
                        self.leftMargin = leftMargin
                        self.rightMargin = rightMargin
                        self.topMargin = topMargin
                        self.bottomMargin = bottomMargin
                        self.pageSize = pageSize
                        self.pageOrientation = pageOrientation
                        self.textFormatter = textFormatter
                        
                        self.authorName = authorName
                        self.pdfTitle = pdfTitle
                        
            let tmpFilePath = SimplePDFUtilities.pathForTmpFile(fileName: "PlagiarismReport.pdf")
            self.pdfFilePath = SimplePDFUtilities.renameFilePathToPreventNameCollissions(url: tmpFilePath)
                        
                        self.pdfWriter = PDFWriter(textFormatter: textFormatter, pageSize: pageSize, pageOrientation: pageOrientation,
                                leftMargin: leftMargin, rightMargin: rightMargin, topMargin: topMargin, bottomMargin: bottomMargin,
                                pagesCount: 0,
                                headerFooterTexts: headerFooterTexts, headerFooterImages: headerFooterImages,
                                kPageNumberPlaceholder: kPageNumberPlaceholder, kPagesCountPlaceholder: kPagesCountPlaceholder,
                                kStandardSpacing: kStandardSpacing, kDefaultBackgroundBoxColor: kDefaultBackgroundBoxColor)
        }
        
        public func initializePDFWriter(pagesCount: Int) -> PDFWriter {
                return PDFWriter(textFormatter: textFormatter, pageSize: pageSize, pageOrientation: pageOrientation,
                        leftMargin: leftMargin, rightMargin: rightMargin, topMargin: topMargin, bottomMargin: bottomMargin,
                        pagesCount: pagesCount,
                        headerFooterTexts: headerFooterTexts, headerFooterImages: headerFooterImages,
                        kPageNumberPlaceholder: kPageNumberPlaceholder, kPagesCountPlaceholder: kPagesCountPlaceholder,
                        kStandardSpacing: kStandardSpacing, kDefaultBackgroundBoxColor: kDefaultBackgroundBoxColor)
        }
        
        public func writePDFWithoutTableOfContents() -> String {
                
                /////////// CACULATIONS PASS ///////////
                // Start with a clean slate
            self.pdfWriter = initializePDFWriter(pagesCount: 0)
                var pageIndex = -1
            
            for  i in (0..<document.document.count) {
                        let docElement = document.document[i]
                        //let pageNumber = pageIndex + docElement.pageRange.location
                    docElement.executeFunctionCall(pdf: pdfWriter, calculationOnly: true)
                        pageIndex += (docElement.pageRange.location + docElement.pageRange.length)
                }
                
                /////////// RENDERING PASS ///////////
            self.pdfWriter = initializePDFWriter(pagesCount: pageIndex+1)
                // 1. create context
    
            pdfWriter.openPDF(path: pdfFilePath, title: pdfTitle, author: authorName)
                pageIndex = -1
                
            for  i in (0..<document.document.count) {
                        let docElement = document.document[i]
                        //let pageNumber = pageIndex + docElement.pageRange.location
                    docElement.executeFunctionCall(pdf: pdfWriter, calculationOnly: false)
                        pageIndex += (docElement.pageRange.location + docElement.pageRange.length)
                }
                
                // 3. end pdf context
                pdfWriter.closePDF()
                return pdfFilePath
        }
        
        
        public func writePDFWithTableOfContents() -> String {
                /////////// CACULATIONS PASS ///////////
                // Start with a clean slate
            self.pdfWriter = initializePDFWriter(pagesCount: 0)
                
                // Generate TOC data structure
                // todo: empty toc structure here
                document.tableOfContents = document.generateTableOfContents()
                var tocInserted = false
                var pageIndex = -1
            for  i in (0..<document.document.count) {
                        let docElement = document.document[i]
                        let pageNumber = pageIndex + docElement.pageRange.location
                        // if (location == 1 && pageNumber == document.tableOfContentsOnPage) || (location == 0 && pageNumber > document.tableOfContents) {
                        if(pageNumber >= document.tableOfContentsOnPage && tocInserted == false) {
                                tocInserted = true
                            document.tableOfContentsPagesRange = pdfWriter.drawTableofContents(document: document, calculationOnly: true)
                                pageIndex += (document.tableOfContentsPagesRange.location + document.tableOfContentsPagesRange.length)
                        }
                        
                    docElement.executeFunctionCall(pdf: pdfWriter, calculationOnly: true)
                        pageIndex += (docElement.pageRange.location + docElement.pageRange.length)
                }
                
                if(tocInserted == false) {
                        tocInserted = true
                    document.tableOfContentsPagesRange = pdfWriter.drawTableofContents(document: document, calculationOnly: true)
                        pageIndex += (document.tableOfContentsPagesRange.location + document.tableOfContentsPagesRange.length)
                }
                
                /////////// RECALCULATE TOC -- AFTER WE HAVE UPDATED THE PAGE RANGES BY INSERTING TOC ///////
                // todo: fill in toc data structure with page numbers here
                document.tableOfContents = document.generateTableOfContents()
                
                /////////// RENDERING PASS ///////////
            self.pdfWriter = initializePDFWriter(pagesCount: pageIndex+1)
                // 1. create context
            pdfWriter.openPDF(path: pdfFilePath, title: pdfTitle, author: authorName)
                
                tocInserted = false
                pageIndex = -1
                
            for  i in (0..<document.document.count) {
                        let docElement = document.document[i]
                        let pageNumber = pageIndex + docElement.pageRange.location
                        if(pageNumber >= document.tableOfContentsOnPage && tocInserted == false) {
                                tocInserted = true
                            document.tableOfContentsPagesRange = pdfWriter.drawTableofContents(document: document, calculationOnly: false)
                                pageIndex += (document.tableOfContentsPagesRange.location + document.tableOfContentsPagesRange.length)
                        }
                        
                    docElement.executeFunctionCall(pdf: pdfWriter, calculationOnly: false)
                        pageIndex += (docElement.pageRange.location + docElement.pageRange.length)
                }
                if(tocInserted == false) {
                        tocInserted = true
                    document.tableOfContentsPagesRange = pdfWriter.drawTableofContents(document: document, calculationOnly: false)
                        pageIndex += (document.tableOfContentsPagesRange.location + document.tableOfContentsPagesRange.length)
                }
                
                // 3. end pdf context
                pdfWriter.closePDF()
                return pdfFilePath
        }
        
        // MARK: - Commands
        // NOTE: these functions should only be called by consumers of the class, don't call them internally because they change
        // the document structure
        public func startNewPage() -> NSRange {
            let range = pdfWriter.startNewPage(calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.startNewPage
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addH1(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addH1(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addH1(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addH2(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addH2(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addH2(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addH3(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addH3(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addH3(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addH4(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addH4(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addH4(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addH5(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addH5(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addH5(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addH6(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addH6(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addH6(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addBodyText(string: String, backgroundBoxColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addBodyText(string: string, backgroundBoxColor: backgroundBoxColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addBodyText(string: string, backgroundBoxColor: backgroundBoxColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addImages(imagePaths:[String], imageCaptions: [String], imagesPerRow:Int = 3, spacing:CGFloat = 2, padding:CGFloat = 5) -> NSRange {
            let range = pdfWriter.addImages(imagePaths: imagePaths, imageCaptions: imageCaptions, imagesPerRow: imagesPerRow, spacing: spacing, padding: padding, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addImages(imagePaths: imagePaths, imageCaptions: imageCaptions, imagesPerRow: imagesPerRow, spacing: spacing, padding: padding)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                document.document.append(docNode)
                return range
        }
        
        public func addImagesRow(imagePaths: [String], imageCaptions: [NSAttributedString], columnWidths: [CGFloat],
                spacing: CGFloat = 2, padding: CGFloat = 5, captionBackgroundColor: NSColor? = nil, imageBackgroundColor: NSColor? = nil) -> NSRange {
            let range = pdfWriter.addImagesRow(imagePaths: imagePaths, imageCaptions: imageCaptions, columnWidths: columnWidths, spacing: spacing, padding: padding, captionBackgroundColor: captionBackgroundColor, imageBackgroundColor: imageBackgroundColor, calculationOnly: true)
                        let funcCall = DocumentStructure.FunctionCall.addImagesRow(imagePaths: imagePaths, imageCaptions: imageCaptions, columnWidths: columnWidths, spacing: spacing, padding: padding, captionBackgroundColor: captionBackgroundColor, imageBackgroundColor: imageBackgroundColor)
                        let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                        self.document.document.append(docNode)
                        return range
        }
        
        public func addAttributedStringsToColumns(columnWidths: [CGFloat], strings: [NSAttributedString], horizontalPadding: CGFloat = 5, allowSplitting: Bool = true, backgroundColor: NSColor? = nil) -> NSRange  {
            let range = pdfWriter.addAttributedStringsToColumns(columnWidths: columnWidths, strings: strings, horizontalPadding: horizontalPadding, allowSplitting: allowSplitting, backgroundColor: backgroundColor, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addAttributedStringsToColumns(columnWidths: columnWidths, strings: strings, horizontalPadding: horizontalPadding, allowSplitting: allowSplitting, backgroundColor: backgroundColor)
                let docNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                self.document.document.append(docNode)
                return range
        }
        
        public func addAttributedString(attrString: NSAttributedString, allowSplitting:Bool = true, backgroundBoxColor: NSColor? = nil) -> NSRange {
            return addAttributedStringsToColumns(columnWidths: [pdfWriter.availablePageRect.size.width], strings: [attrString], horizontalPadding: 0.0, allowSplitting: allowSplitting, backgroundColor: backgroundBoxColor)
        }
        
        // This function can be used to render a view to a PDF page (mostly useful to design cover pages). A view is always added to its own page. It starts 
        // a new page if required, and any contented added after it appears on the next page.
        //
        // Here's how you can design a cover page with using a NSView (sample applies to any other view that you want to add to pdf)
        // 1. Create a nib with the same dimensions as PDF page (e.g. A4 page is 595x842)
        // 2. All the labels in the view should have their class set to `SimplePDFLabel` (or a subclass of it)
        // 3. Load the view from the nib and add it to pdf
        // ```
        // // ...
        // let coverPage = NSBundle.mainBundle().loadNibNamed("PDFCoverPage", owner: self, options: nil).first as PDFCoverPage
        // pdf.addView(coverPage)
        // ```
        //
        // NOTE: 
        //      Please note that if you use the above method to render a view to PDF, AutoLayout will *not* be run on it, If your view doesn't rely on 
        // autolayout e.g. say it's a table, may be an invoice?, you don't need to worry about anything.
        //
        // However, if your view uses AutoLayout to correctly position elements, you *have to* add it to the active view hierarchy. You can add to the
        // view hierarchy off-screen, then call `pdf.addView()` to render it to PDF. The catch here is that now the view would render as *bitmap*. This means
        // any labels will not be selectable as text and they would lose quality if you zoom in (because they are bitmaps).
        //
        
        public func addView(view: NSView) -> NSRange {
            let range = pdfWriter.addView(view: view, calculationOnly: true)
                let funcCall = DocumentStructure.FunctionCall.addView(view: view)
                let documentNode = DocumentStructure.DocumentElement(functionCall: funcCall, pageRange: range)
                self.document.document.append(documentNode)
                return range
        }
}
