//
//  downloadFilesCellView.swift
//  PlagiarismChecker
//
//  Created by macbook on 28/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa

class downloadFilesCellView: NSTableCellView {

    //MARK: - PROPERTIES / OUTLETS -
    @IBOutlet weak var imageContainer: NSView!
    @IBOutlet weak var fileIconImageView: NSImageView!
    @IBOutlet weak var fileContainerView: NSView!
    @IBOutlet weak var lbFileName: NSTextField!
    @IBOutlet weak var btnDownload: NSButton!
    @IBOutlet weak var verticalLine: NSBox!
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        // Drawing code here.
        imageContainer.layer?.borderColor = #colorLiteral(red: 0.9282422662, green: 0.195631057, blue: 0.2137799859, alpha: 1)
        imageContainer.layer?.cornerRadius = imageContainer.frame.width/2
   
        imageContainer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        imageContainer.layer?.borderWidth = 2.0
        
        verticalLine.wantsLayer = true
        verticalLine.fillColor = #colorLiteral(red: 0.9282422662, green: 0.195631057, blue: 0.2137799859, alpha: 1)
        
        fileContainerView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        fileContainerView.layer?.cornerRadius = 18
    }
}
