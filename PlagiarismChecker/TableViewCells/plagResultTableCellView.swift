//
//  plagResultTableCellView.swift
//  PlagiarismChecker
//
//  Created by macbook on 26/12/2017.
//  Copyright © 2017 Talha Ejaz. All rights reserved.
//

import Cocoa
import AppKit

class plagResultTableCellView: NSTableCellView {

    @IBOutlet weak var lbQuery: NSTextField!
    @IBOutlet weak var viewQuery: NSView!
    @IBOutlet weak var btnWeb: NSButton!
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        // Drawing code here.
    }
}
