//
//  IntroVC.swift
//  PlagiarismChecker
//
//  Created by macbook on 13/01/2018.
//  Copyright © 2018 Talha Ejaz. All rights reserved.
//

import Cocoa

class IntroVC: NSViewController {

    //MARK: - PROPERTIES -
    @IBOutlet weak var btnSkip: DudNSButton!
    @IBOutlet weak var btnLogin: DudNSButton!
    
    //MARK: - VIEW LIFE CYCLE -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    override func viewDidLayout() {
        super.viewDidLayout()
        self.view.wantsLayer = true
        self.view.backgroundColor = Theme.appNavBarColor()
        
        btnSkip.isBordered = false
        btnSkip.setText(text: "Skip", font:NSFont.init(name: "HelveticaNeue", size: 14.0)!, color: NSColor.white)
        btnSkip.setButtonType(.momentaryChange)
        
        btnLogin.isBordered = false
        btnLogin.setText(text: "LOGIN", font:NSFont.init(name: "HelveticaNeue", size: 14.0)!, color: NSColor.white)
        btnLo
        btnLogin.setButtonType(.momentaryChange)
    }
    
    //MARK: - ACTIONS -
    
}
